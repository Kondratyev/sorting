The directory insertion contains the following files:
1. "sublist.lisp" - sublists theory.
2. "range.lisp" - sublists equivalence theory.
3. "permut.lisp" - permutation theory.
4. "ordered.lisp" - sorting theory.
5. "rep.lisp" - replacement operation theory.
6. "vc-1.lisp" - verification condition from program start to outer loop.
7. "vc-2.lisp" - verification condition from outer loop exit to program end.
8. "vc-3.lisp" - verification condition inside outer loop.

The files "sublist.lisp", "range.lisp", "permut.lisp" and "ordered.lisp" contain underlying theory. They can be used to write sorting program specification.

The files "rep.lisp", "vc-1.lisp", "vc-2.lisp" and "vc-3.lisp" were generated during experiment of deductive verification of insertion sorting program.

1) "sublist.lisp". The construction "define" defines a function "sublist", which takes three arguments: the first index, the second index and a list. This function returns a sublist of argument of given range. In our underlying theory C arrays are simulated by integer lists. The functions "nth" and "update-nth" are used to list access by index and to create an updated list respectively. The lists in ACL2 are
numerated starting from zero (as in C). The function "nth" takes two arguments: index and list. It returns the value of element from list at given index. The function "update-nth" takes three arguments: index, new value and list. It returns a list, which is equal to given list except for element at given index, which has the new value.
The function "len" returns the length of argument.
Also the file "sublist.lisp" contains lemmas of sublists underlying theory. They are defined by construction "defrule". Their names have the prefix "sublist-".

2) "range.lisp". The construction "define" defines a predicate "equal-ranges", which has six arguments: the range of the first list (the first and the second arguments), the range of the second list (the third and the fourth arguments), the first and the second lists (the fifth and the sixth arguments).
The predicate "equal-ranges" detects whether the first sublist is equal to the second sublist at given
ranges.

3) "permut.lisp". The construction "define" defines a predicate "permutation", which has four arguments: the first index, the second index, the first list and the second list. The predicate "permutation" detects whether the first list is a permutation of the second list at given range.
Also the file "permut.lisp" contains lemmas of permutation underlying theory. They are defined by construction "defrule". Their names have the prefix "permutation-".

4) "ordered.lisp". The construction "define" defines a predicate "ordered", which has three arguments: the first index, the second index, the first list and integer list. The predicate "ordered" detects whether integer list is sorted in nondecreasing way at given range.
Also the file "ordered.lisp" contains lemmas of sorting underlying theory. They are defined by construction "defrule". Their names have the prefix "ordered-". These lemmas have predicates "natp", "integerp" and "integer-listp". The predicate "natp" checks whether the argument is natural number. The predicate "integerp" checks whether the argument is integer number. The predicate "integer-listp" checks whether the argument is integer list.

5) "rep.lisp". The construction "defprod" creates a type of data structure. By this construction the types "frame" and "envir" were defined. All objects of the type "frame" are named "fr". The object "fr" is a structure corresponding to a vector v of modifying variables. The object "fr" has a boolean field "loop-break", integer field "j" and a field "a", whose type is integer list.
The field "loop-break" stores an information whether a loop exit occurred. The field "j" corresponds to loop counter of program. The field "a" corresponds to array.
The function "frame->loop-break" takes an object of type "frame" and returns the value of its field "loop-break".
The function "frame->j" takes an object of type "frame" and returns the value of its field "j".
The function "frame->a" takes an object of type "frame" and returns the value of its field "a".
The function "frame-init" creates an object of type "frame" with given field values.
The function "frame-fix" casts an argument to the type "frame".
The function "frame-p" checks whether the argument is an object of type "frame".
All objects of the type "envir" are named "env". The object "env" is a structure corresponding to a vector of non-modifying variables in loop body. This object has an integer field "upper-bound" and integer field "k".
The field "upper-bound" stores an information about inaccessible upper bound of loop counter (it is variable "i" in the file "vc-3.lisp"). The field "k" corresponds to program variable "k".
The function "envir->upper-bound" takes an object of type "envir" and returns the value of its field "upper-bound". The function "envir->k" takes an object of type "envir" and returns the value of its field "k".
The function "envir-init" creates an object of type "envir" with given field values. The function "envir-fix" casts an argument to the type "envir". The function "envir-p" checks whether the argument is an object of type "envir".
Also the file "rep.lisp" contains lemmas of replacement operation. They are defined by construction "defrule". Their names have the prefix "rep-lemma-".

6) "vc-1.lisp" contains verification condition "vc-1" (from program start to outer loop) defined by construction "defrule". It is a logical formula containing logical operations "not", "or", "and", "implies" written in prefix form.
This verification condition was proved automatically in ACL2.

7) "vc-2.lisp" contains verification condition "vc-2" (from outer loop exit to program end) defined by construction "defrule". This formula is put into construction "with-arith5-help", which tells ACL2 to apply a library "arithmetic-5". In our case it helps to work with set of inequalities.
This verification condition was proved automatically in ACL2.

8) "vc-3.lisp" contains verification condition "vc-3" (inside outer loop) defined by construction "defrule". The predicates "permutation" and "ordered" are used, so the file "vc-3.lisp" contains constructions "(include-book "permut")" and "(include-book "ordered")".
Also the file "vc-3.lisp" contains lemmas, which allow to prove "vc-3". They are defined by construction "defrule". Their names have the prefix "vc-3-lemma-".
This verification condition was proved automatically in ACL2.
