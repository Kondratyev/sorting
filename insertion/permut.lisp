(in-package "ACL2")

(include-book "textbook/chap11/perm" :dir :system)
(include-book "textbook/chap11/perm-append" :dir :system)

(defthm perm-append
    (perm (append x y) (append y x)))

(include-book "std/util/defrule" :dir :system)
(include-book "centaur/fty/top" :dir :system)
(include-book "std/util/bstar" :dir :system)
(include-book "std/lists/top" :dir :system)
(include-book "std/basic/inductions" :dir :system)

(include-book "sublist")
(include-book "range")

(defrule perm-reflexive
    (perm x x))

(defthm perm-symm
    (implies (perm x y) (perm y x)))

(defthm perm-transit
    (implies (and (perm x y) (perm y z)) (perm x z))
    :hints (("Goal" :induct (and (perm x y) (perm x z)))))

(defrule perm-concat
    (implies
        (and
            (perm x y)
            (perm u v)
        )
        (perm
            (append x u)
            (append y v)
        )
    )
    :disable (perm-append)
    :hints (("Goal" :induct (perm x y)))
)

(defrule perm-concat-2
    (implies
        (and
            (perm x y)
            (perm u v)
        )
        (perm
            (append x u)
            (append v y)
        )
    )
    :do-not-induct t
    :disable (perm-append
              perm-transit
              perm-concat)
    :use (perm-concat
          (:instance perm-transit
           (x (append x u))
           (y (append y v))
           (z (append v y)))
          (:instance perm-append
           (x y)
           (y v)))
)

(verify-guards in)

(verify-guards del)

(verify-guards perm)

(define permutation
    (
        (i integerp)
        (j integerp)
        (u true-listp)
        (v true-listp)
    )
    :returns (result booleanp)
    (b*
        (
            (i (ifix i))
            (j (ifix j))
            (u (list-fix u))
            (v (list-fix v))
        )
        (perm (sublist i j u) (sublist i j v))
    )
    ///
    (fty::deffixequiv permutation))

(defrule permutation-1
    (implies
        (and
            (natp i)
            (natp j)
            (true-listp u)
            (true-listp v)
            (equal
                (sublist i j u)
                (sublist i j v)
            )
        )
        (permutation i j u v)
    )
    :use ((:instance perm-reflexive
          (x (sublist i j u))))
    :enable permutation
    :disable (sublist sublist-1)
)

(defrule permutation-2
    (implies
        (and
            (natp i)
            (natp j)
            (true-listp u)
            (true-listp v)
            (equal u v)
        )
        (permutation i j u v)
    )
    :use (sublist-1 permutation-1)
    :disable (sublist permutation)
)

(defrule permutation-3
    (implies
        (and
            (natp i)
            (natp j)
            (true-listp u)
        )
        (permutation i j u u)
    )
    :use ((:instance permutation-2
          (i i) (j j) (u u) (v u)))
    :disable (sublist permutation)
)

(defrule permutation-5
    (implies
        (and
            (natp i)
            (natp j)
            (true-listp u)
            (true-listp v)
            (permutation i j u v)
        )
        (permutation i j v u)
    )
    :use ((:instance perm-symm
          (x (sublist i j u)) (y (sublist i j v))))
    :enable permutation
    :disable (sublist sublist-1)
)

(defrule permutation-6
    (implies
        (and
            (natp i)
            (natp j)
            (true-listp u)
            (true-listp v)
            (true-listp w)
            (permutation i j u v)
            (permutation i j v w)
        )
        (permutation i j u w)
    )
    :use ((:instance perm-transit
          (x (sublist i j u)) (y (sublist i j v)) (z (sublist i j w))))
    :enable permutation
    :disable (sublist sublist-1)
)

(defrule permutation-7
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (true-listp u)
            (true-listp v)
            (<= i k)
            (< k j)
            (< j (len u))
            (< j (len v))
            (permutation i k u v)
            (permutation (+ k 1) j u v)
        )
        (permutation i j u v)
    )
    :hints (("Goal" :expand
             (
              (permutation i k u v)
              (permutation (+ k 1) j u v)
              (permutation i j u v)
             )
             :use
             (sublist-14
              (:instance perm-concat
               (x (sublist i k u))
               (y (sublist i k v))
               (u (sublist (+ k 1) j u))
               (v (sublist (+ k 1) j v))
              )
              (:instance sublist-14
               (u v)
              )
             ))
           )
    :enable (permutation)
    :disable (sublist sublist-1 sublist-2 sublist-13
              sublist-14 perm-concat)
)

(local
(defrule permutation-lemma-1
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (<= k l)
            (<= l j)
            (< j (len u))
            (< (- k 1) i)
        )
        (equal
            (sublist i j u)
            (append
                (sublist i (- k 1) u)
                (sublist k j u)
            )
        )
    )
    :do-not-induct t
    :enable (permutation sublist)
    :disable (sublist-1 sublist-2 sublist-13
              sublist-14)
    :hints (("Goal" :expand
                    ((sublist i (- k 1) u))))
)
)

(local
(defrule permutation-lemma-2
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (<= k l)
            (<= l j)
            (< j (len u))
        )
        (equal
            (sublist i j u)
            (append
                (sublist i (- k 1) u)
                (sublist k j u)
            )
        )
    )
    :do-not-induct t
    :enable (permutation)
    :disable (sublist sublist-1 sublist-2 sublist-13
              sublist-14 permutation-lemma-1)
    :hints (("Goal" :cases ((< i k)))
            ("Subgoal 2" :use permutation-lemma-1)
            ("Subgoal 1" :use
                         (:instance sublist-14
                         (k (- k 1)))))
)
)

(local
(defrule permutation-lemma-3
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (<= k l)
            (<= l j)
            (< j (len u))
            (< j (+ l 1))
        )
        (equal
            (sublist k j u)
            (append
                (sublist k l u)
                (sublist (+ l 1) j u)
            )
        )
    )
    :do-not-induct t
    :enable (permutation sublist)
    :disable (sublist-1 sublist-2 sublist-13
              sublist-14 permutation-lemma-1
              permutation-lemma-2)
    :hints (("Goal" :expand
                    ((sublist (+ l 1) j u))))
)
)

(local
(defrule permutation-lemma-4
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (<= k l)
            (<= l j)
            (< j (len u))
        )
        (equal
            (sublist k j u)
            (append
                (sublist k l u)
                (sublist (+ l 1) j u)
            )
        )
    )
    :do-not-induct t
    :enable (permutation)
    :disable (sublist sublist-1 sublist-2 sublist-13
              sublist-14 permutation-lemma-1
              permutation-lemma-2 permutation-lemma-3)
    :hints (("Goal" :cases ((< l j)))
            ("Subgoal 2" :use permutation-lemma-3)
            ("Subgoal 1" :use
                         (:instance sublist-14
                         (i k)
                         (k l))))
)
)

(local
(defrule permutation-lemma-5
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (<= k l)
            (<= l j)
            (< j (len u))
            (< (- k 1) i)
        )
        (equal
            (sublist i (- k 1) u)
            (sublist i (- k 1)
                (update-nth
                    k
                    (nth l u)
                    (update-nth
                        l
                        (nth k u)
                        u
                    )
                )
            )
        )
    )
    :do-not-induct t
    :enable (sublist)
    :disable (sublist-1 sublist-2 sublist-13
              sublist-14 permutation-lemma-1
              permutation-lemma-2 permutation-lemma-3
              permutation-lemma-4)
)
)

(defrule permutation-lemma-6
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (<= k l)
            (<= l j)
            (< j (len u))
            (< i k)
            (natp bound)
            (<= i bound)
            (<= bound (- k 1))
        )
        (equal-ranges
            bound
            (- k 1)
            bound
            (- k 1)
            u
            (update-nth
                k
                (nth l u)
                (update-nth
                    l
                    (nth k u)
                    u
                )
            )
        )
    )
    :induct (inc-induct bound k)
    :enable (equal-ranges)
    :disable (sublist sublist-1 sublist-2 sublist-13
              sublist-14 permutation-lemma-1
              permutation-lemma-2 permutation-lemma-3
              permutation-lemma-4 permutation-lemma-5)
    :hints (("Subgoal *1/1" :cases ((<= (+ bound 1) (- k 1))))
            ("Subgoal *1/1.2" :expand
                (equal-ranges
                    bound
                    (- k 1)
                    bound
                    (- k 1)
                    u
                    (update-nth
                        k
                        (nth l u)
                        (update-nth
                            l
                            (nth k u)
                            u
                        )
                    )
                )
                :use
                ((:instance sublist-9
                 (i (- k 1)))))
            ("Subgoal *1/1.1" :expand
                (equal-ranges
                    bound
                    (- k 1)
                    bound
                    (- k 1)
                    u
                    (update-nth
                        k
                        (nth l u)
                        (update-nth
                            l
                            (nth k u)
                            u
                        )
                    )
                )))
)

(defrule permutation-lemma-7
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (<= k l)
            (<= l j)
            (< j (len u))
            (< i k)
        )
        (equal-ranges
            i
            (- k 1)
            i
            (- k 1)
            u
            (update-nth
                k
                (nth l u)
                (update-nth
                    l
                    (nth k u)
                    u
                )
            )
        )
    )
    :do-not-induct t
    :disable (sublist sublist-1 sublist-2 sublist-13
              sublist-14 equal-ranges permutation-lemma-1
              permutation-lemma-2 permutation-lemma-3
              permutation-lemma-4 permutation-lemma-5
              permutation-lemma-6)
    :use ((:instance permutation-lemma-6 (bound i)))
)

(local
(defrule permutation-lemma-8
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (<= k l)
            (<= l j)
            (< j (len u))
            (< i k)
        )
        (equal
            (sublist i (- k 1) u)
            (sublist i (- k 1)
                (update-nth
                    k
                    (nth l u)
                    (update-nth
                        l
                        (nth k u)
                        u
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (sublist sublist-1 sublist-2 sublist-13
              sublist-14 equal-ranges equal-ranges-sublists
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7)
    :use (permutation-lemma-7
          (:instance equal-ranges-sublists
           (i1 i)
           (j1 (- k 1))
           (i2 i)
           (j2 (- k 1))
           (l1 u)
           (l2 (update-nth
                    k
                    (nth l u)
                    (update-nth
                        l
                        (nth k u)
                        u
                    )
                ))))
)
)

(local
(defrule permutation-lemma-9
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (<= k l)
            (<= l j)
            (< j (len u))
        )
        (equal
            (sublist i (- k 1) u)
            (sublist i (- k 1)
                (update-nth
                    k
                    (nth l u)
                    (update-nth
                        l
                        (nth k u)
                        u
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (sublist sublist-1 sublist-2 sublist-13
              sublist-14 equal-ranges equal-ranges-sublists
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8)
    :hints (("Goal" :cases ((< i k)))
            ("Subgoal 2" :use permutation-lemma-5)
            ("Subgoal 1" :use permutation-lemma-8))
)
)

(local
(defrule permutation-lemma-10
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (<= k l)
            (<= l j)
            (< j (len u))
            (< j (+ l 1))
        )
        (equal
            (sublist (+ l 1) j u)
            (sublist (+ l 1) j
                (update-nth
                    k
                    (nth l u)
                    (update-nth
                        l
                        (nth k u)
                        u
                    )
                )
            )
        )
    )
    :do-not-induct t
    :enable (sublist)
    :disable (sublist-1 sublist-2 sublist-13 sublist-14
              equal-ranges equal-ranges-sublists
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9)
)
)

(defrule permutation-lemma-11
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (<= k l)
            (<= l j)
            (< j (len u))
            (<= (+ l 1) j)
            (natp bound)
            (<= (+ l 1) bound)
            (<= bound j)
        )
        (equal-ranges
            bound
            j
            bound
            j
            u
            (update-nth
                k
                (nth l u)
                (update-nth
                    l
                    (nth k u)
                    u
                )
            )
        )
    )
    :induct (inc-induct bound (+ j 1))
    :enable (equal-ranges)
    :disable (sublist-1 sublist-2 sublist-13 sublist-14
              equal-ranges-sublists
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10)
    :hints (("Subgoal *1/1" :cases ((<= (+ bound 1) j)))
            ("Subgoal *1/1.2" :expand
                (equal-ranges
                    bound
                    j
                    bound
                    j
                    u
                    (update-nth
                        k
                        (nth l u)
                        (update-nth
                            l
                            (nth k u)
                            u
                        )
                    )
                )
                :use
                ((:instance sublist-9
                 (i j))))
            ("Subgoal *1/1.1" :expand
                (equal-ranges
                    bound
                    j
                    bound
                    j
                    u
                    (update-nth
                        k
                        (nth l u)
                        (update-nth
                            l
                            (nth k u)
                            u
                        )
                    )
                )))
)

(defrule permutation-lemma-12
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (<= k l)
            (<= l j)
            (< j (len u))
            (<= (+ l 1) j)
        )
        (equal-ranges
            (+ l 1)
            j
            (+ l 1)
            j
            u
            (update-nth
                k
                (nth l u)
                (update-nth
                    l
                    (nth k u)
                    u
                )
            )
        )
    )
    :do-not-induct t
    :disable (sublist-1 sublist-2 sublist-13 sublist-14
              equal-ranges equal-ranges-sublists
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11)
    :use ((:instance permutation-lemma-11 (bound (+ l 1))))
)

(defrule permutation-lemma-13
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (<= k l)
            (<= l j)
            (< j (len u))
            (<= (+ l 1) j)
        )
        (equal
            (sublist (+ l 1) j u)
            (sublist (+ l 1) j
                (update-nth
                    k
                    (nth l u)
                    (update-nth
                        l
                        (nth k u)
                        u
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (sublist sublist-1 sublist-2 sublist-13
              sublist-14 equal-ranges equal-ranges-sublists
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12)
    :use (permutation-lemma-12
          (:instance equal-ranges-sublists
           (i1 (+ l 1))
           (j1 j)
           (i2 (+ l 1))
           (j2 j)
           (l1 u)
           (l2 (update-nth
                    k
                    (nth l u)
                    (update-nth
                        l
                        (nth k u)
                        u
                    )
                ))))
)

(local
(defrule permutation-lemma-14
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (<= k l)
            (<= l j)
            (< j (len u))
        )
        (equal
            (sublist (+ l 1) j u)
            (sublist (+ l 1) j
                (update-nth
                    k
                    (nth l u)
                    (update-nth
                        l
                        (nth k u)
                        u
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (sublist sublist-1 sublist-2 sublist-13
              sublist-14 equal-ranges equal-ranges-sublists
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13)
    :hints (("Goal" :cases ((<= (+ l 1) j)))
            ("Subgoal 2" :use permutation-lemma-10)
            ("Subgoal 1" :use permutation-lemma-13))
)
)

(defrule permutation-lemma-15
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (<= k l)
            (<= l j)
            (< j (len u))
            (< (- k 1) i)
        )
        (permutation
            i
            (- k 1)
            u
            (update-nth
                k
                (nth l u)
                (update-nth
                    l
                    (nth k u)
                    u
                )
            )
        )
    )
    :do-not-induct t
    :enable (permutation sublist)
    :disable (sublist-1 sublist-2 sublist-13 sublist-14
              equal-ranges-sublists equal-ranges permutation-1
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14)
)

(defrule permutation-lemma-16
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (<= k l)
            (<= l j)
            (< j (len u))
        )
        (permutation
            i
            (- k 1)
            u
            (update-nth
                k
                (nth l u)
                (update-nth
                    l
                    (nth k u)
                    u
                )
            )
        )
    )
    :do-not-induct t
    :disable (sublist sublist-1 sublist-2 sublist-13
              sublist-14 equal-ranges-sublists
              equal-ranges permutation permutation-1
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15)
    :hints (("Goal" :cases ((< i k)))
            ("Subgoal 2" :use (permutation-lemma-15))
            ("Subgoal 1" :use (permutation-lemma-9
                (:instance permutation-1
                    (i i)
                    (j (- k 1))
                    (u u)
                    (v (update-nth
                        k
                        (nth l u)
                        (update-nth
                            l
                            (nth k u)
                            u
                        )
                     ))))))
)

(defrule permutation-lemma-17
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (<= k l)
            (<= l j)
            (< j (len u))
            (< j (+ l 1))
        )
        (permutation
            (+ l 1)
            j
            u
            (update-nth
                k
                (nth l u)
                (update-nth
                    l
                    (nth k u)
                    u
                )
            )
        )
    )
    :do-not-induct t
    :enable (permutation sublist)
    :disable (sublist-1 sublist-2 sublist-13 sublist-14
              equal-ranges-sublists equal-ranges permutation-1
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16)
)

(defrule permutation-lemma-18
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (<= k l)
            (<= l j)
            (< j (len u))
        )
        (permutation
            (+ l 1)
            j
            u
            (update-nth
                k
                (nth l u)
                (update-nth
                    l
                    (nth k u)
                    u
                )
            )
        )
    )
    :do-not-induct t
    :disable (sublist sublist-1 sublist-2 sublist-13
              sublist-14 equal-ranges-sublists
              equal-ranges permutation permutation-1
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17)
    :hints (("Goal" :cases ((<= (+ l 1) j)))
            ("Subgoal 2" :use (permutation-lemma-17))
            ("Subgoal 1" :use (permutation-lemma-14
                (:instance permutation-1
                    (i (+ l 1))
                    (j j)
                    (u u)
                    (v (update-nth
                        k
                        (nth l u)
                        (update-nth
                            l
                            (nth k u)
                            u
                        )
                     ))))))
)

(local
(defrule permutation-lemma-19
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (<= k l)
            (<= l j)
            (< j (len u))
            (< l (+ k 1))
        )
        (equal
            (sublist k l u)
            (append
                (sublist k k u)
                (sublist (+ k 1) l u)
            )
        )
    )
    :do-not-induct t
    :enable (permutation sublist)
    :disable (sublist-1 sublist-2 sublist-13 sublist-14
              equal-ranges-sublists equal-ranges permutation-1
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18)
    :hints (("Goal" :expand
                    ((sublist (+ k 1) l u))))
)
)

(local
(defrule permutation-lemma-20
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (<= k l)
            (<= l j)
            (< j (len u))
        )
        (equal
            (sublist k l u)
            (append
                (sublist k k u)
                (sublist (+ k 1) l u)
            )
        )
    )
    :do-not-induct t
    :disable (sublist sublist-1 sublist-2 sublist-13
              sublist-14 equal-ranges-sublists
              equal-ranges permutation permutation-1
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18
              permutation-lemma-19)
    :hints (("Goal" :cases ((< k l)))
            ("Subgoal 2" :use permutation-lemma-19)
            ("Subgoal 1" :use
                         (:instance sublist-14
                          (i k)
                          (j l)))))
)
)

(defrule permutation-lemma-21
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (< k l)
            (<= l j)
            (< j (len u))
            (<= l (+ k 1))
        )
        (equal
            (sublist (+ k 1) l u)
            (append
                (sublist (+ k 1) (- l 1) u)
                (sublist l l u)
            )
        )
    )
    :do-not-induct t
    :enable (sublist)
    :disable (sublist-1 sublist-2 sublist-13 sublist-14
              permutation equal-ranges-sublists
              equal-ranges permutation-1
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18
              permutation-lemma-19 permutation-lemma-20)
    :hints (("Goal" :expand
                    ((sublist (+ k 1) (- l 1) u))))
)

(defrule permutation-lemma-22
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (< k l)
            (<= l j)
            (< j (len u))
        )
        (equal
            (sublist (+ k 1) l u)
            (append
                (sublist (+ k 1) (- l 1) u)
                (sublist l l u)
            )
        )
    )
    :do-not-induct t
    :disable (sublist sublist-1 sublist-2 sublist-13
              sublist-14 equal-ranges-sublists
              equal-ranges permutation permutation-1
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18
              permutation-lemma-19 permutation-lemma-20
              permutation-lemma-21)
    :hints (("Goal" :cases ((< (+ k 1) l)))
            ("Subgoal 2" :use permutation-lemma-21)
            ("Subgoal 1" :use
                         (:instance sublist-14
                          (i (+ k 1))
                          (k (- l 1))
                          (j l)))))
)

(local
(defrule permutation-lemma-23
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (<= k l)
            (<= l j)
            (< j (len u))
        )
        (equal
            (sublist k k u)
            (list (nth k u))
        )
    )
    :do-not-induct t
    :disable (sublist sublist-1 sublist-2 sublist-13
              sublist-14 equal-ranges-sublists
              equal-ranges permutation permutation-1
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18
              permutation-lemma-19 permutation-lemma-20
              permutation-lemma-21 permutation-lemma-22)
    :use (:instance sublist-9
          (i k))
)
)

(local
(defrule permutation-lemma-24
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (<= k l)
            (<= l j)
            (< j (len u))
        )
        (equal
            (sublist l l u)
            (list (nth l u))
        )
    )
    :do-not-induct t
    :disable (sublist sublist-1 sublist-2 sublist-13
              sublist-14 equal-ranges-sublists
              equal-ranges permutation permutation-1
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18
              permutation-lemma-19 permutation-lemma-20
              permutation-lemma-21 permutation-lemma-22
              permutation-lemma-23)
    :use (:instance sublist-9
          (i l))
)
)

(defrule permutation-lemma-25
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (< k l)
            (<= l j)
            (< j (len u))
            (natp bound)
            (<= (+ k 1) bound)
            (<= bound (- l 1))
        )
        (equal-ranges
            bound
            (- l 1)
            bound
            (- l 1)
            u
            (update-nth
                k
                (nth l u)
                (update-nth
                    l
                    (nth k u)
                    u
                )
            )
        )
    )
    :induct (inc-induct bound l)
    :enable (equal-ranges)
    :disable (sublist sublist-1 sublist-2 sublist-13
              sublist-14 equal-ranges-sublists
              permutation permutation-1
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18
              permutation-lemma-19 permutation-lemma-20
              permutation-lemma-21 permutation-lemma-22
              permutation-lemma-23)
    :hints (("Subgoal *1/1" :cases ((<= (+ bound 1) (- l 1))))
            ("Subgoal *1/1.2" :expand
                (equal-ranges
                    bound
                    (- l 1)
                    bound
                    (- l 1)
                    u
                    (update-nth
                        k
                        (nth l u)
                        (update-nth
                            l
                            (nth k u)
                            u
                        )
                    )
                )
                :use
                ((:instance sublist-9
                 (i (- l 1)))))
            ("Subgoal *1/1.1" :expand
                (equal-ranges
                    bound
                    (- l 1)
                    bound
                    (- l 1)
                    u
                    (update-nth
                        k
                        (nth l u)
                        (update-nth
                            l
                            (nth k u)
                            u
                        )
                    )
                )))
)

(defrule permutation-lemma-26
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (< (+ k 1) l)
            (<= l j)
            (< j (len u))
        )
        (equal-ranges
            (+ k 1)
            (- l 1)
            (+ k 1)
            (- l 1)
            u
            (update-nth
                k
                (nth l u)
                (update-nth
                    l
                    (nth k u)
                    u
                )
            )
        )
    )
    :do-not-induct t
    :disable (sublist sublist-1 sublist-2 sublist-13
              sublist-14 equal-ranges-sublists
              equal-ranges permutation permutation-1
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18
              permutation-lemma-19 permutation-lemma-20
              permutation-lemma-21 permutation-lemma-22
              permutation-lemma-23 permutation-lemma-24
              permutation-lemma-25)
    :use ((:instance permutation-lemma-25 (bound (+ k 1))))
)

(defrule permutation-lemma-27
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (= (+ k 1) l)
            (<= l j)
            (< j (len u))
        )
        (equal
            (sublist (+ k 1) (- l 1) u)
            (sublist (+ k 1) (- l 1)
                (update-nth
                    k
                    (nth l u)
                    (update-nth
                        l
                        (nth k u)
                        u
                    )
                )
            )
        )
    )
    :do-not-induct t
    :enable (sublist)
    :disable (sublist-1 sublist-2 sublist-13
              sublist-14 equal-ranges-sublists
              equal-ranges permutation permutation-1
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18
              permutation-lemma-19 permutation-lemma-20
              permutation-lemma-21 permutation-lemma-22
              permutation-lemma-23 permutation-lemma-24
              permutation-lemma-25 permutation-lemma-26)
)

(defrule permutation-lemma-28
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (< (+ k 1) l)
            (<= l j)
            (< j (len u))
        )
        (equal
            (sublist (+ k 1) (- l 1) u)
            (sublist (+ k 1) (- l 1)
                (update-nth
                    k
                    (nth l u)
                    (update-nth
                        l
                        (nth k u)
                        u
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (sublist sublist-1 sublist-2 sublist-13
              sublist-14 equal-ranges-sublists
              equal-ranges permutation permutation-1
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18
              permutation-lemma-19 permutation-lemma-20
              permutation-lemma-21 permutation-lemma-22
              permutation-lemma-23 permutation-lemma-24
              permutation-lemma-25 permutation-lemma-26
              permutation-lemma-27)
    :use (permutation-lemma-26
          (:instance equal-ranges-sublists
           (i1 (+ k 1))
           (j1 (- l 1))
           (i2 (+ k 1))
           (j2 (- l 1))
           (l1 u)
           (l2 (update-nth
                    k
                    (nth l u)
                    (update-nth
                        l
                        (nth k u)
                        u
                    )
                ))))
)

(local
(defrule permutation-lemma-29
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (< k l)
            (<= l j)
            (< j (len u))
        )
        (equal
            (sublist (+ k 1) (- l 1) u)
            (sublist (+ k 1) (- l 1)
                (update-nth
                    k
                    (nth l u)
                    (update-nth
                        l
                        (nth k u)
                        u
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (sublist sublist-1 sublist-2 sublist-13
              sublist-14 equal-ranges-sublists
              equal-ranges permutation permutation-1
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18
              permutation-lemma-19 permutation-lemma-20
              permutation-lemma-21 permutation-lemma-22
              permutation-lemma-23 permutation-lemma-24
              permutation-lemma-25 permutation-lemma-26
              permutation-lemma-27 permutation-lemma-28)
    :hints (("Goal" :cases ((< (+ k 1) l)))
            ("Subgoal 2" :use (permutation-lemma-27))
            ("Subgoal 1" :use (permutation-lemma-28)))
)
)

(defrule permutation-lemma-30
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (<= k l)
            (<= l j)
            (< j (len u))
        )
        (equal
            (sublist
                k
                k
                (update-nth
                    k
                    (nth l u)
                    (update-nth
                        l
                        (nth k u)
                        u
                    )
                )
            )
            (list (nth l u))
        )
    )
    :do-not-induct t
    :disable (sublist sublist-1 sublist-2 sublist-13
              sublist-14 equal-ranges-sublists
              equal-ranges permutation permutation-1
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18
              permutation-lemma-19 permutation-lemma-20
              permutation-lemma-21 permutation-lemma-22
              permutation-lemma-23 permutation-lemma-24
              permutation-lemma-25 permutation-lemma-26
              permutation-lemma-27 permutation-lemma-28
              permutation-lemma-29)
    :use ((:instance sublist-9
          (i k)
          (u (update-nth
                    k
                    (nth l u)
                    (update-nth
                        l
                        (nth k u)
                        u
                    )
                ))))
)

(local
(defrule permutation-lemma-31
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (<= k l)
            (<= l j)
            (< j (len u))
        )
        (equal
            (sublist l l u)
            (sublist
                k
                k
                (update-nth
                    k
                    (nth l u)
                    (update-nth
                        l
                        (nth k u)
                        u
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (sublist sublist-1 sublist-2 sublist-13
              sublist-14 equal-ranges-sublists
              equal-ranges permutation permutation-1
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18
              permutation-lemma-19 permutation-lemma-20
              permutation-lemma-21 permutation-lemma-22
              permutation-lemma-23 permutation-lemma-24
              permutation-lemma-25 permutation-lemma-26
              permutation-lemma-27 permutation-lemma-28
              permutation-lemma-29 permutation-lemma-30)
    :use (permutation-lemma-24 permutation-lemma-30)
)
)

(defrule permutation-lemma-32
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (<= k l)
            (<= l j)
            (< j (len u))
        )
        (equal
            (sublist
                l
                l
                (update-nth
                    k
                    (nth l u)
                    (update-nth
                        l
                        (nth k u)
                        u
                    )
                )
            )
            (list (nth k u))
        )
    )
    :do-not-induct t
    :disable (sublist sublist-1 sublist-2 sublist-13
              sublist-14 equal-ranges-sublists
              equal-ranges permutation permutation-1
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18
              permutation-lemma-19 permutation-lemma-20
              permutation-lemma-21 permutation-lemma-22
              permutation-lemma-23 permutation-lemma-24
              permutation-lemma-25 permutation-lemma-26
              permutation-lemma-27 permutation-lemma-28
              permutation-lemma-29 permutation-lemma-30
              permutation-lemma-31)
    :use ((:instance sublist-9
          (i l)
          (u (update-nth
                    k
                    (nth l u)
                    (update-nth
                        l
                        (nth k u)
                        u
                    )
                ))))
)

(local
(defrule permutation-lemma-33
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (<= k l)
            (<= l j)
            (< j (len u))
        )
        (equal
            (sublist k k u)
            (sublist
                l
                l
                (update-nth
                    k
                    (nth l u)
                    (update-nth
                        l
                        (nth k u)
                        u
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (sublist sublist-1 sublist-2 sublist-13
              sublist-14 equal-ranges-sublists
              equal-ranges permutation permutation-1
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18
              permutation-lemma-19 permutation-lemma-20
              permutation-lemma-21 permutation-lemma-22
              permutation-lemma-23 permutation-lemma-24
              permutation-lemma-25 permutation-lemma-26
              permutation-lemma-27 permutation-lemma-28
              permutation-lemma-29 permutation-lemma-30
              permutation-lemma-31 permutation-lemma-32)
    :use (permutation-lemma-23 permutation-lemma-32)
)
)

(local
(defrule permutation-lemma-34
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (<= k l)
            (<= l j)
            (< j (len u))
        )
        (equal
            (len u)
            (len
                (update-nth
                    k
                    (nth l u)
                    (update-nth
                        l
                        (nth k u)
                        u
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (sublist sublist-1 sublist-2 sublist-13
              sublist-14 equal-ranges-sublists
              equal-ranges permutation permutation-1
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18
              permutation-lemma-19 permutation-lemma-20
              permutation-lemma-21 permutation-lemma-22
              permutation-lemma-23 permutation-lemma-24
              permutation-lemma-25 permutation-lemma-26
              permutation-lemma-27 permutation-lemma-28
              permutation-lemma-29 permutation-lemma-30
              permutation-lemma-31 permutation-lemma-32
              permutation-lemma-33)
    :use (permutation-lemma-23 permutation-lemma-32)
)
)

(defrule permutation-lemma-35
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (<= k l)
            (<= l j)
            (< j (len u))
            (< (- k 1) i)
        )
        (equal
            (sublist
                i
                j
                (update-nth
                    k
                    (nth l u)
                    (update-nth
                        l
                        (nth k u)
                        u
                    )
                )
            )
            (append
                (sublist
                    i
                    (- k 1)
                    (update-nth
                        k
                        (nth l u)
                        (update-nth
                            l
                            (nth k u)
                            u
                        )
                    )
                )
                (sublist
                    k
                    j
                    (update-nth
                        k
                        (nth l u)
                        (update-nth
                            l
                            (nth k u)
                            u
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :enable (sublist)
    :disable (sublist-1 sublist-2 sublist-13
              sublist-14 equal-ranges-sublists
              equal-ranges permutation permutation-1
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18
              permutation-lemma-19 permutation-lemma-20
              permutation-lemma-21 permutation-lemma-22
              permutation-lemma-23 permutation-lemma-24
              permutation-lemma-25 permutation-lemma-26
              permutation-lemma-27 permutation-lemma-28
              permutation-lemma-29 permutation-lemma-30
              permutation-lemma-31 permutation-lemma-32
              permutation-lemma-33 permutation-lemma-34)
    :use (permutation-lemma-34)
    :hints (("Goal"
             :expand
              (
                (sublist
                    i
                    (- k 1)
                    (update-nth
                        k
                        (nth l u)
                        (update-nth
                            l
                            (nth k u)
                            u
                        )
                    )
                )
              )))
)

(defrule permutation-lemma-36
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (<= k l)
            (<= l j)
            (< j (len u))
        )
        (equal
            (sublist
                i
                j
                (update-nth
                    k
                    (nth l u)
                    (update-nth
                        l
                        (nth k u)
                        u
                    )
                )
            )
            (append
                (sublist
                    i
                    (- k 1)
                    (update-nth
                        k
                        (nth l u)
                        (update-nth
                            l
                            (nth k u)
                            u
                        )
                    )
                )
                (sublist
                    k
                    j
                    (update-nth
                        k
                        (nth l u)
                        (update-nth
                            l
                            (nth k u)
                            u
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (sublist sublist-1 sublist-2 sublist-13
              sublist-14 equal-ranges-sublists
              equal-ranges permutation permutation-1
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18
              permutation-lemma-19 permutation-lemma-20
              permutation-lemma-21 permutation-lemma-22
              permutation-lemma-23 permutation-lemma-24
              permutation-lemma-25 permutation-lemma-26
              permutation-lemma-27 permutation-lemma-28
              permutation-lemma-29 permutation-lemma-30
              permutation-lemma-31 permutation-lemma-32
              permutation-lemma-33 permutation-lemma-34
              permutation-lemma-35)
    :hints (("Goal" :cases ((< i k)))
            ("Subgoal 2" :use (permutation-lemma-34
                               permutation-lemma-35))
            ("Subgoal 1" :use (permutation-lemma-34
                         (:instance sublist-14
                         (k (- k 1))
                         (u
                            (update-nth
                                k
                                (nth l u)
                                (update-nth
                                    l
                                    (nth k u)
                                    u
                                )
                            )
                         )))))
)

(defrule permutation-lemma-37
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (<= k l)
            (<= l j)
            (< j (len u))
            (< j (+ l 1))
        )
        (equal
            (sublist
                k
                j
                (update-nth
                    k
                    (nth l u)
                    (update-nth
                        l
                        (nth k u)
                        u
                    )
                )
            )
            (append
                (sublist
                    k
                    l
                    (update-nth
                        k
                        (nth l u)
                        (update-nth
                            l
                            (nth k u)
                            u
                        )
                    )
                )
                (sublist
                    (+ l 1)
                    j
                    (update-nth
                        k
                        (nth l u)
                        (update-nth
                            l
                            (nth k u)
                            u
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :enable (sublist)
    :disable (sublist-1 sublist-2 sublist-13
              sublist-14 equal-ranges-sublists
              equal-ranges permutation permutation-1
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18
              permutation-lemma-19 permutation-lemma-20
              permutation-lemma-21 permutation-lemma-22
              permutation-lemma-23 permutation-lemma-24
              permutation-lemma-25 permutation-lemma-26
              permutation-lemma-27 permutation-lemma-28
              permutation-lemma-29 permutation-lemma-30
              permutation-lemma-31 permutation-lemma-32
              permutation-lemma-33 permutation-lemma-34
              permutation-lemma-35 permutation-lemma-36)
    :use (permutation-lemma-34)
    :hints (("Goal"
             :expand
              (
                (sublist
                    (+ l 1)
                    j
                    (update-nth
                        k
                        (nth l u)
                        (update-nth
                            l
                            (nth k u)
                            u
                        )
                    )
                )
              )))
)

(defrule permutation-lemma-38
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (<= k l)
            (<= l j)
            (< j (len u))
        )
        (equal
            (sublist
                k
                j
                (update-nth
                    k
                    (nth l u)
                    (update-nth
                        l
                        (nth k u)
                        u
                    )
                )
            )
            (append
                (sublist
                    k
                    l
                    (update-nth
                        k
                        (nth l u)
                        (update-nth
                            l
                            (nth k u)
                            u
                        )
                    )
                )
                (sublist
                    (+ l 1)
                    j
                    (update-nth
                        k
                        (nth l u)
                        (update-nth
                            l
                            (nth k u)
                            u
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (sublist sublist-1 sublist-2 sublist-13
              sublist-14 equal-ranges-sublists
              equal-ranges permutation permutation-1
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18
              permutation-lemma-19 permutation-lemma-20
              permutation-lemma-21 permutation-lemma-22
              permutation-lemma-23 permutation-lemma-24
              permutation-lemma-25 permutation-lemma-26
              permutation-lemma-27 permutation-lemma-28
              permutation-lemma-29 permutation-lemma-30
              permutation-lemma-31 permutation-lemma-32
              permutation-lemma-33 permutation-lemma-34
              permutation-lemma-35 permutation-lemma-36
              permutation-lemma-37)
    :hints (("Goal" :cases ((< l j)))
            ("Subgoal 2" :use (permutation-lemma-34
                               permutation-lemma-37))
            ("Subgoal 1" :use (permutation-lemma-34
                         (:instance sublist-14
                         (i k)
                         (k l)
                         (u
                            (update-nth
                                k
                                (nth l u)
                                (update-nth
                                    l
                                    (nth k u)
                                    u
                                )
                            )
                         )))))
)

(defrule permutation-lemma-39
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (<= k l)
            (<= l j)
            (< j (len u))
        )
        (perm
            (sublist k k u)
            (sublist
                l
                l
                (update-nth
                    k
                    (nth l u)
                    (update-nth
                        l
                        (nth k u)
                        u
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (sublist sublist-1 sublist-2 sublist-13
              sublist-14 equal-ranges-sublists equal-ranges
              perm-reflexive permutation permutation-1
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18
              permutation-lemma-19 permutation-lemma-20
              permutation-lemma-21 permutation-lemma-22
              permutation-lemma-23 permutation-lemma-24
              permutation-lemma-25 permutation-lemma-26
              permutation-lemma-27 permutation-lemma-28
              permutation-lemma-29 permutation-lemma-30
              permutation-lemma-31 permutation-lemma-32
              permutation-lemma-33 permutation-lemma-34
              permutation-lemma-35 permutation-lemma-36
              permutation-lemma-37 permutation-lemma-38)
    :use (permutation-lemma-33
          (:instance perm-reflexive (x (sublist k k u))))
)

(defrule permutation-lemma-40
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (<= k l)
            (<= l j)
            (< j (len u))
        )
        (perm
            (sublist l l u)
            (sublist
                k
                k
                (update-nth
                    k
                    (nth l u)
                    (update-nth
                        l
                        (nth k u)
                        u
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (sublist sublist-1 sublist-2 sublist-13
              sublist-14 equal-ranges-sublists equal-ranges
              perm-reflexive permutation permutation-1
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18
              permutation-lemma-19 permutation-lemma-20
              permutation-lemma-21 permutation-lemma-22
              permutation-lemma-23 permutation-lemma-24
              permutation-lemma-25 permutation-lemma-26
              permutation-lemma-27 permutation-lemma-28
              permutation-lemma-29 permutation-lemma-30
              permutation-lemma-31 permutation-lemma-32
              permutation-lemma-33 permutation-lemma-34
              permutation-lemma-35 permutation-lemma-36
              permutation-lemma-37 permutation-lemma-38
              permutation-lemma-39)
    :use (permutation-lemma-31
          (:instance perm-reflexive (x (sublist l l u))))
)

(defrule permutation-lemma-41
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (< k l)
            (<= l j)
            (< j (len u))
        )
        (perm
            (sublist (+ k 1) (- l 1) u)
            (sublist
                (+ k 1)
                (- l 1)
                (update-nth
                    k
                    (nth l u)
                    (update-nth
                        l
                        (nth k u)
                        u
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (sublist sublist-1 sublist-2 sublist-13
              sublist-14 equal-ranges-sublists equal-ranges
              perm-reflexive permutation permutation-1
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18
              permutation-lemma-19 permutation-lemma-20
              permutation-lemma-21 permutation-lemma-22
              permutation-lemma-23 permutation-lemma-24
              permutation-lemma-25 permutation-lemma-26
              permutation-lemma-27 permutation-lemma-28
              permutation-lemma-29 permutation-lemma-30
              permutation-lemma-31 permutation-lemma-32
              permutation-lemma-33 permutation-lemma-34
              permutation-lemma-35 permutation-lemma-36
              permutation-lemma-37 permutation-lemma-38
              permutation-lemma-39 permutation-lemma-40)
    :use (permutation-lemma-29
          (:instance perm-reflexive (x (sublist (+ k 1) (- l 1) u))))
)

(defrule permutation-lemma-42
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (<= k l)
            (<= l j)
            (< j (len u))
            (< l (+ k 1))
        )
        (equal
            (sublist
                k
                l
                (update-nth
                    k
                    (nth l u)
                    (update-nth
                        l
                        (nth k u)
                        u
                    )
                )
            )
            (append
                (sublist
                    k
                    k
                    (update-nth
                        k
                        (nth l u)
                        (update-nth
                            l
                            (nth k u)
                            u
                        )
                    )
                )
                (sublist
                    (+ k 1)
                    l
                    (update-nth
                        k
                        (nth l u)
                        (update-nth
                            l
                            (nth k u)
                            u
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :enable (sublist)
    :disable (sublist-1 sublist-2 sublist-9 sublist-13
              sublist-14 equal-ranges-sublists equal-ranges
              perm-reflexive permutation permutation-1
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18
              permutation-lemma-19 permutation-lemma-20
              permutation-lemma-21 permutation-lemma-22
              permutation-lemma-23 permutation-lemma-24
              permutation-lemma-25 permutation-lemma-26
              permutation-lemma-27 permutation-lemma-28
              permutation-lemma-29 permutation-lemma-30
              permutation-lemma-31 permutation-lemma-32
              permutation-lemma-33 permutation-lemma-34
              permutation-lemma-35 permutation-lemma-36
              permutation-lemma-37 permutation-lemma-38
              permutation-lemma-39 permutation-lemma-40
              permutation-lemma-41)
    :use (permutation-lemma-34)
    :hints (("Goal"
             :expand
              (
                (sublist
                    (+ k 1)
                    l
                    (update-nth
                        k
                        (nth l u)
                        (update-nth
                            l
                            (nth k u)
                            u
                        )
                    )
                )
              )))
)

(defrule permutation-lemma-43
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (<= k l)
            (<= l j)
            (< j (len u))
        )
        (equal
            (sublist
                k
                l
                (update-nth
                    k
                    (nth l u)
                    (update-nth
                        l
                        (nth k u)
                        u
                    )
                )
            )
            (append
                (sublist
                    k
                    k
                    (update-nth
                        k
                        (nth l u)
                        (update-nth
                            l
                            (nth k u)
                            u
                        )
                    )
                )
                (sublist
                    (+ k 1)
                    l
                    (update-nth
                        k
                        (nth l u)
                        (update-nth
                            l
                            (nth k u)
                            u
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (sublist sublist-1 sublist-2 sublist-9 sublist-13
              sublist-14 equal-ranges-sublists equal-ranges
              perm-reflexive permutation permutation-1
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18
              permutation-lemma-19 permutation-lemma-20
              permutation-lemma-21 permutation-lemma-22
              permutation-lemma-23 permutation-lemma-24
              permutation-lemma-25 permutation-lemma-26
              permutation-lemma-27 permutation-lemma-28
              permutation-lemma-29 permutation-lemma-30
              permutation-lemma-31 permutation-lemma-32
              permutation-lemma-33 permutation-lemma-34
              permutation-lemma-35 permutation-lemma-36
              permutation-lemma-37 permutation-lemma-38
              permutation-lemma-39 permutation-lemma-40
              permutation-lemma-41 permutation-lemma-42)
    :use (permutation-lemma-34)
    :hints (("Goal" :cases ((< k l)))
            ("Subgoal 2" :use (permutation-lemma-34
                               permutation-lemma-42))
            ("Subgoal 1" :use (permutation-lemma-34
                         (:instance sublist-14
                         (i k)
                         (k k)
                         (j l)
                         (u
                            (update-nth
                                k
                                (nth l u)
                                (update-nth
                                    l
                                    (nth k u)
                                    u
                                )
                            )
                         )))))
)

(defrule permutation-lemma-44
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (< k l)
            (<= l j)
            (< j (len u))
            (<= l (+ k 1))
        )
        (equal
            (sublist
                (+ k 1)
                l
                (update-nth
                    k
                    (nth l u)
                    (update-nth
                        l
                        (nth k u)
                        u
                    )
                )
            )
            (append
                (sublist
                    (+ k 1)
                    (- l 1)
                    (update-nth
                        k
                        (nth l u)
                        (update-nth
                            l
                            (nth k u)
                            u
                        )
                    )
                )
                (sublist
                    l
                    l
                    (update-nth
                        k
                        (nth l u)
                        (update-nth
                            l
                            (nth k u)
                            u
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :enable (sublist)
    :disable (sublist-1 sublist-2 sublist-9 sublist-13
              sublist-14 equal-ranges-sublists equal-ranges
              perm-reflexive permutation permutation-1
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18
              permutation-lemma-19 permutation-lemma-20
              permutation-lemma-21 permutation-lemma-22
              permutation-lemma-23 permutation-lemma-24
              permutation-lemma-25 permutation-lemma-26
              permutation-lemma-27 permutation-lemma-28
              permutation-lemma-29 permutation-lemma-30
              permutation-lemma-31 permutation-lemma-32
              permutation-lemma-33 permutation-lemma-34
              permutation-lemma-35 permutation-lemma-36
              permutation-lemma-37 permutation-lemma-38
              permutation-lemma-39 permutation-lemma-40
              permutation-lemma-41 permutation-lemma-42
              permutation-lemma-43)
    :use (permutation-lemma-34)
    :hints (("Goal"
             :expand
              (
                (sublist
                    (+ k 1)
                    (- l 1)
                    (update-nth
                        k
                        (nth l u)
                        (update-nth
                            l
                            (nth k u)
                            u
                        )
                    )
                )
              )))
)

(defrule permutation-lemma-45
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (< k l)
            (<= l j)
            (< j (len u))
        )
        (equal
            (sublist
                (+ k 1)
                l
                (update-nth
                    k
                    (nth l u)
                    (update-nth
                        l
                        (nth k u)
                        u
                    )
                )
            )
            (append
                (sublist
                    (+ k 1)
                    (- l 1)
                    (update-nth
                        k
                        (nth l u)
                        (update-nth
                            l
                            (nth k u)
                            u
                        )
                    )
                )
                (sublist
                    l
                    l
                    (update-nth
                        k
                        (nth l u)
                        (update-nth
                            l
                            (nth k u)
                            u
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (sublist sublist-1 sublist-2 sublist-9 sublist-13
              sublist-14 equal-ranges-sublists equal-ranges
              perm-reflexive permutation permutation-1
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18
              permutation-lemma-19 permutation-lemma-20
              permutation-lemma-21 permutation-lemma-22
              permutation-lemma-23 permutation-lemma-24
              permutation-lemma-25 permutation-lemma-26
              permutation-lemma-27 permutation-lemma-28
              permutation-lemma-29 permutation-lemma-30
              permutation-lemma-31 permutation-lemma-32
              permutation-lemma-33 permutation-lemma-34
              permutation-lemma-35 permutation-lemma-36
              permutation-lemma-37 permutation-lemma-38
              permutation-lemma-39 permutation-lemma-40
              permutation-lemma-41 permutation-lemma-42
              permutation-lemma-43 permutation-lemma-44)
    :hints (("Goal" :cases ((< (+ k 1) l)))
            ("Subgoal 2" :use (permutation-lemma-34
                               permutation-lemma-44))
            ("Subgoal 1" :use (permutation-lemma-34
                         (:instance sublist-14
                         (i (+ k 1))
                         (k (- l 1))
                         (j l)
                         (u
                            (update-nth
                                k
                                (nth l u)
                                (update-nth
                                    l
                                    (nth k u)
                                    u
                                )
                            )
                         )))))
)

(local
(defrule permutation-lemma-46
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (= k l)
            (<= l j)
            (< j (len u))
        )
        (equal
            (sublist k l u)
            (sublist
                k
                l
                (update-nth
                    k
                    (nth l u)
                    (update-nth
                        l
                        (nth k u)
                        u
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (sublist sublist-1 sublist-2 sublist-9 sublist-13
              sublist-14 equal-ranges-sublists equal-ranges
              perm-reflexive permutation permutation-1
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18
              permutation-lemma-19 permutation-lemma-20
              permutation-lemma-21 permutation-lemma-22
              permutation-lemma-23 permutation-lemma-24
              permutation-lemma-25 permutation-lemma-26
              permutation-lemma-27 permutation-lemma-28
              permutation-lemma-29 permutation-lemma-30
              permutation-lemma-31 permutation-lemma-32
              permutation-lemma-33 permutation-lemma-34
              permutation-lemma-35 permutation-lemma-36
              permutation-lemma-37 permutation-lemma-38
              permutation-lemma-39 permutation-lemma-40
              permutation-lemma-41 permutation-lemma-42
              permutation-lemma-43 permutation-lemma-44
              permutation-lemma-45)
    :use (permutation-lemma-31 permutation-lemma-33)
)
)

(defrule permutation-lemma-47
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (= k l)
            (<= l j)
            (< j (len u))
        )
        (permutation
            k
            l
            u
            (update-nth
                k
                (nth l u)
                (update-nth
                    l
                    (nth k u)
                    u
                )
            )
        )
    )
    :do-not-induct t
    :enable (permutation)
    :disable (sublist sublist-1 sublist-2 sublist-9 sublist-13
              sublist-14 equal-ranges-sublists equal-ranges
              perm-reflexive permutation-1
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18
              permutation-lemma-19 permutation-lemma-20
              permutation-lemma-21 permutation-lemma-22
              permutation-lemma-23 permutation-lemma-24
              permutation-lemma-25 permutation-lemma-26
              permutation-lemma-27 permutation-lemma-28
              permutation-lemma-29 permutation-lemma-30
              permutation-lemma-31 permutation-lemma-32
              permutation-lemma-33 permutation-lemma-34
              permutation-lemma-35 permutation-lemma-36
              permutation-lemma-37 permutation-lemma-38
              permutation-lemma-39 permutation-lemma-40
              permutation-lemma-41 permutation-lemma-42
              permutation-lemma-43 permutation-lemma-44
              permutation-lemma-45 permutation-lemma-46)
    :use (permutation-lemma-46
          (:instance perm-reflexive (x (sublist k l u))))
)

(defrule permutation-lemma-48
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (= k l)
            (<= l j)
            (< j (len u))
        )
        (permutation
            k
            j
            u
            (update-nth
                k
                (nth l u)
                (update-nth
                    l
                    (nth k u)
                    u
                )
            )
        )
    )
    :do-not-induct t
    :disable (sublist sublist-1 sublist-2 sublist-9 sublist-13
              sublist-14 equal-ranges-sublists equal-ranges
              perm-reflexive permutation permutation-1 permutation-7
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18
              permutation-lemma-19 permutation-lemma-20
              permutation-lemma-21 permutation-lemma-22
              permutation-lemma-23 permutation-lemma-24
              permutation-lemma-25 permutation-lemma-26
              permutation-lemma-27 permutation-lemma-28
              permutation-lemma-29 permutation-lemma-30
              permutation-lemma-31 permutation-lemma-32
              permutation-lemma-33 permutation-lemma-34
              permutation-lemma-35 permutation-lemma-36
              permutation-lemma-37 permutation-lemma-38
              permutation-lemma-39 permutation-lemma-40
              permutation-lemma-41 permutation-lemma-42
              permutation-lemma-43 permutation-lemma-44
              permutation-lemma-45 permutation-lemma-46
              permutation-lemma-47)
    :use (permutation-lemma-18 permutation-lemma-34
          permutation-lemma-47
          (:instance permutation-7
           (i k)
           (k l)
           (v (update-nth
                k
                (nth l u)
                (update-nth
                    l
                    (nth k u)
                    u
                )
            ))))
)

(defrule permutation-lemma-49
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (= k l)
            (<= l j)
            (< j (len u))
            (< (- k 1) i)
        )
        (permutation
            i
            j
            u
            (update-nth
                k
                (nth l u)
                (update-nth
                    l
                    (nth k u)
                    u
                )
            )
        )
    )
    :do-not-induct t
    :disable (sublist sublist-1 sublist-2 sublist-9 sublist-13
              sublist-14 equal-ranges-sublists equal-ranges
              perm-reflexive permutation permutation-1 permutation-7
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18
              permutation-lemma-19 permutation-lemma-20
              permutation-lemma-21 permutation-lemma-22
              permutation-lemma-23 permutation-lemma-24
              permutation-lemma-25 permutation-lemma-26
              permutation-lemma-27 permutation-lemma-28
              permutation-lemma-29 permutation-lemma-30
              permutation-lemma-31 permutation-lemma-32
              permutation-lemma-33 permutation-lemma-34
              permutation-lemma-35 permutation-lemma-36
              permutation-lemma-37 permutation-lemma-38
              permutation-lemma-39 permutation-lemma-40
              permutation-lemma-41 permutation-lemma-42
              permutation-lemma-43 permutation-lemma-44
              permutation-lemma-45 permutation-lemma-46
              permutation-lemma-47 permutation-lemma-48)
    :use (permutation-lemma-48)
    :cases ((= i k))
)

(defrule permutation-lemma-50
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (= k l)
            (<= l j)
            (< j (len u))
            (<= i (- k 1))
        )
        (permutation
            i
            j
            u
            (update-nth
                k
                (nth l u)
                (update-nth
                    l
                    (nth k u)
                    u
                )
            )
        )
    )
    :do-not-induct t
    :disable (sublist sublist-1 sublist-2 sublist-9 sublist-13
              sublist-14 equal-ranges-sublists equal-ranges
              perm-reflexive permutation permutation-1 permutation-7
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18
              permutation-lemma-19 permutation-lemma-20
              permutation-lemma-21 permutation-lemma-22
              permutation-lemma-23 permutation-lemma-24
              permutation-lemma-25 permutation-lemma-26
              permutation-lemma-27 permutation-lemma-28
              permutation-lemma-29 permutation-lemma-30
              permutation-lemma-31 permutation-lemma-32
              permutation-lemma-33 permutation-lemma-34
              permutation-lemma-35 permutation-lemma-36
              permutation-lemma-37 permutation-lemma-38
              permutation-lemma-39 permutation-lemma-40
              permutation-lemma-41 permutation-lemma-42
              permutation-lemma-43 permutation-lemma-44
              permutation-lemma-45 permutation-lemma-46
              permutation-lemma-47 permutation-lemma-48
              permutation-lemma-49)
    :use (permutation-lemma-16 permutation-lemma-34
          permutation-lemma-48
          (:instance permutation-7
           (k (- k 1))
           (v (update-nth
                k
                (nth l u)
                (update-nth
                    l
                    (nth k u)
                    u
                )
            ))))
)

(defrule permutation-lemma-51
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (= k l)
            (<= l j)
            (< j (len u))
        )
        (permutation
            i
            j
            u
            (update-nth
                k
                (nth l u)
                (update-nth
                    l
                    (nth k u)
                    u
                )
            )
        )
    )
    :do-not-induct t
    :disable (sublist sublist-1 sublist-2 sublist-9 sublist-13
              sublist-14 equal-ranges-sublists equal-ranges
              perm-reflexive permutation permutation-1 permutation-7
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18
              permutation-lemma-19 permutation-lemma-20
              permutation-lemma-21 permutation-lemma-22
              permutation-lemma-23 permutation-lemma-24
              permutation-lemma-25 permutation-lemma-26
              permutation-lemma-27 permutation-lemma-28
              permutation-lemma-29 permutation-lemma-30
              permutation-lemma-31 permutation-lemma-32
              permutation-lemma-33 permutation-lemma-34
              permutation-lemma-35 permutation-lemma-36
              permutation-lemma-37 permutation-lemma-38
              permutation-lemma-39 permutation-lemma-40
              permutation-lemma-41 permutation-lemma-42
              permutation-lemma-43 permutation-lemma-44
              permutation-lemma-45 permutation-lemma-46
              permutation-lemma-47 permutation-lemma-48
              permutation-lemma-49 permutation-lemma-50)
    :hints (("Goal" :cases ((<= i (- k 1))))
            ("Subgoal 2" :use (permutation-lemma-49))
            ("Subgoal 1" :use (permutation-lemma-50)))
)

(local
(defrule permutation-lemma-52
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (<= k l)
            (<= l j)
            (< j (len u))
            (< (- l 1) k)
        )
        (equal
            (sublist k l u)
            (append
                (sublist k (- l 1) u)
                (sublist l l u)
            )
        )
    )
    :do-not-induct t
    :enable (sublist)
    :disable (sublist-1 sublist-2 sublist-9 sublist-13
              sublist-14 equal-ranges-sublists equal-ranges
              perm-reflexive permutation permutation-1 permutation-7
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18
              permutation-lemma-19 permutation-lemma-20
              permutation-lemma-21 permutation-lemma-22
              permutation-lemma-23 permutation-lemma-24
              permutation-lemma-25 permutation-lemma-26
              permutation-lemma-27 permutation-lemma-28
              permutation-lemma-29 permutation-lemma-30
              permutation-lemma-31 permutation-lemma-32
              permutation-lemma-33 permutation-lemma-34
              permutation-lemma-35 permutation-lemma-36
              permutation-lemma-37 permutation-lemma-38
              permutation-lemma-39 permutation-lemma-40
              permutation-lemma-41 permutation-lemma-42
              permutation-lemma-43 permutation-lemma-44
              permutation-lemma-45 permutation-lemma-46
              permutation-lemma-47 permutation-lemma-48
              permutation-lemma-49 permutation-lemma-50
              permutation-lemma-51)
    :hints (("Goal" :expand
                    ((sublist k (- l 1) u))))
)
)

(local
(defrule permutation-lemma-53
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (<= k l)
            (<= l j)
            (< j (len u))
        )
        (equal
            (sublist k l u)
            (append
                (sublist k (- l 1) u)
                (sublist l l u)
            )
        )
    )
    :do-not-induct t
    :disable (sublist sublist-1 sublist-2 sublist-9 sublist-13
              sublist-14 equal-ranges-sublists equal-ranges
              perm-reflexive permutation permutation-1 permutation-7
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18
              permutation-lemma-19 permutation-lemma-20
              permutation-lemma-21 permutation-lemma-22
              permutation-lemma-23 permutation-lemma-24
              permutation-lemma-25 permutation-lemma-26
              permutation-lemma-27 permutation-lemma-28
              permutation-lemma-29 permutation-lemma-30
              permutation-lemma-31 permutation-lemma-32
              permutation-lemma-33 permutation-lemma-34
              permutation-lemma-35 permutation-lemma-36
              permutation-lemma-37 permutation-lemma-38
              permutation-lemma-39 permutation-lemma-40
              permutation-lemma-41 permutation-lemma-42
              permutation-lemma-43 permutation-lemma-44
              permutation-lemma-45 permutation-lemma-46
              permutation-lemma-47 permutation-lemma-48
              permutation-lemma-49 permutation-lemma-50
              permutation-lemma-51 permutation-lemma-52)
    :hints (("Goal" :cases ((<= k (- l 1))))
            ("Subgoal 2" :use permutation-lemma-52)
            ("Subgoal 1" :use
                         (:instance sublist-14
                          (i k)
                          (k (- l 1))
                          (j l)))))
)
)

(local
(defrule permutation-lemma-54
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (< k l)
            (<= l j)
            (< j (len u))
            (<= l (+ k 1))
        )
        (equal
            (sublist k (- l 1) u)
            (append
                (sublist k k u)
                (sublist (+ k 1) (- l 1) u)
            )
        )
    )
    :do-not-induct t
    :enable (sublist)
    :disable (sublist-1 sublist-2 sublist-9 sublist-13
              sublist-14 equal-ranges-sublists equal-ranges
              perm-reflexive permutation permutation-1 permutation-7
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18
              permutation-lemma-19 permutation-lemma-20
              permutation-lemma-21 permutation-lemma-22
              permutation-lemma-23 permutation-lemma-24
              permutation-lemma-25 permutation-lemma-26
              permutation-lemma-27 permutation-lemma-28
              permutation-lemma-29 permutation-lemma-30
              permutation-lemma-31 permutation-lemma-32
              permutation-lemma-33 permutation-lemma-34
              permutation-lemma-35 permutation-lemma-36
              permutation-lemma-37 permutation-lemma-38
              permutation-lemma-39 permutation-lemma-40
              permutation-lemma-41 permutation-lemma-42
              permutation-lemma-43 permutation-lemma-44
              permutation-lemma-45 permutation-lemma-46
              permutation-lemma-47 permutation-lemma-48
              permutation-lemma-49 permutation-lemma-50
              permutation-lemma-51 permutation-lemma-52
              permutation-lemma-53)
    :hints (("Goal" :expand
                    ((sublist (+ k 1) (- l 1) u))))
)
)

(local
(defrule permutation-lemma-55
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (< k l)
            (<= l j)
            (< j (len u))
        )
        (equal
            (sublist k (- l 1) u)
            (append
                (sublist k k u)
                (sublist (+ k 1) (- l 1) u)
            )
        )
    )
    :do-not-induct t
    :disable (sublist sublist-1 sublist-2 sublist-9 sublist-13
              sublist-14 equal-ranges-sublists equal-ranges
              perm-reflexive permutation permutation-1 permutation-7
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18
              permutation-lemma-19 permutation-lemma-20
              permutation-lemma-21 permutation-lemma-22
              permutation-lemma-23 permutation-lemma-24
              permutation-lemma-25 permutation-lemma-26
              permutation-lemma-27 permutation-lemma-28
              permutation-lemma-29 permutation-lemma-30
              permutation-lemma-31 permutation-lemma-32
              permutation-lemma-33 permutation-lemma-34
              permutation-lemma-35 permutation-lemma-36
              permutation-lemma-37 permutation-lemma-38
              permutation-lemma-39 permutation-lemma-40
              permutation-lemma-41 permutation-lemma-42
              permutation-lemma-43 permutation-lemma-44
              permutation-lemma-45 permutation-lemma-46
              permutation-lemma-47 permutation-lemma-48
              permutation-lemma-49 permutation-lemma-50
              permutation-lemma-51 permutation-lemma-52
              permutation-lemma-53 permutation-lemma-54)
    :hints (("Goal" :cases ((< (+ k 1) l)))
            ("Subgoal 2" :use permutation-lemma-54)
            ("Subgoal 1" :use
                         (:instance sublist-14
                          (i k)
                          (k k)
                          (j (- l 1))))))
)
)

(defrule permutation-lemma-56
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (<= k l)
            (<= l j)
            (< j (len u))
            (< (- l 1) k)
        )
        (equal
            (sublist
                k
                l
                (update-nth
                    k
                    (nth l u)
                    (update-nth
                        l
                        (nth k u)
                        u
                    )
                )
            )
            (append
                (sublist
                    k
                    (- l 1)
                    (update-nth
                        k
                        (nth l u)
                        (update-nth
                            l
                            (nth k u)
                            u
                        )
                    )
                )
                (sublist
                    l
                    l
                    (update-nth
                        k
                        (nth l u)
                        (update-nth
                            l
                            (nth k u)
                            u
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :enable (sublist)
    :disable (sublist-1 sublist-2 sublist-9 sublist-13
              sublist-14 equal-ranges-sublists equal-ranges
              perm-reflexive permutation permutation-1 permutation-7
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18
              permutation-lemma-19 permutation-lemma-20
              permutation-lemma-21 permutation-lemma-22
              permutation-lemma-23 permutation-lemma-24
              permutation-lemma-25 permutation-lemma-26
              permutation-lemma-27 permutation-lemma-28
              permutation-lemma-29 permutation-lemma-30
              permutation-lemma-31 permutation-lemma-32
              permutation-lemma-33 permutation-lemma-34
              permutation-lemma-35 permutation-lemma-36
              permutation-lemma-37 permutation-lemma-38
              permutation-lemma-39 permutation-lemma-40
              permutation-lemma-41 permutation-lemma-42
              permutation-lemma-43 permutation-lemma-44
              permutation-lemma-45 permutation-lemma-46
              permutation-lemma-47 permutation-lemma-48
              permutation-lemma-49 permutation-lemma-50
              permutation-lemma-51 permutation-lemma-52
              permutation-lemma-53 permutation-lemma-54
              permutation-lemma-55)
    :use (permutation-lemma-34)
    :hints (("Goal"
             :expand
              (
                (sublist
                    k
                    (- l 1)
                    (update-nth
                        k
                        (nth l u)
                        (update-nth
                            l
                            (nth k u)
                            u
                        )
                    )
                )
              )))
)

(defrule permutation-lemma-57
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (<= k l)
            (<= l j)
            (< j (len u))
        )
        (equal
            (sublist
                k
                l
                (update-nth
                    k
                    (nth l u)
                    (update-nth
                        l
                        (nth k u)
                        u
                    )
                )
            )
            (append
                (sublist
                    k
                    (- l 1)
                    (update-nth
                        k
                        (nth l u)
                        (update-nth
                            l
                            (nth k u)
                            u
                        )
                    )
                )
                (sublist
                    l
                    l
                    (update-nth
                        k
                        (nth l u)
                        (update-nth
                            l
                            (nth k u)
                            u
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (sublist sublist-1 sublist-2 sublist-9 sublist-13
              sublist-14 equal-ranges-sublists equal-ranges
              perm-reflexive permutation permutation-1 permutation-7
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18
              permutation-lemma-19 permutation-lemma-20
              permutation-lemma-21 permutation-lemma-22
              permutation-lemma-23 permutation-lemma-24
              permutation-lemma-25 permutation-lemma-26
              permutation-lemma-27 permutation-lemma-28
              permutation-lemma-29 permutation-lemma-30
              permutation-lemma-31 permutation-lemma-32
              permutation-lemma-33 permutation-lemma-34
              permutation-lemma-35 permutation-lemma-36
              permutation-lemma-37 permutation-lemma-38
              permutation-lemma-39 permutation-lemma-40
              permutation-lemma-41 permutation-lemma-42
              permutation-lemma-43 permutation-lemma-44
              permutation-lemma-45 permutation-lemma-46
              permutation-lemma-47 permutation-lemma-48
              permutation-lemma-49 permutation-lemma-50
              permutation-lemma-51 permutation-lemma-52
              permutation-lemma-53 permutation-lemma-54
              permutation-lemma-55 permutation-lemma-56)
    :use (permutation-lemma-34)
    :hints (("Goal" :cases ((< k l)))
            ("Subgoal 2" :use (permutation-lemma-34
                               permutation-lemma-56))
            ("Subgoal 1" :use (permutation-lemma-34
                         (:instance sublist-14
                         (i k)
                         (k (- l 1))
                         (j l)
                         (u
                            (update-nth
                                k
                                (nth l u)
                                (update-nth
                                    l
                                    (nth k u)
                                    u
                                )
                            )
                         )))))
)

(defrule permutation-lemma-58
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (< k l)
            (<= l j)
            (< j (len u))
            (<= l (+ k 1))
        )
        (equal
            (sublist
                k
                (- l 1)
                (update-nth
                    k
                    (nth l u)
                    (update-nth
                        l
                        (nth k u)
                        u
                    )
                )
            )
            (append
                (sublist
                    k
                    k
                    (update-nth
                        k
                        (nth l u)
                        (update-nth
                            l
                            (nth k u)
                            u
                        )
                    )
                )
                (sublist
                    (+ k 1)
                    (- l 1)
                    (update-nth
                        k
                        (nth l u)
                        (update-nth
                            l
                            (nth k u)
                            u
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :enable (sublist)
    :disable (sublist-1 sublist-2 sublist-9 sublist-13
              sublist-14 equal-ranges-sublists equal-ranges
              perm-reflexive permutation permutation-1 permutation-7
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18
              permutation-lemma-19 permutation-lemma-20
              permutation-lemma-21 permutation-lemma-22
              permutation-lemma-23 permutation-lemma-24
              permutation-lemma-25 permutation-lemma-26
              permutation-lemma-27 permutation-lemma-28
              permutation-lemma-29 permutation-lemma-30
              permutation-lemma-31 permutation-lemma-32
              permutation-lemma-33 permutation-lemma-34
              permutation-lemma-35 permutation-lemma-36
              permutation-lemma-37 permutation-lemma-38
              permutation-lemma-39 permutation-lemma-40
              permutation-lemma-41 permutation-lemma-42
              permutation-lemma-43 permutation-lemma-44
              permutation-lemma-45 permutation-lemma-46
              permutation-lemma-47 permutation-lemma-48
              permutation-lemma-49 permutation-lemma-50
              permutation-lemma-51 permutation-lemma-52
              permutation-lemma-53 permutation-lemma-54
              permutation-lemma-55 permutation-lemma-56
              permutation-lemma-57)
    :use (permutation-lemma-34)
    :hints (("Goal"
             :expand
              (
                (sublist
                    (+ k 1)
                    (- l 1)
                    (update-nth
                        k
                        (nth l u)
                        (update-nth
                            l
                            (nth k u)
                            u
                        )
                    )
                )
              )))
)

(defrule permutation-lemma-59
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (< k l)
            (<= l j)
            (< j (len u))
        )
        (equal
            (sublist
                k
                (- l 1)
                (update-nth
                    k
                    (nth l u)
                    (update-nth
                        l
                        (nth k u)
                        u
                    )
                )
            )
            (append
                (sublist
                    k
                    k
                    (update-nth
                        k
                        (nth l u)
                        (update-nth
                            l
                            (nth k u)
                            u
                        )
                    )
                )
                (sublist
                    (+ k 1)
                    (- l 1)
                    (update-nth
                        k
                        (nth l u)
                        (update-nth
                            l
                            (nth k u)
                            u
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (sublist sublist-1 sublist-2 sublist-9 sublist-13
              sublist-14 equal-ranges-sublists equal-ranges
              perm-reflexive permutation permutation-1 permutation-7
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18
              permutation-lemma-19 permutation-lemma-20
              permutation-lemma-21 permutation-lemma-22
              permutation-lemma-23 permutation-lemma-24
              permutation-lemma-25 permutation-lemma-26
              permutation-lemma-27 permutation-lemma-28
              permutation-lemma-29 permutation-lemma-30
              permutation-lemma-31 permutation-lemma-32
              permutation-lemma-33 permutation-lemma-34
              permutation-lemma-35 permutation-lemma-36
              permutation-lemma-37 permutation-lemma-38
              permutation-lemma-39 permutation-lemma-40
              permutation-lemma-41 permutation-lemma-42
              permutation-lemma-43 permutation-lemma-44
              permutation-lemma-45 permutation-lemma-46
              permutation-lemma-47 permutation-lemma-48
              permutation-lemma-49 permutation-lemma-50
              permutation-lemma-51 permutation-lemma-52
              permutation-lemma-53 permutation-lemma-54
              permutation-lemma-55 permutation-lemma-56
              permutation-lemma-57 permutation-lemma-58)
    :hints (("Goal" :cases ((< (+ k 1) l)))
            ("Subgoal 2" :use (permutation-lemma-34
                               permutation-lemma-58))
            ("Subgoal 1" :use (permutation-lemma-34
                         (:instance sublist-14
                         (i k)
                         (k k)
                         (j (- l 1))
                         (u
                            (update-nth
                                k
                                (nth l u)
                                (update-nth
                                    l
                                    (nth k u)
                                    u
                                )
                            )
                         )))))
)

(defrule permutation-lemma-60
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (< k l)
            (<= l j)
            (< j (len u))
        )
        (perm
            (append
                (sublist k k u)
                (sublist (+ k 1) (- l 1) u)
            )
            (append
                (sublist
                    (+ k 1)
                    (- l 1)
                    (update-nth
                        k
                        (nth l u)
                        (update-nth
                            l
                            (nth k u)
                            u
                        )
                    )
                )
                (sublist
                    l
                    l
                    (update-nth
                        k
                        (nth l u)
                        (update-nth
                            l
                            (nth k u)
                            u
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (sublist sublist-1 sublist-2 sublist-9 sublist-13
              sublist-14 equal-ranges-sublists equal-ranges
              perm-reflexive permutation permutation-1 permutation-7
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18
              permutation-lemma-19 permutation-lemma-20
              permutation-lemma-21 permutation-lemma-22
              permutation-lemma-23 permutation-lemma-24
              permutation-lemma-25 permutation-lemma-26
              permutation-lemma-27 permutation-lemma-28
              permutation-lemma-29 permutation-lemma-30
              permutation-lemma-31 permutation-lemma-32
              permutation-lemma-33 permutation-lemma-34
              permutation-lemma-35 permutation-lemma-36
              permutation-lemma-37 permutation-lemma-38
              permutation-lemma-39 permutation-lemma-40
              permutation-lemma-41 permutation-lemma-42
              permutation-lemma-43 permutation-lemma-44
              permutation-lemma-45 permutation-lemma-46
              permutation-lemma-47 permutation-lemma-48
              permutation-lemma-49 permutation-lemma-50
              permutation-lemma-51 permutation-lemma-52
              permutation-lemma-53 permutation-lemma-54
              permutation-lemma-55 permutation-lemma-56
              permutation-lemma-57 permutation-lemma-58
              permutation-lemma-59)
    :use (permutation-lemma-39 permutation-lemma-41
          (:instance perm-concat-2
           (x (sublist k k u))
           (u (sublist (+ k 1) (- l 1) u))
           (v
                (sublist
                    (+ k 1)
                    (- l 1)
                    (update-nth
                        k
                        (nth l u)
                        (update-nth
                            l
                            (nth k u)
                            u
                        )
                    )
                ))
           (y
                (sublist
                    l
                    l
                    (update-nth
                        k
                        (nth l u)
                        (update-nth
                            l
                            (nth k u)
                            u
                        )
                    )
                ))))
)

(defrule permutation-lemma-61
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (< k l)
            (<= l j)
            (< j (len u))
        )
        (perm
            (sublist k (- l 1) u)
            (sublist
                (+ k 1)
                l
                (update-nth
                    k
                    (nth l u)
                    (update-nth
                        l
                        (nth k u)
                        u
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (sublist sublist-1 sublist-2 sublist-9 sublist-13
              sublist-14 equal-ranges-sublists equal-ranges
              perm-reflexive permutation permutation-1 permutation-7
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18
              permutation-lemma-19 permutation-lemma-20
              permutation-lemma-21 permutation-lemma-22
              permutation-lemma-23 permutation-lemma-24
              permutation-lemma-25 permutation-lemma-26
              permutation-lemma-27 permutation-lemma-28
              permutation-lemma-29 permutation-lemma-30
              permutation-lemma-31 permutation-lemma-32
              permutation-lemma-33 permutation-lemma-34
              permutation-lemma-35 permutation-lemma-36
              permutation-lemma-37 permutation-lemma-38
              permutation-lemma-39 permutation-lemma-40
              permutation-lemma-41 permutation-lemma-42
              permutation-lemma-43 permutation-lemma-44
              permutation-lemma-45 permutation-lemma-46
              permutation-lemma-47 permutation-lemma-48
              permutation-lemma-49 permutation-lemma-50
              permutation-lemma-51 permutation-lemma-52
              permutation-lemma-53 permutation-lemma-54
              permutation-lemma-55 permutation-lemma-56
              permutation-lemma-57 permutation-lemma-58
              permutation-lemma-59 permutation-lemma-60)
    :use (permutation-lemma-45
          permutation-lemma-55
          permutation-lemma-60)
)

(defrule permutation-lemma-62
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (< k l)
            (<= l j)
            (< j (len u))
        )
        (perm
            (append
                (sublist k (- l 1) u)
                (sublist l l u)
            )
            (append
                (sublist
                    k
                    k
                    (update-nth
                        k
                        (nth l u)
                        (update-nth
                            l
                            (nth k u)
                            u
                        )
                    )
                )
                (sublist
                    (+ k 1)
                    l
                    (update-nth
                        k
                        (nth l u)
                        (update-nth
                            l
                            (nth k u)
                            u
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (sublist sublist-1 sublist-2 sublist-9 sublist-13
              sublist-14 equal-ranges-sublists equal-ranges
              perm-reflexive permutation permutation-1 permutation-7
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18
              permutation-lemma-19 permutation-lemma-20
              permutation-lemma-21 permutation-lemma-22
              permutation-lemma-23 permutation-lemma-24
              permutation-lemma-25 permutation-lemma-26
              permutation-lemma-27 permutation-lemma-28
              permutation-lemma-29 permutation-lemma-30
              permutation-lemma-31 permutation-lemma-32
              permutation-lemma-33 permutation-lemma-34
              permutation-lemma-35 permutation-lemma-36
              permutation-lemma-37 permutation-lemma-38
              permutation-lemma-39 permutation-lemma-40
              permutation-lemma-41 permutation-lemma-42
              permutation-lemma-43 permutation-lemma-44
              permutation-lemma-45 permutation-lemma-46
              permutation-lemma-47 permutation-lemma-48
              permutation-lemma-49 permutation-lemma-50
              permutation-lemma-51 permutation-lemma-52
              permutation-lemma-53 permutation-lemma-54
              permutation-lemma-55 permutation-lemma-56
              permutation-lemma-57 permutation-lemma-58
              permutation-lemma-59 permutation-lemma-60
              permutation-lemma-61)
    :use (permutation-lemma-40 permutation-lemma-61
          (:instance perm-concat-2
           (x (sublist k (- l 1) u))
           (u (sublist l l u))
           (v
                (sublist
                    k
                    k
                    (update-nth
                        k
                        (nth l u)
                        (update-nth
                            l
                            (nth k u)
                            u
                        )
                    )
                ))
           (y
                (sublist
                    (+ k 1)
                    l
                    (update-nth
                        k
                        (nth l u)
                        (update-nth
                            l
                            (nth k u)
                            u
                        )
                    )
                ))))
)

(defrule permutation-lemma-63
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (< k l)
            (<= l j)
            (< j (len u))
        )
        (perm
            (sublist k l u)
            (sublist
                k
                l
                (update-nth
                    k
                    (nth l u)
                    (update-nth
                        l
                        (nth k u)
                        u
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (sublist sublist-1 sublist-2 sublist-7 sublist-9
              sublist-13 sublist-14 equal-ranges-sublists equal-ranges
              perm perm-reflexive permutation permutation-1 permutation-7
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18
              permutation-lemma-19 permutation-lemma-20
              permutation-lemma-21 permutation-lemma-22
              permutation-lemma-23 permutation-lemma-24
              permutation-lemma-25 permutation-lemma-26
              permutation-lemma-27 permutation-lemma-28
              permutation-lemma-29 permutation-lemma-30
              permutation-lemma-31 permutation-lemma-32
              permutation-lemma-33 permutation-lemma-34
              permutation-lemma-35 permutation-lemma-36
              permutation-lemma-37 permutation-lemma-38
              permutation-lemma-39 permutation-lemma-40
              permutation-lemma-41 permutation-lemma-42
              permutation-lemma-43 permutation-lemma-44
              permutation-lemma-45 permutation-lemma-46
              permutation-lemma-47 permutation-lemma-48
              permutation-lemma-49 permutation-lemma-50
              permutation-lemma-51 permutation-lemma-52
              permutation-lemma-53 permutation-lemma-54
              permutation-lemma-55 permutation-lemma-56
              permutation-lemma-57 permutation-lemma-58
              permutation-lemma-59 permutation-lemma-60
              permutation-lemma-61 permutation-lemma-62)
    :use (permutation-lemma-43
          permutation-lemma-53
          permutation-lemma-62)
)

(defrule permutation-lemma-64
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (< k l)
            (<= l j)
            (< j (len u))
        )
        (permutation
            k
            l
            u
            (update-nth
                k
                (nth l u)
                (update-nth
                    l
                    (nth k u)
                    u
                )
            )
        )
    )
    :do-not-induct t
    :enable (permutation)
    :disable (sublist sublist-1 sublist-2 sublist-7 sublist-9
              sublist-13 sublist-14 equal-ranges-sublists equal-ranges
              perm perm-reflexive permutation-1 permutation-7
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18
              permutation-lemma-19 permutation-lemma-20
              permutation-lemma-21 permutation-lemma-22
              permutation-lemma-23 permutation-lemma-24
              permutation-lemma-25 permutation-lemma-26
              permutation-lemma-27 permutation-lemma-28
              permutation-lemma-29 permutation-lemma-30
              permutation-lemma-31 permutation-lemma-32
              permutation-lemma-33 permutation-lemma-34
              permutation-lemma-35 permutation-lemma-36
              permutation-lemma-37 permutation-lemma-38
              permutation-lemma-39 permutation-lemma-40
              permutation-lemma-41 permutation-lemma-42
              permutation-lemma-43 permutation-lemma-44
              permutation-lemma-45 permutation-lemma-46
              permutation-lemma-47 permutation-lemma-48
              permutation-lemma-49 permutation-lemma-50
              permutation-lemma-51 permutation-lemma-52
              permutation-lemma-53 permutation-lemma-54
              permutation-lemma-55 permutation-lemma-56
              permutation-lemma-57 permutation-lemma-58
              permutation-lemma-59 permutation-lemma-60
              permutation-lemma-61 permutation-lemma-62
              permutation-lemma-63)
    :hints (("Goal"
             :use (permutation-lemma-63)
             :expand
             (
                (permutation
                    k
                    l
                    u
                    (update-nth
                        k
                        (nth l u)
                        (update-nth
                            l
                            (nth k u)
                            u
                        )
                    )
                )
             )))
)

(defrule permutation-lemma-65
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (< k l)
            (<= l j)
            (< j (len u))
        )
        (permutation
            k
            j
            u
            (update-nth
                k
                (nth l u)
                (update-nth
                    l
                    (nth k u)
                    u
                )
            )
        )
    )
    :do-not-induct t
    :disable (sublist sublist-1 sublist-2 sublist-7 sublist-9
              sublist-13 sublist-14 equal-ranges-sublists equal-ranges
              perm perm-reflexive permutation permutation-1 permutation-7
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18
              permutation-lemma-19 permutation-lemma-20
              permutation-lemma-21 permutation-lemma-22
              permutation-lemma-23 permutation-lemma-24
              permutation-lemma-25 permutation-lemma-26
              permutation-lemma-27 permutation-lemma-28
              permutation-lemma-29 permutation-lemma-30
              permutation-lemma-31 permutation-lemma-32
              permutation-lemma-33 permutation-lemma-34
              permutation-lemma-35 permutation-lemma-36
              permutation-lemma-37 permutation-lemma-38
              permutation-lemma-39 permutation-lemma-40
              permutation-lemma-41 permutation-lemma-42
              permutation-lemma-43 permutation-lemma-44
              permutation-lemma-45 permutation-lemma-46
              permutation-lemma-47 permutation-lemma-48
              permutation-lemma-49 permutation-lemma-50
              permutation-lemma-51 permutation-lemma-52
              permutation-lemma-53 permutation-lemma-54
              permutation-lemma-55 permutation-lemma-56
              permutation-lemma-57 permutation-lemma-58
              permutation-lemma-59 permutation-lemma-60
              permutation-lemma-61 permutation-lemma-62
              permutation-lemma-63 permutation-lemma-64)
    :use (permutation-lemma-18 permutation-lemma-34
          permutation-lemma-64
          (:instance permutation-7
           (i k)
           (k l)
           (v (update-nth
                k
                (nth l u)
                (update-nth
                    l
                    (nth k u)
                    u
                )
            ))))
)

(defrule permutation-lemma-66
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (< k l)
            (<= l j)
            (< j (len u))
            (< (- k 1) i)
        )
        (permutation
            i
            j
            u
            (update-nth
                k
                (nth l u)
                (update-nth
                    l
                    (nth k u)
                    u
                )
            )
        )
    )
    :do-not-induct t
    :disable (sublist sublist-1 sublist-2 sublist-7 sublist-9
              sublist-13 sublist-14 equal-ranges-sublists equal-ranges
              perm perm-reflexive permutation permutation-1 permutation-7
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18
              permutation-lemma-19 permutation-lemma-20
              permutation-lemma-21 permutation-lemma-22
              permutation-lemma-23 permutation-lemma-24
              permutation-lemma-25 permutation-lemma-26
              permutation-lemma-27 permutation-lemma-28
              permutation-lemma-29 permutation-lemma-30
              permutation-lemma-31 permutation-lemma-32
              permutation-lemma-33 permutation-lemma-34
              permutation-lemma-35 permutation-lemma-36
              permutation-lemma-37 permutation-lemma-38
              permutation-lemma-39 permutation-lemma-40
              permutation-lemma-41 permutation-lemma-42
              permutation-lemma-43 permutation-lemma-44
              permutation-lemma-45 permutation-lemma-46
              permutation-lemma-47 permutation-lemma-48
              permutation-lemma-49 permutation-lemma-50
              permutation-lemma-51 permutation-lemma-52
              permutation-lemma-53 permutation-lemma-54
              permutation-lemma-55 permutation-lemma-56
              permutation-lemma-57 permutation-lemma-58
              permutation-lemma-59 permutation-lemma-60
              permutation-lemma-61 permutation-lemma-62
              permutation-lemma-63 permutation-lemma-64
              permutation-lemma-65)
    :use (permutation-lemma-65)
    :cases ((= i k))
)

(defrule permutation-lemma-67
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (< k l)
            (<= l j)
            (< j (len u))
            (<= i (- k 1))
        )
        (permutation
            i
            j
            u
            (update-nth
                k
                (nth l u)
                (update-nth
                    l
                    (nth k u)
                    u
                )
            )
        )
    )
    :do-not-induct t
    :disable (sublist sublist-1 sublist-2 sublist-7 sublist-9
              sublist-13 sublist-14 equal-ranges-sublists equal-ranges
              perm perm-reflexive permutation permutation-1 permutation-7
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18
              permutation-lemma-19 permutation-lemma-20
              permutation-lemma-21 permutation-lemma-22
              permutation-lemma-23 permutation-lemma-24
              permutation-lemma-25 permutation-lemma-26
              permutation-lemma-27 permutation-lemma-28
              permutation-lemma-29 permutation-lemma-30
              permutation-lemma-31 permutation-lemma-32
              permutation-lemma-33 permutation-lemma-34
              permutation-lemma-35 permutation-lemma-36
              permutation-lemma-37 permutation-lemma-38
              permutation-lemma-39 permutation-lemma-40
              permutation-lemma-41 permutation-lemma-42
              permutation-lemma-43 permutation-lemma-44
              permutation-lemma-45 permutation-lemma-46
              permutation-lemma-47 permutation-lemma-48
              permutation-lemma-49 permutation-lemma-50
              permutation-lemma-51 permutation-lemma-52
              permutation-lemma-53 permutation-lemma-54
              permutation-lemma-55 permutation-lemma-56
              permutation-lemma-57 permutation-lemma-58
              permutation-lemma-59 permutation-lemma-60
              permutation-lemma-61 permutation-lemma-62
              permutation-lemma-63 permutation-lemma-64
              permutation-lemma-65 permutation-lemma-66)
    :use (permutation-lemma-16 permutation-lemma-34
          permutation-lemma-65
          (:instance permutation-7
           (k (- k 1))
           (v (update-nth
                k
                (nth l u)
                (update-nth
                    l
                    (nth k u)
                    u
                )
            ))))
)

(defrule permutation-lemma-68
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (< k l)
            (<= l j)
            (< j (len u))
        )
        (permutation
            i
            j
            u
            (update-nth
                k
                (nth l u)
                (update-nth
                    l
                    (nth k u)
                    u
                )
            )
        )
    )
    :do-not-induct t
    :disable (sublist sublist-1 sublist-2 sublist-7 sublist-9
              sublist-13 sublist-14 equal-ranges-sublists equal-ranges
              perm perm-reflexive permutation permutation-1 permutation-7
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18
              permutation-lemma-19 permutation-lemma-20
              permutation-lemma-21 permutation-lemma-22
              permutation-lemma-23 permutation-lemma-24
              permutation-lemma-25 permutation-lemma-26
              permutation-lemma-27 permutation-lemma-28
              permutation-lemma-29 permutation-lemma-30
              permutation-lemma-31 permutation-lemma-32
              permutation-lemma-33 permutation-lemma-34
              permutation-lemma-35 permutation-lemma-36
              permutation-lemma-37 permutation-lemma-38
              permutation-lemma-39 permutation-lemma-40
              permutation-lemma-41 permutation-lemma-42
              permutation-lemma-43 permutation-lemma-44
              permutation-lemma-45 permutation-lemma-46
              permutation-lemma-47 permutation-lemma-48
              permutation-lemma-49 permutation-lemma-50
              permutation-lemma-51 permutation-lemma-52
              permutation-lemma-53 permutation-lemma-54
              permutation-lemma-55 permutation-lemma-56
              permutation-lemma-57 permutation-lemma-58
              permutation-lemma-59 permutation-lemma-60
              permutation-lemma-61 permutation-lemma-62
              permutation-lemma-63 permutation-lemma-64
              permutation-lemma-65 permutation-lemma-66
              permutation-lemma-67)
    :hints (("Goal" :cases ((<= i (- k 1))))
            ("Subgoal 2" :use (permutation-lemma-66))
            ("Subgoal 1" :use (permutation-lemma-67)))
)

(defrule permutation-lemma-69
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (<= k l)
            (<= l j)
            (< j (len u))
        )
        (permutation
            i
            j
            u
            (update-nth
                k
                (nth l u)
                (update-nth
                    l
                    (nth k u)
                    u
                )
            )
        )
    )
    :do-not-induct t
    :disable (sublist sublist-1 sublist-2 sublist-7 sublist-9
              sublist-13 sublist-14 equal-ranges-sublists equal-ranges
              perm perm-reflexive permutation permutation-1 permutation-7
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18
              permutation-lemma-19 permutation-lemma-20
              permutation-lemma-21 permutation-lemma-22
              permutation-lemma-23 permutation-lemma-24
              permutation-lemma-25 permutation-lemma-26
              permutation-lemma-27 permutation-lemma-28
              permutation-lemma-29 permutation-lemma-30
              permutation-lemma-31 permutation-lemma-32
              permutation-lemma-33 permutation-lemma-34
              permutation-lemma-35 permutation-lemma-36
              permutation-lemma-37 permutation-lemma-38
              permutation-lemma-39 permutation-lemma-40
              permutation-lemma-41 permutation-lemma-42
              permutation-lemma-43 permutation-lemma-44
              permutation-lemma-45 permutation-lemma-46
              permutation-lemma-47 permutation-lemma-48
              permutation-lemma-49 permutation-lemma-50
              permutation-lemma-51 permutation-lemma-52
              permutation-lemma-53 permutation-lemma-54
              permutation-lemma-55 permutation-lemma-56
              permutation-lemma-57 permutation-lemma-58
              permutation-lemma-59 permutation-lemma-60
              permutation-lemma-61 permutation-lemma-62
              permutation-lemma-63 permutation-lemma-64
              permutation-lemma-65 permutation-lemma-66
              permutation-lemma-67 permutation-lemma-68)
    :hints (("Goal" :cases ((not (= k l))))
            ("Subgoal 2" :use (permutation-lemma-51))
            ("Subgoal 1" :use (permutation-lemma-68)))
)

(defrule permutation-8
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (natp l)
            (true-listp u)
            (<= i k)
            (<= i l)
            (<= k j)
            (<= l j)
            (< j (len u))
        )
        (permutation
            i
            j
            u
            (update-nth
                k
                (nth l u)
                (update-nth
                    l
                    (nth k u)
                    u
                )
            )
        )
    )
    :do-not-induct t
    :disable (sublist sublist-1 sublist-2 sublist-7 sublist-9
              sublist-13 sublist-14 equal-ranges-sublists equal-ranges
              perm perm-reflexive permutation permutation-1 permutation-7
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18
              permutation-lemma-19 permutation-lemma-20
              permutation-lemma-21 permutation-lemma-22
              permutation-lemma-23 permutation-lemma-24
              permutation-lemma-25 permutation-lemma-26
              permutation-lemma-27 permutation-lemma-28
              permutation-lemma-29 permutation-lemma-30
              permutation-lemma-31 permutation-lemma-32
              permutation-lemma-33 permutation-lemma-34
              permutation-lemma-35 permutation-lemma-36
              permutation-lemma-37 permutation-lemma-38
              permutation-lemma-39 permutation-lemma-40
              permutation-lemma-41 permutation-lemma-42
              permutation-lemma-43 permutation-lemma-44
              permutation-lemma-45 permutation-lemma-46
              permutation-lemma-47 permutation-lemma-48
              permutation-lemma-49 permutation-lemma-50
              permutation-lemma-51 permutation-lemma-52
              permutation-lemma-53 permutation-lemma-54
              permutation-lemma-55 permutation-lemma-56
              permutation-lemma-57 permutation-lemma-58
              permutation-lemma-59 permutation-lemma-60
              permutation-lemma-61 permutation-lemma-62
              permutation-lemma-63 permutation-lemma-64
              permutation-lemma-65 permutation-lemma-66
              permutation-lemma-67 permutation-lemma-68
              permutation-lemma-69)
    :hints (("Goal" :cases ((<= k l)))
            ("Subgoal 2" :use ((:instance
                                 permutation-lemma-69
                                 (k l)
                                 (l k))))
            ("Subgoal 1" :use (permutation-lemma-69)))
)

(defrule permutation-lemma-70
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (true-listp u)
            (true-listp v)
            (<= i k)
            (= k j)
            (< j (len u))
            (< j (len v))
            (equal
                (sublist i k u)
                (sublist (+ i (- j k)) j v)
            )
            (equal
                (sublist (+ k 1) j u)
                (sublist i (- (+ i (- j k)) 1) v)
            )
        )
        (permutation i j u v)
    )
    :do-not-induct t
    :enable (permutation)
    :disable (sublist sublist-1 sublist-2 sublist-7 sublist-9
              sublist-13 sublist-14 equal-ranges-sublists equal-ranges
              perm perm-reflexive permutation-1
              permutation-7 permutation-8
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18
              permutation-lemma-19 permutation-lemma-20
              permutation-lemma-21 permutation-lemma-22
              permutation-lemma-23 permutation-lemma-24
              permutation-lemma-25 permutation-lemma-26
              permutation-lemma-27 permutation-lemma-28
              permutation-lemma-29 permutation-lemma-30
              permutation-lemma-31 permutation-lemma-32
              permutation-lemma-33 permutation-lemma-34
              permutation-lemma-35 permutation-lemma-36
              permutation-lemma-37 permutation-lemma-38
              permutation-lemma-39 permutation-lemma-40
              permutation-lemma-41 permutation-lemma-42
              permutation-lemma-43 permutation-lemma-44
              permutation-lemma-45 permutation-lemma-46
              permutation-lemma-47 permutation-lemma-48
              permutation-lemma-49 permutation-lemma-50
              permutation-lemma-51 permutation-lemma-52
              permutation-lemma-53 permutation-lemma-54
              permutation-lemma-55 permutation-lemma-56
              permutation-lemma-57 permutation-lemma-58
              permutation-lemma-59 permutation-lemma-60
              permutation-lemma-61 permutation-lemma-62
              permutation-lemma-63 permutation-lemma-64
              permutation-lemma-65 permutation-lemma-66
              permutation-lemma-67 permutation-lemma-68
              permutation-lemma-69)
    :hints (("Goal"
             :use (
               (:instance perm-reflexive
                (x (sublist i j u))))
             :expand
             (
                 (permutation i j u v)
             )))
)

(defrule permutation-lemma-71
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (true-listp u)
            (true-listp v)
            (<= i k)
            (< k j)
            (< j (len u))
            (< j (len v))
            (equal
                (sublist i k u)
                (sublist (+ i (- j k)) j v)
            )
            (equal
                (sublist (+ k 1) j u)
                (sublist i (- (+ i (- j k)) 1) v)
            )
        )
        (permutation i j u v)
    )
    :do-not-induct t
    :enable (permutation)
    :disable (sublist sublist-1 sublist-2 sublist-7 sublist-9
              sublist-13 sublist-14 equal-ranges-sublists equal-ranges
              perm perm-reflexive permutation-1
              permutation-7 permutation-8
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18
              permutation-lemma-19 permutation-lemma-20
              permutation-lemma-21 permutation-lemma-22
              permutation-lemma-23 permutation-lemma-24
              permutation-lemma-25 permutation-lemma-26
              permutation-lemma-27 permutation-lemma-28
              permutation-lemma-29 permutation-lemma-30
              permutation-lemma-31 permutation-lemma-32
              permutation-lemma-33 permutation-lemma-34
              permutation-lemma-35 permutation-lemma-36
              permutation-lemma-37 permutation-lemma-38
              permutation-lemma-39 permutation-lemma-40
              permutation-lemma-41 permutation-lemma-42
              permutation-lemma-43 permutation-lemma-44
              permutation-lemma-45 permutation-lemma-46
              permutation-lemma-47 permutation-lemma-48
              permutation-lemma-49 permutation-lemma-50
              permutation-lemma-51 permutation-lemma-52
              permutation-lemma-53 permutation-lemma-54
              permutation-lemma-55 permutation-lemma-56
              permutation-lemma-57 permutation-lemma-58
              permutation-lemma-59 permutation-lemma-60
              permutation-lemma-61 permutation-lemma-62
              permutation-lemma-63 permutation-lemma-64
              permutation-lemma-65 permutation-lemma-66
              permutation-lemma-67 permutation-lemma-68
              permutation-lemma-69 permutation-lemma-70)
    :hints (("Goal"
             :expand ((permutation i j u v))
             :use (sublist-14
              (:instance perm-concat-2
               (x (sublist i k u))
               (y (sublist (+ i (- j k)) j v))
               (u (sublist (+ k 1) j u))
               (v (sublist i (- (+ i (- j k)) 1) v)))
              (:instance sublist-14
               (k (- (+ i (- j k)) 1))
               (u v))
              (:instance perm-reflexive
               (x (sublist i k u)))
              (:instance perm-reflexive
               (x (sublist (+ i (- j k)) j v)))
              (:instance perm-reflexive
               (x (sublist (+ k 1) j u)))
              (:instance perm-reflexive
               (x (sublist i (- (+ i (- j k)) 1) v))))
             ))
)

(defrule permutation-9
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (true-listp u)
            (true-listp v)
            (<= i k)
            (<= k j)
            (< j (len u))
            (< j (len v))
            (equal
                (sublist i k u)
                (sublist (+ i (- j k)) j v)
            )
            (equal
                (sublist (+ k 1) j u)
                (sublist i (- (+ i (- j k)) 1) v)
            )
        )
        (permutation i j u v)
    )
    :do-not-induct t
    :disable (sublist sublist-1 sublist-2 sublist-7 sublist-9
              sublist-13 sublist-14 equal-ranges-sublists equal-ranges
              perm perm-reflexive permutation permutation-1
              permutation-7 permutation-8
              permutation-lemma-1 permutation-lemma-2
              permutation-lemma-3 permutation-lemma-4
              permutation-lemma-5 permutation-lemma-6
              permutation-lemma-7 permutation-lemma-8
              permutation-lemma-9 permutation-lemma-10
              permutation-lemma-11 permutation-lemma-12
              permutation-lemma-13 permutation-lemma-14
              permutation-lemma-15 permutation-lemma-16
              permutation-lemma-17 permutation-lemma-18
              permutation-lemma-19 permutation-lemma-20
              permutation-lemma-21 permutation-lemma-22
              permutation-lemma-23 permutation-lemma-24
              permutation-lemma-25 permutation-lemma-26
              permutation-lemma-27 permutation-lemma-28
              permutation-lemma-29 permutation-lemma-30
              permutation-lemma-31 permutation-lemma-32
              permutation-lemma-33 permutation-lemma-34
              permutation-lemma-35 permutation-lemma-36
              permutation-lemma-37 permutation-lemma-38
              permutation-lemma-39 permutation-lemma-40
              permutation-lemma-41 permutation-lemma-42
              permutation-lemma-43 permutation-lemma-44
              permutation-lemma-45 permutation-lemma-46
              permutation-lemma-47 permutation-lemma-48
              permutation-lemma-49 permutation-lemma-50
              permutation-lemma-51 permutation-lemma-52
              permutation-lemma-53 permutation-lemma-54
              permutation-lemma-55 permutation-lemma-56
              permutation-lemma-57 permutation-lemma-58
              permutation-lemma-59 permutation-lemma-60
              permutation-lemma-61 permutation-lemma-62
              permutation-lemma-63 permutation-lemma-64
              permutation-lemma-65 permutation-lemma-66
              permutation-lemma-67 permutation-lemma-68
              permutation-lemma-69 permutation-lemma-70
              permutation-lemma-71)
    :hints (("Goal" :cases ((< k j)))
            ("Subgoal 2" :use (permutation-lemma-70))
            ("Subgoal 1" :use (permutation-lemma-71)))
)
