(in-package "ACL2")

(include-book "std/util/defrule" :dir :system)
(include-book "centaur/fty/top" :dir :system)
(include-book "std/util/bstar" :dir :system)
(include-book "std/lists/top" :dir :system)
(include-book "std/basic/inductions" :dir :system)

(include-book "sublist")

(defun inc-inc-induct (n1 n2 max1 max2)
    (declare (xargs :measure (nfix (+ (- max1 n1) (- max2 n2)))))
    (if
        (and
            (natp n1)
            (natp n2)
            (natp max1)
            (natp max2)
            (< n1 max1)
            (< n2 max2)
        )
        (cons n1 (inc-inc-induct (+ 1 n1) (+ 1 n2) max1 max2))
        nil
    )
)

(define equal-ranges
    (
        (i1 natp)
        (j1 natp)
        (i2 natp)
        (j2 natp)
        (l1 true-listp)
        (l2 true-listp)
    )
    :returns (result booleanp)
    :measure (+ 1 (nfix (- (nfix j1) (nfix i1))))
    (b*
        (
            (i1 (nfix i1))
            (j1 (nfix j1))
            (i2 (nfix i2))
            (j2 (nfix j2))
            (l1 (list-fix l1))
            (l2 (list-fix l2))
            ((when (<= (len l1) j1)) nil)
            ((when (<= (len l2) j2)) nil)
            ((when (< j1 i1)) nil)
            ((when (< j2 i2)) nil)
            ((when (not (= (- j1 i1) (- j2 i2)))) nil)
            ((when (= i1 j1)) (equal (nth i1 l1) (nth i2 l2)))
        )
        (and
            (equal (nth i1 l1) (nth i2 l2))
            (equal-ranges (+ i1 1) j1 (+ i2 1) j2 l1 l2)
        )
    )
    ///
    (fty::deffixequiv equal-ranges)
)

(defrule equal-ranges-sublists-1
    (implies
        (and
            (natp i1)
            (natp j1)
            (natp i2)
            (natp j2)
            (true-listp l1)
            (true-listp l2)
            (< j1 (len l1))
            (< j2 (len l2))
            (<= i1 j1)
            (<= i2 j2)
            (= (- j1 i1) (- j2 i2))
            (= i1 j1)
        )
        (equal
            (equal-ranges i1 j1 i2 j2 l1 l2)
            (equal
                (sublist i1 j1 l1)
                (sublist i2 j2 l2)
            )
        )
    )
    :do-not-induct t
    :use ((:instance car-of-nthcdr (i i1) (x l1))
          (:instance car-of-nthcdr (i i2) (x l2))
          (:instance take-of-1 (x (nthcdr i1 l1)))
          (:instance take-of-1 (x (nthcdr i2 l2))))
    :hints (("Goal"
            :expand
            ((equal-ranges i1 j1 i2 j2 l1 l2)
             (sublist i1 j1 l1)
             (sublist i2 j2 l2))))
    :enable (equal-ranges sublist)
    :disable (sublist-2 car-of-nthcdr take-of-1)
)

(defrule equal-ranges-sublists
    (implies
        (and
            (natp i1)
            (natp j1)
            (natp i2)
            (natp j2)
            (true-listp l1)
            (true-listp l2)
            (< j1 (len l1))
            (< j2 (len l2))
            (<= i1 j1)
            (<= i2 j2)
            (= (- j1 i1) (- j2 i2))
        )
        (equal
            (equal-ranges i1 j1 i2 j2 l1 l2)
            (equal
                (sublist i1 j1 l1)
                (sublist i2 j2 l2)
            )
        )
    )
    :induct (inc-inc-induct i1 i2 (+ j1 1) (+ j2 1))
    :disable (sublist-1 sublist-2 sublist-13)
    :enable (equal-ranges)
    :hints (("Subgoal *1/1"
             :cases ((< j1 (+ i1 1))))
             ("Subgoal *1/1.2"
             :do-not-induct t
             :use ((:instance sublist-13 (i i1) (j j1) (u l1))
                   (:instance sublist-13 (i i2) (j j2) (u l2)))
             :expand (equal-ranges i1 j1 i2 j2 l1 l2)))
)

