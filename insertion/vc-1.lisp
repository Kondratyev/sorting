(in-package "ACL2")

(include-book "std/util/defrule" :dir :system)
(include-book "centaur/fty/top" :dir :system)
(include-book "std/util/bstar" :dir :system)
(include-book "std/lists/top" :dir :system)
(include-book "std/basic/inductions" :dir :system)
(include-book "tools/with-arith5-help" :dir :system)
(local (allow-arith5-help))

(include-book "permut")
(include-book "ordered")

(with-arith5-help
    (defrule vc-1
        (implies
            (and
                (natp n)
                (< 0 n)
                (integer-listp a0)
                (integer-listp a)
                (equal a0 a)
                (<= n (len a0))   
            )
            (and
                (natp 1)
                (natp n)
                (<= 1 n)
                (integer-listp a0)
                (integer-listp a)
                (<= n (len a))
                (equal
                    (len a0)
                    (len a)
                )
                (equal
                    (sublist 1 (- n 1) a0)
                    (sublist 1 (- n 1) a)
                )
                (permutation 0 0 a0 a)
                (ordered 0 0 a)
            )
        )
        :rule-classes nil
    )
)
