(in-package "ACL2")

(include-book "std/util/defrule" :dir :system)
(include-book "centaur/fty/top" :dir :system)
(include-book "std/util/bstar" :dir :system)
(include-book "std/lists/top" :dir :system)
(include-book "std/basic/inductions" :dir :system)
(include-book "tools/with-arith5-help" :dir :system)
(local (allow-arith5-help))

(include-book "permut")
(include-book "ordered")
(include-book "range")

(fty::defprod frame
    ((loop-break booleanp)
     (j integerp)
     (a integer-listp)))

(fty::defprod envir
    ((upper-bound integerp)
     (k integerp)))

(define frame-init
    ((j integerp)
     (a integer-listp))
    :returns (fr frame-p)
    (make-frame
        :loop-break nil
        :j j
        :a a
    )
    ///
    (fty::deffixequiv frame-init))

(define envir-init
    ((upper-bound integerp)
     (k integerp))
    :returns (env envir-p)
    (make-envir
        :upper-bound upper-bound
        :k k
    )
    ///
    (fty::deffixequiv envir-init))

(define rep
    ((iteration natp)
     (env envir-p)
     (fr frame-p))
    :measure (nfix iteration)
    :verify-guards nil
    :returns (upd-fr frame-p)
    (b*
        ((iteration (nfix iteration))
         (env (envir-fix env))
         (fr (frame-fix fr))        
         ((when (zp iteration)) fr)
         (fr (rep (- iteration 1) env fr))
         ((when (frame->loop-break fr)) fr)        
         (fr
             (if
                 (<=
                     (nth
                         (- (envir->upper-bound env) iteration)
                         (frame->a fr))
                     (envir->k env))
                 (b*
                     ((fr (change-frame fr :loop-break t))
                      ((when t) fr))
                 fr)
             fr))
         ((when (frame->loop-break fr)) fr)
         (fr (change-frame
                 fr
                 :a
                 (update-nth
                     (+ (- (envir->upper-bound env) iteration) 1)
                     (nth
                         (- (envir->upper-bound env) iteration)
                         (frame->a fr))
                     (frame->a fr))))
         (fr (change-frame fr :j (- (- (envir->upper-bound env) iteration) 1))))
    fr)
    ///
    (defrule rep-lemma-1
        (equal
            (envir-fix (envir-fix env))
            (envir-fix env)
        )
    )
    (defrule rep-lemma-2
        (equal
            (frame-fix (frame-fix fr))
            (frame-fix fr)
        )
    )
    (defrule rep-lemma-3
        (equal
            (rep iteration (envir-fix env) fr)
            (rep iteration env fr)
        )
        :hints (("Goal"
                 :expand
                 ((rep iteration (envir-fix env) fr)
                 (rep iteration env fr))))
        :use rep-lemma-1
        :do-not-induct t
    )
    (defrule rep-lemma-4
        (equal
            (rep iteration env (frame-fix fr))
            (rep iteration env fr)
        )
        :hints (("Goal"
                 :expand
                 ((rep iteration env (frame-fix fr))
                 (rep iteration env fr))))
        :use rep-lemma-2
        :do-not-induct t
    )
    (fty::deffixequiv rep)
    (defrule rep-lemma-5
        (integer-listp (frame->a (rep iteration env fr)))
        :induct (dec-induct iteration)
    )
    (defrule rep-lemma-6
        (implies
            (< (frame->j fr) (envir->upper-bound env))
            (<= (frame->j (rep iteration env fr)) (- (envir->upper-bound env) 1))
        )
        :induct (dec-induct iteration)
        :hints (("Subgoal *1/2"
                 :expand ((rep iteration env fr))
                 :do-not-induct t))
    )
    (defrule rep-lemma-7
        (implies
            (and
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (< (frame->j fr) (envir->upper-bound env))
            )
            (equal
                (len (frame->a (rep iteration env fr)))
                (len (frame->a fr))
            )
        )
        :induct (dec-induct iteration)
        :hints (("Subgoal *1/2"
                 :expand ((rep iteration env fr)
                          (len (frame->a (rep (- iteration 1) env fr))))
                 :do-not-induct t))
    )
    (defrule rep-lemma-8
        (implies
            (and
                (natp iteration)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (frame->loop-break (rep iteration env fr))
            )
            (equal
                (frame->j (rep (- iteration 1) env fr))
                (frame->j (rep iteration env fr))
            )
        )
        :do-not-induct t
        :hints (("Goal"
                 :expand ((rep iteration env fr))))
    )
    (defrule rep-lemma-9
        (implies
            (and
                (natp iteration)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (not (frame->loop-break (rep iteration env fr)))
            )
            (equal
                (frame->j (rep iteration env fr))
                (- (- (envir->upper-bound env) iteration) 1)
            )
        )
        :do-not-induct t
        :hints (("Goal"
                 :expand ((rep iteration env fr))))
    )
    (defrule rep-lemma-10
        (implies
            (and
                (natp iteration)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (not (frame->loop-break (rep iteration env fr)))
                (<= index (frame->j (rep iteration env fr)))
            )
            (<= index (- (- (envir->upper-bound env) iteration) 1))
        )
        :use rep-lemma-9
    )
    (defrule rep-lemma-11
        (implies
            (and
                (natp iteration)
                (natp index)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (<= index (- (- (envir->upper-bound env) iteration) 1))
            )
            (not
                (equal
                    index
                    (+ (- (envir->upper-bound env) iteration) 1)
                )
            )
        )
    )
    (defrule rep-lemma-12
        (implies
            (not
                (equal
                    index
                    (+ (- (envir->upper-bound env) iteration) 1)
                )
            )
            (equal
                (nth
                    index
                    (frame->a (rep (- iteration 1) env fr))
                )
                (nth
                    index
                    (update-nth
                        (+ (- (envir->upper-bound env) iteration) 1)
                        (nth
                            (- (envir->upper-bound env) iteration)
                            (frame->a (rep (- iteration 1) env fr))
                        )
                        (frame->a (rep (- iteration 1) env fr))
                    )
                )
            )
        )
        :use ((:instance nth-of-update-nth-diff
               (n1 index)
               (n2 (+ (- (envir->upper-bound env) iteration) 1))
               (v (nth (- (envir->upper-bound env) iteration)
                       (frame->a (rep (- iteration 1) env fr))))
               (x (frame->a (rep (- iteration 1) env fr)))
              ))
    )
    (defrule rep-lemma-13
        (implies
            (and
                (natp iteration)
                (natp index)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (not
                    (equal
                        index
                        (+ (- (envir->upper-bound env) iteration) 1)
                    )
                )
            )
            (equal
                (nth
                    index
                    (frame->a (rep (- iteration 1) env fr))
                )
                (nth
                    index
                    (update-nth
                        (+ (- (envir->upper-bound env) iteration) 1)
                        (nth
                            (- (envir->upper-bound env) iteration)
                            (frame->a (rep (- iteration 1) env fr))
                        )
                        (frame->a (rep (- iteration 1) env fr))
                    )
                )
            )
        )
        :use (rep-lemma-11
              rep-lemma-12)
    )
    (defrule rep-lemma-14
        (implies
            (and
                (natp iteration)
                (not (zp iteration))
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
            )
            (natp (+ (- (envir->upper-bound env) iteration) 1))
        )
        :disable (rep-lemma-12 rep-lemma-13)
    )
    (defrule rep-lemma-15
        (implies
            (and
                (natp iteration)
                (not (zp iteration))
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
            )
            (integerp
                (nth
                    (- (envir->upper-bound env) iteration)
                    (frame->a (rep (- iteration 1) env fr))
                )
            )
        )
        :disable (rep-lemma-12 rep-lemma-13)
        :use ((:instance 
               sublist-12
               (i (- (envir->upper-bound env) iteration))
               (u (frame->a (rep (- iteration 1) env fr)))))
    )
    (defrule rep-lemma-16
        (implies
            (and
                (natp iteration)
                (not (zp iteration))
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
            )
            (integer-listp
                (frame->a (rep (- iteration 1) env fr))
            )
        )
        :disable (rep-lemma-12 rep-lemma-13)
    )
    (defrule rep-lemma-17
        (implies
            (and
                (natp iteration)
                (not (zp iteration))
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
            )
            (<
                (+ (- (envir->upper-bound env) iteration) 1)
                (len (frame->a (rep (- iteration 1) env fr)))
            )
        )
        :disable (rep-lemma-12 rep-lemma-13)
    )
    (defrule rep-lemma-18
        (implies
            (and
                (natp iteration)
                (not (zp iteration))
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
            )
            (and
                (natp (+ (- (envir->upper-bound env) iteration) 1))
                (integerp
                    (nth
                        (- (envir->upper-bound env) iteration)
                        (frame->a (rep (- iteration 1) env fr))
                    )
                )
                (integer-listp
                    (frame->a (rep (- iteration 1) env fr))
                )
                (<
                    (+ (- (envir->upper-bound env) iteration) 1)
                    (len (frame->a (rep (- iteration 1) env fr)))
                )
            )
        )
        :do-not-induct t
        :disable (rep-lemma-12 rep-lemma-13)
        :use (rep-lemma-14 rep-lemma-15 rep-lemma-16 rep-lemma-17)
    )
    (defrule rep-lemma-19
        (implies
            (and
                (natp iteration)
                (not (zp iteration))
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
            )
            (integer-listp
                (update-nth
                    (+ (- (envir->upper-bound env) iteration) 1)
                    (nth
                        (- (envir->upper-bound env) iteration)
                        (frame->a (rep (- iteration 1) env fr))
                    )
                    (frame->a (rep (- iteration 1) env fr))
                )
            )
        )
        :disable (rep-lemma-12 rep-lemma-13)
        :use ((:instance
               sublist-11
               (i (+ (- (envir->upper-bound env) iteration) 1))
               (v
                    (nth
                        (- (envir->upper-bound env) iteration)
                        (frame->a (rep (- iteration 1) env fr))
                    )
               )
               (u (frame->a (rep (- iteration 1) env fr)))))
    )
    (defrule rep-lemma-20
        (implies
            (and
                (natp iteration)
                (natp index)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (not (frame->loop-break (rep iteration env fr)))
                (not
                    (equal
                        index
                        (+ (- (envir->upper-bound env) iteration) 1)
                    )
                )
            )
            (equal
                (nth
                    index
                    (frame->a (rep (- iteration 1) env fr))
                )
                (nth
                    index
                    (frame->a (rep iteration env fr))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-12
                  rep-lemma-13 rep-lemma-9 rep-lemma-5)
        :use rep-lemma-13
        :hints (("Goal"
                 :expand ((rep iteration env fr))
                 :cases ((not (zp iteration)))
               )
               ("Subgoal 1" :use rep-lemma-19))
    )
    (defrule rep-lemma-21
        (implies
            (and
                (natp iteration)
                (natp index)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (frame->loop-break (rep iteration env fr))
                (not
                    (equal
                        index
                        (+ (- (envir->upper-bound env) iteration) 1)
                    )
                )
            )
            (equal
                (nth
                    index
                    (frame->a (rep (- iteration 1) env fr))
                )
                (nth
                    index
                    (frame->a (rep iteration env fr))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-12
                  rep-lemma-13 rep-lemma-9 rep-lemma-5)
        :use rep-lemma-13
        :hints (("Goal"
                 :expand ((rep iteration env fr))
                 :cases ((not (zp iteration)))
               )
               ("Subgoal 1" :use rep-lemma-19))
    )
    (defrule rep-lemma-22
        (implies
            (and
                (natp iteration)
                (natp index)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (not
                    (equal
                        index
                        (+ (- (envir->upper-bound env) iteration) 1)
                    )
                )
            )
            (equal
                (nth
                    index
                    (frame->a (rep (- iteration 1) env fr))
                )
                (nth
                    index
                    (frame->a (rep iteration env fr))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-12
                  rep-lemma-13 rep-lemma-9 rep-lemma-5)
        :hints (("Goal"
                 :cases ((frame->loop-break (rep iteration env fr))))
                 ("Subgoal 2"
                  :use rep-lemma-20)
                 ("Subgoal 1"
                  :use rep-lemma-21)
               )
    )
    (defrule rep-lemma-23
        (implies
            (and
                (natp iteration)
                (natp index)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
            )
            (<=
                (- (- (envir->upper-bound env) iteration) 1)
                (frame->j (rep iteration env fr))
            )
        )
        :induct (dec-induct iteration)
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-12
                  rep-lemma-13 rep-lemma-6 rep-lemma-9 rep-lemma-5 rep-lemma-10
                  rep-lemma-22)
        :hints (("Subgoal *1/2"
                 :expand ((rep iteration env fr))
                 :do-not-induct t))
    )
    (defrule rep-lemma-24
        (implies
            (and
                (natp iteration)
                (natp index)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (<= index (+ (frame->j (rep iteration env fr)) 1))
            )
            (equal
                (nth
                    index
                    (frame->a (rep (- iteration 1) env fr))
                )
                (nth
                    index
                    (frame->a (rep iteration env fr))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22)
        :hints (("Goal"
                 :cases ((frame->loop-break (rep iteration env fr))))
                 ("Subgoal 2"
                  :use (rep-lemma-9 rep-lemma-22))
                 ("Subgoal 1"
                  :expand ((rep iteration env fr)))
               )
    )
    (defrule rep-lemma-25
        (implies
            (and
                (natp iteration)
                (natp index)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (<= index (+ (frame->j (rep iteration env fr)) 1))
            )
            (<= index (+ (frame->j (rep (- iteration 1) env fr)) 1))
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22)
        :hints (("Goal"
                 :cases ((frame->loop-break (rep (- iteration 1) env fr))))
                 ("Subgoal 2"
                  :expand ((rep iteration env fr))
                  :use ((:instance rep-lemma-9 (iteration (- iteration 1)))))
                 ("Subgoal 1"
                  :expand ((rep iteration env fr)))
               )
    )
    (defrule rep-lemma-26
        (implies
            (and
                (not (zp iteration))
                (natp iteration)
                (natp index)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (<= index (+ (frame->j (rep iteration env fr)) 1))
            )
            (and
                (natp (- iteration 1))
                (natp index)
                (<= (- iteration 1) (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (<= index (+ (frame->j (rep (- iteration 1) env fr)) 1))
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-25)
        :use rep-lemma-25
    )
    (defrule rep-lemma-27
        (implies
            (and
                (natp iteration)
                (natp index)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (<= index (+ (frame->j (rep iteration env fr)) 1))
            )
            (equal
                (nth
                    index
                    (frame->a fr)
                )
                (nth
                    index
                    (frame->a (rep iteration env fr))
                )
            )
        )
        :induct (dec-induct iteration)
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26)
        :hints (("Subgoal *1/2"
                 :use (rep-lemma-24 rep-lemma-26)
                 :do-not-induct t))
    )
    (defrule rep-lemma-28
        (implies
            (and
                (natp iteration)
                (natp index)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (= index (+ (frame->j (rep iteration env fr)) 1))
            )
            (equal
                (nth
                    index
                    (frame->a fr)
                )
                (nth
                    index
                    (frame->a (rep iteration env fr))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27)
        :use rep-lemma-27
    )
    (defrule rep-lemma-29
        (implies
            (and
                (not (zp iteration))
                (not (frame->loop-break (rep iteration env fr)))
                (natp iteration)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
            )
            (not (frame->loop-break (rep (- iteration 1) env fr)))
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25)
        :hints (("Goal" :expand (rep iteration env fr)))
    )
    (defrule rep-lemma-30
        (implies
            (and
                (not (zp iteration))
                (not (frame->loop-break (rep iteration env fr)))
                (natp iteration)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
            )
            (equal
                (frame->j (rep (- iteration 1) env fr))
                (- (envir->upper-bound env) iteration)
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25)
        :use (rep-lemma-29 (:instance rep-lemma-9 (iteration (- iteration 1))))
    )
    (defrule rep-lemma-31
        (iff
            (and
                (not (zp iteration))
                (not (frame->loop-break (rep iteration env fr)))
                (natp iteration)
                (natp index)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (>= index (+ (frame->j (rep iteration env fr)) 2))
            )
            (and
                (not (zp iteration))
                (not (frame->loop-break (rep iteration env fr)))
                (natp iteration)
                (natp index)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (>= index (+ (frame->j (rep (- iteration 1) env fr)) 1))
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30)
        :use (rep-lemma-9 rep-lemma-30)
    )
    (defrule rep-lemma-32
        (implies
            (and
                (not (zp iteration))
                (not (frame->loop-break (rep iteration env fr)))
                (natp iteration)
                (natp index)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (not (frame->loop-break (rep iteration env fr)))
                (> index (+ (frame->j (rep (- iteration 1) env fr)) 1))               
            )
            (> index (+ (- (envir->upper-bound env) iteration) 1))
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30)
        :use rep-lemma-30
    )
    (defrule rep-lemma-33
        (implies
            (and
                (not (zp iteration))
                (not (frame->loop-break (rep iteration env fr)))
                (natp iteration)
                (natp index)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (> index (+ (- (envir->upper-bound env) iteration) 1))
            )
            (not
                (equal
                    index
                    (+ (- (envir->upper-bound env) iteration) 1)
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32)
        :use rep-lemma-32
    )
   (defrule rep-lemma-34
        (implies
            (and
                (not (zp iteration))
                (not (frame->loop-break (rep iteration env fr)))
                (natp iteration)
                (natp index)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (not (frame->loop-break (rep iteration env fr)))
                (> index (+ (frame->j (rep (- iteration 1) env fr)) 1))               
            )
            (not
                (equal
                    index
                    (+ (- (envir->upper-bound env) iteration) 1)
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33)
        :use (rep-lemma-32 rep-lemma-33)
    )
    (defrule rep-lemma-35
        (implies
            (and
                (not (zp iteration))
                (not (frame->loop-break (rep iteration env fr)))
                (natp iteration)
                (natp index)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (> index (+ (- (envir->upper-bound env) iteration) 1))
            )
            (equal
                (nth
                    index
                    (frame->a (rep (- iteration 1) env fr))
                )
                (nth
                    index
                    (update-nth
                        (+ (- (envir->upper-bound env) iteration) 1)
                        (nth
                            (- (envir->upper-bound env) iteration)
                            (frame->a (rep (- iteration 1) env fr))
                        )
                        (frame->a (rep (- iteration 1) env fr))
                    )
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33)
        :use (rep-lemma-33
              rep-lemma-12)
    )
    (defrule rep-lemma-36
        (implies
            (and
                (not (zp iteration))
                (not (frame->loop-break (rep iteration env fr)))
                (natp iteration)
                (natp index)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (not (frame->loop-break (rep iteration env fr)))
                (> index (+ (frame->j (rep (- iteration 1) env fr)) 1))               
            )
            (equal
                (nth
                    index
                    (frame->a (rep (- iteration 1) env fr))
                )
                (nth
                    index
                    (update-nth
                        (+ (- (envir->upper-bound env) iteration) 1)
                        (nth
                            (- (envir->upper-bound env) iteration)
                            (frame->a (rep (- iteration 1) env fr))
                        )
                        (frame->a (rep (- iteration 1) env fr))
                    )
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35)
        :use (rep-lemma-35
              rep-lemma-32)
    )
    (defrule rep-lemma-37
        (implies
            (and
                (not (zp iteration))
                (not (frame->loop-break (rep iteration env fr)))
                (natp iteration)
                (natp index)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
            )
            (equal
                (nth
                    (- (envir->upper-bound env) iteration)
                    (frame->a fr)
                )
                (nth
                    (- (envir->upper-bound env) iteration)
                    (frame->a (rep iteration env fr))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28)
        :use (rep-lemma-9
              (:instance rep-lemma-28 (index (- (envir->upper-bound env) iteration))))
    )
    (defrule rep-lemma-38
        (implies
            (and
                (not (zp iteration))
                (not (frame->loop-break (rep iteration env fr)))
                (natp iteration)
                (natp index)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
            )
            (equal
                (nth
                    (- (envir->upper-bound env) iteration)
                    (frame->a fr)
                )
                (nth
                    (- (envir->upper-bound env) iteration)
                    (frame->a (rep (- iteration 1) env fr))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37)
        :use (rep-lemma-37
              (:instance rep-lemma-20 (index (- (envir->upper-bound env) iteration))))
    )
    (defrule rep-lemma-39
        (implies
            (and
                (natp iteration)
                (natp index)
                (<= iteration (envir->upper-bound env))
                (<= index (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (>= index (+ (frame->j (rep iteration env fr)) 2))
            )
            (equal
                (nth
                    (- index 1)
                    (frame->a fr)
                )
                (nth
                    index
                    (frame->a (rep iteration env fr))
                )
            )
        )
        :induct (dec-induct iteration)
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37)
        :hints (("Subgoal *1/2"
                 :cases ((not (frame->loop-break (rep iteration env fr)))))
                 ("Subgoal *1/2.2"
                 :use rep-lemma-8
                 :expand (rep iteration env fr)
                 :do-not-induct t)
                 ("Subgoal *1/2.1"
                 :use rep-lemma-31
                 :cases ((> index (+ (frame->j (rep (- iteration 1) env fr)) 1))
                         (= index (+ (frame->j (rep (- iteration 1) env fr)) 1)))
                 :do-not-induct t)
                 ("Subgoal *1/2.1.2"
                 :use (rep-lemma-19 rep-lemma-36)
                 :expand (rep iteration env fr)
                 :do-not-induct t)
                 ("Subgoal *1/2.1.1"
                 :use (rep-lemma-9 rep-lemma-19 rep-lemma-30 rep-lemma-38)
                 :expand (rep iteration env fr)
                 :do-not-induct t))
    )
    (defrule rep-lemma-40
        (implies
            (and
                (natp iteration)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
            )
            (natp (+ (+ (frame->j (rep iteration env fr)) 1) 1))
        )
        :induct (dec-induct iteration)
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39)
        :hints (("Subgoal *1/2"
                 :cases ((not (frame->loop-break (rep iteration env fr)))))
                 ("Subgoal *1/2.2"
                 :use rep-lemma-8
                 :do-not-induct t)
                 ("Subgoal *1/2.1"
                 :use rep-lemma-9
                 :do-not-induct t))
    )
    (defrule rep-lemma-41
        (implies
            (and
                (natp iteration)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
            )
            (< (+ (frame->j (rep iteration env fr)) 1) (len (frame->a fr)))
        )
        :induct (dec-induct iteration)
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40)
        :hints (("Subgoal *1/2"
                 :cases ((not (frame->loop-break (rep iteration env fr)))))
                 ("Subgoal *1/2.2"
                 :use rep-lemma-8
                 :do-not-induct t)
                 ("Subgoal *1/2.1"
                 :use rep-lemma-9
                 :do-not-induct t))
    )
    (defrule rep-lemma-42
        (implies
            (and
                (natp iteration)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
            )
            (< (+ (frame->j (rep iteration env fr)) 1) (len (frame->a (rep iteration env fr))))
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41)
        :use (rep-lemma-7 rep-lemma-41)
    )
    (defrule rep-lemma-43
        (implies
            (and
                (natp iteration)
                (natp bound)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (<= bound (+ (frame->j (rep iteration env fr)) 1))
            )
            (equal-ranges
                bound
                (+ (frame->j (rep iteration env fr)) 1)
                bound
                (+ (frame->j (rep iteration env fr)) 1)
                (frame->a fr)
                (frame->a (rep iteration env fr))
            )
        )
        :induct (inc-induct bound (+ (+ (frame->j (rep iteration env fr)) 1) 1))
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 equal-ranges-sublists
                  equal-ranges-sublists-1)
        :enable (equal-ranges)
        :hints (("Subgoal *1/1"
                 :cases ((<= (+ bound 1) (+ (frame->j (rep iteration env fr)) 1))))
                ("Subgoal *1/1.2"
                 :do-not-induct t
                 :use (rep-lemma-41 rep-lemma-42
                      (:instance rep-lemma-27 (index bound)))
                 :expand
                     (equal-ranges
                         bound
                         (+ (frame->j (rep iteration env fr)) 1)
                         bound
                         (+ (frame->j (rep iteration env fr)) 1)
                         (frame->a fr)
                         (frame->a (rep iteration env fr))
                     )
                )
                ("Subgoal *1/1.1"
                 :do-not-induct t
                 :use (rep-lemma-41 rep-lemma-42
                      (:instance rep-lemma-27 (index bound)))
                 :expand
                     (equal-ranges
                         bound
                         (+ (frame->j (rep iteration env fr)) 1)
                         bound
                         (+ (frame->j (rep iteration env fr)) 1)
                         (frame->a fr)
                         (frame->a (rep iteration env fr))
                     )
                ))
    )
    (defrule rep-lemma-44
        (implies
            (and
                (natp iteration)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
            )
            (< 0 (+ (+ (frame->j (rep iteration env fr)) 1) 1))
        )
        :induct (dec-induct iteration)
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 equal-ranges-sublists equal-ranges-sublists-1)
        :hints (("Subgoal *1/2"
                 :cases ((not (frame->loop-break (rep iteration env fr)))))
                 ("Subgoal *1/2.2"
                 :use rep-lemma-8
                 :do-not-induct t)
                 ("Subgoal *1/2.1"
                 :use rep-lemma-9
                 :do-not-induct t))
    )
    (defrule rep-lemma-45
        (implies
            (and
                (natp iteration)
                (natp bound)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (<= (+ (frame->j (rep iteration env fr)) 2) bound)
                (<= bound (envir->upper-bound env))
            )
            (equal-ranges
                (- bound 1)
                (- (envir->upper-bound env) 1)
                bound
                (envir->upper-bound env)
                (frame->a fr)
                (frame->a (rep iteration env fr))
            )
        )
        :induct (inc-induct bound (+ (envir->upper-bound env) 1))
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1)
        :enable (equal-ranges)
        :hints (("Subgoal *1/1"
                 :cases ((<= (+ bound 1) (envir->upper-bound env))))
                ("Subgoal *1/1.2"
                 :do-not-induct t
                 :use (rep-lemma-7 rep-lemma-44
                      (:instance rep-lemma-39 (index bound)))
                 :expand
                     (equal-ranges
                         (- bound 1)
                         (- (envir->upper-bound env) 1)
                         bound
                         (envir->upper-bound env)
                         (frame->a fr)
                         (frame->a (rep iteration env fr))
                     )
                )
                ("Subgoal *1/1.1"
                 :do-not-induct t
                 :use (rep-lemma-7 rep-lemma-44
                      (:instance rep-lemma-39 (index bound)))
                 :expand
                     (equal-ranges
                         (- bound 1)
                         (- (envir->upper-bound env) 1)
                         bound
                         (envir->upper-bound env)
                         (frame->a fr)
                         (frame->a (rep iteration env fr))
                     )
                ))
        )
    )
    (defrule rep-lemma-46
        (implies
            (and
                (natp iteration)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
            )
            (natp (+ (frame->j (rep iteration env fr)) 1))
        )
        :induct (dec-induct iteration)
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1)
        :hints (("Subgoal *1/2"
                 :cases ((not (frame->loop-break (rep iteration env fr)))))
                 ("Subgoal *1/2.2"
                 :use rep-lemma-8
                 :do-not-induct t)
                 ("Subgoal *1/2.1"
                 :use rep-lemma-9
                 :do-not-induct t)
                 ("Subgoal *1/1"
                 :expand (rep 0 env fr)
                 :do-not-induct t))
    )
    (defrule rep-lemma-47
        (implies
            (and
                (natp iteration)
                (natp bound)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (<= bound (+ (frame->j (rep iteration env fr)) 1))
            )
            (equal
                (sublist 
                    bound
                    (+ (frame->j (rep iteration env fr)) 1)
                    (frame->a fr)
                )
                (sublist
                    bound
                    (+ (frame->j (rep iteration env fr)) 1)
                    (frame->a (rep iteration env fr))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep-lemma-46)
        :use (rep-lemma-41 rep-lemma-42 rep-lemma-43 rep-lemma-46
               (:instance equal-ranges-sublists
                (i1 bound)
                (i2 bound)
                (j1 (+ (frame->j (rep iteration env fr)) 1))
                (j2 (+ (frame->j (rep iteration env fr)) 1))
                (l1 (frame->a fr))
                (l2 (frame->a (rep iteration env fr)))))
    )
    (defrule rep-lemma-48
        (implies
            (and
                (natp iteration)
                (natp bound)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (<= (+ (frame->j (rep iteration env fr)) 2) bound)
                (<= bound (envir->upper-bound env))
            )
            (natp (- bound 1))
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep-lemma-45 rep-lemma-46)
        :use (rep-lemma-44 rep-lemma-46)
    )
    (defrule rep-lemma-49
        (implies
            (and
                (natp iteration)
                (natp bound)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (<= (+ (frame->j (rep iteration env fr)) 2) bound)
                (<= bound (envir->upper-bound env))
            )
            (natp (- (envir->upper-bound env) 1))
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-48)
        :use (rep-lemma-44 rep-lemma-46 rep-lemma-48)
    )
    (defrule rep-lemma-50
        (implies
            (and
                (natp iteration)
                (natp bound)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (<= (+ (frame->j (rep iteration env fr)) 2) bound)
                (<= bound (envir->upper-bound env))
            )
            (natp (envir->upper-bound env))
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-48
                  rep-lemma-48)
        :use (rep-lemma-44 rep-lemma-46 rep-lemma-48 rep-lemma-48)
    )
    (defrule rep-lemma-51
        (implies
            (and
                (natp iteration)
                (natp bound)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (<= (+ (frame->j (rep iteration env fr)) 2) bound)
                (<= bound (envir->upper-bound env))
            )
            (equal
                (sublist 
                    (- bound 1)
                    (- (envir->upper-bound env) 1)
                    (frame->a fr)
                )
                (sublist
                    bound
                    (envir->upper-bound env)
                    (frame->a (rep iteration env fr))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50)
        :use (rep-lemma-7 rep-lemma-41 rep-lemma-42 rep-lemma-44 rep-lemma-45 rep-lemma-46
              rep-lemma-48 rep-lemma-49 rep-lemma-50
               (:instance equal-ranges-sublists
                (i1 (- bound 1))
                (i2 bound)
                (j1 (- (envir->upper-bound env) 1))
                (j2 (envir->upper-bound env))
                (l1 (frame->a fr))
                (l2 (frame->a (rep iteration env fr)))))
    )
    (defrule rep-lemma-52
        (implies
            (and
                (natp iteration)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (<= 0 (+ (frame->j (rep iteration env fr)) 1))
            )
            (equal
                (sublist 
                    0
                    (+ (frame->j (rep iteration env fr)) 1)
                    (frame->a fr)
                )
                (sublist
                    0
                    (+ (frame->j (rep iteration env fr)) 1)
                    (frame->a (rep iteration env fr))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51)
        :use (rep-lemma-46
               (:instance rep-lemma-47 (bound 0)))
    )
    (defrule rep-lemma-53
        (implies
            (and
                (natp iteration)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (<= (+ (frame->j (rep iteration env fr)) 2) (envir->upper-bound env))
            )
            (equal
                (sublist 
                    (+ (frame->j (rep iteration env fr)) 1)
                    (- (envir->upper-bound env) 1)
                    (frame->a fr)
                )
                (sublist
                    (+ (frame->j (rep iteration env fr)) 2)
                    (envir->upper-bound env)
                    (frame->a (rep iteration env fr))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52)
        :use (rep-lemma-40
               (:instance rep-lemma-51
                (bound (+ (frame->j (rep iteration env fr)) 2))))
    )
    (defrule rep-lemma-54
        (implies
            (and
                (natp iteration)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
            )
            (equal
                (sublist 
                    0
                    (+ (frame->j (rep iteration env fr)) 1)
                    (frame->a fr)
                )
                (sublist
                    0
                    (+ (frame->j (rep iteration env fr)) 1)
                    (frame->a (rep iteration env fr))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist-2 sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53)
         :enable (sublist)
         :hints (("Goal" :cases ((<= 0 (+ (frame->j (rep iteration env fr)) 1))))
                 ("Subgoal 2" :expand
                     (
                         (sublist 
                             0
                             (+ (frame->j (rep iteration env fr)) 1)
                             (frame->a fr)
                         )
                         (sublist 
                             0
                             (+ (frame->j (rep iteration env fr)) 1)
                             (frame->a (rep iteration env fr))
                         )
                     )
                 )
                 ("Subgoal 1" :use rep-lemma-52))
    )
    (defrule rep-lemma-55
        (implies
            (and
                (natp iteration)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))              
            )
            (equal
                (sublist 
                    (+ (frame->j (rep iteration env fr)) 1)
                    (- (envir->upper-bound env) 1)
                    (frame->a fr)
                )
                (sublist
                    (+ (frame->j (rep iteration env fr)) 2)
                    (envir->upper-bound env)
                    (frame->a (rep iteration env fr))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist-2 sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53 rep-lemma-54)
         :enable (sublist)
         :hints (("Goal"
                 :cases ((<=
                            (+ (frame->j (rep iteration env fr)) 2)
                            (envir->upper-bound env))))
                 ("Subgoal 2" :expand
                     (
                         (sublist 
                             (+ (frame->j (rep iteration env fr)) 1)
                             (- (envir->upper-bound env) 1)
                             (frame->a fr)
                         )
                         (sublist 
                             (+ (frame->j (rep iteration env fr)) 2)
                             (envir->upper-bound env)
                             (frame->a (rep iteration env fr))
                         )
                     )
                 )
                 ("Subgoal 1" :use rep-lemma-53))
    )
    (defrule rep-lemma-56
        (implies
            (and
                (natp iteration)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
            )
            (equal
                (sublist 
                    (+ (frame->j (rep iteration env fr)) 1)
                    (+ (frame->j (rep iteration env fr)) 1)
                    (frame->a fr)
                )
                (sublist
                    (+ (frame->j (rep iteration env fr)) 1)
                    (+ (frame->j (rep iteration env fr)) 1)
                    (frame->a (rep iteration env fr))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53 rep-lemma-54 rep-lemma-55)
        :use (rep-lemma-46
               (:instance rep-lemma-47
                (bound (+ (frame->j (rep iteration env fr)) 1))))
    )
    (defrule rep-lemma-57
        (implies
            (and
                (natp iteration)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
            )
            (integer-listp
                (sublist 
                    0
                    (+ (frame->j (rep iteration env fr)) 1)
                    (frame->a fr)
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53 rep-lemma-54 rep-lemma-55 rep-lemma-56 sublist-6)
        :use (rep-lemma-5 rep-lemma-46
              (:instance sublist-6
               (i 0)
               (j (+ (frame->j (rep iteration env fr)) 1))
               (u (frame->a fr))))
    )
    (defrule rep-lemma-58
        (implies
            (and
                (natp iteration)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
            )
            (integer-listp
                (sublist 
                    0
                    (+ (frame->j (rep iteration env fr)) 1)
                    (frame->a (rep iteration env fr))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53 rep-lemma-54 rep-lemma-55 rep-lemma-56 sublist-6
                  rep-lemma-57)
        :use (rep-lemma-5 rep-lemma-46
              (:instance sublist-6
               (i 0)
               (j (+ (frame->j (rep iteration env fr)) 1))
               (u (frame->a (rep iteration env fr)))))
    )
    (defrule rep-lemma-59
        (implies
            (and
                (natp iteration)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
            )
            (element-list-equiv
                (sublist 
                    0
                    (+ (frame->j (rep iteration env fr)) 1)
                    (frame->a fr)
                )
                (sublist
                    0
                    (+ (frame->j (rep iteration env fr)) 1)
                    (frame->a (rep iteration env fr))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53 rep-lemma-54 rep-lemma-55 rep-lemma-56 sublist-6
                  rep-lemma-57 rep-lemma-58)
        :use (rep-lemma-54 rep-lemma-57 rep-lemma-58)
    )
    (defrule rep-lemma-60
        (implies
            (and
                (natp iteration)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
            )
            (integer-listp
                (sublist 
                    (+ (frame->j (rep iteration env fr)) 1)
                    (+ (frame->j (rep iteration env fr)) 1)
                    (frame->a fr)
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53 rep-lemma-54 rep-lemma-55 rep-lemma-56 sublist-6
                  rep-lemma-57 rep-lemma-58 rep-lemma-59)
        :use (rep-lemma-5 rep-lemma-46
              (:instance sublist-6
               (i (+ (frame->j (rep iteration env fr)) 1))
               (j (+ (frame->j (rep iteration env fr)) 1))
               (u (frame->a fr))))
    )
    (defrule rep-lemma-61
        (implies
            (and
                (natp iteration)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
            )
            (integer-listp
                (sublist 
                    (+ (frame->j (rep iteration env fr)) 1)
                    (+ (frame->j (rep iteration env fr)) 1)
                    (frame->a (rep iteration env fr))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53 rep-lemma-54 rep-lemma-55 rep-lemma-56 sublist-6
                  rep-lemma-57 rep-lemma-58 rep-lemma-59 rep-lemma-60)
        :use (rep-lemma-5 rep-lemma-46
              (:instance sublist-6
               (i (+ (frame->j (rep iteration env fr)) 1))
               (j (+ (frame->j (rep iteration env fr)) 1))
               (u (frame->a (rep iteration env fr)))))
    )
    (defrule rep-lemma-62
        (implies
            (and
                (natp iteration)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
            )
            (element-list-equiv
                (sublist 
                    (+ (frame->j (rep iteration env fr)) 1)
                    (+ (frame->j (rep iteration env fr)) 1)
                    (frame->a fr)
                )
                (sublist
                    (+ (frame->j (rep iteration env fr)) 1)
                    (+ (frame->j (rep iteration env fr)) 1)
                    (frame->a (rep iteration env fr))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53 rep-lemma-54 rep-lemma-55 rep-lemma-56 sublist-6
                  rep-lemma-57 rep-lemma-58 rep-lemma-59 rep-lemma-60 rep-lemma-61)
        :use (rep-lemma-56 rep-lemma-60 rep-lemma-61)
    )
    (defrule rep-lemma-63
        (implies
            (and
                (natp iteration)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
            )
            (integer-listp
                (sublist 
                    (+ (frame->j (rep iteration env fr)) 1)
                    (- (envir->upper-bound env) 1)
                    (frame->a fr)
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist-2 sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53 rep-lemma-54 rep-lemma-55 rep-lemma-56 sublist-6
                  rep-lemma-57 rep-lemma-58 rep-lemma-59 rep-lemma-60 rep-lemma-61
                  rep-lemma-62)
        :enable (sublist)
        :hints (("Goal" :cases ((natp (- (envir->upper-bound env) 1))))
                ("Subgoal 2" :use (rep-lemma-5 rep-lemma-46)
                    :expand
                    (
                       (integer-listp
                           (sublist 
                               (+ (frame->j (rep iteration env fr)) 1)
                               (- (envir->upper-bound env) 1)
                               (frame->a fr)
                           )
                       )))
                ("Subgoal 1" :use (rep-lemma-5 rep-lemma-46
                    (:instance sublist-6
                        (i (+ (frame->j (rep iteration env fr)) 1))
                        (j (- (envir->upper-bound env) 1))
                        (u (frame->a fr))
                    ))))
    )
    (defrule rep-lemma-64
        (implies
            (and
                (natp iteration)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
            )
            (integer-listp
                (sublist 
                    (+ (frame->j (rep iteration env fr)) 2)
                    (envir->upper-bound env)
                    (frame->a (rep iteration env fr))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist-2 sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53 rep-lemma-54 rep-lemma-55 rep-lemma-56 sublist-6
                  rep-lemma-57 rep-lemma-58 rep-lemma-59 rep-lemma-60 rep-lemma-61
                  rep-lemma-62 rep-lemma-63)
        :enable (sublist)
        :hints (("Goal" :cases ((natp (envir->upper-bound env))))
                ("Subgoal 2" :use (rep-lemma-5 rep-lemma-40)
                    :expand
                    (
                       (integer-listp
                           (sublist 
                               (+ (frame->j (rep iteration env fr)) 2)
                               (envir->upper-bound env)
                               (frame->a (rep iteration env fr))
                           )
                       )))
                ("Subgoal 1" :use (rep-lemma-5 rep-lemma-40
                    (:instance sublist-6
                        (i (+ (frame->j (rep iteration env fr)) 2))
                        (j (envir->upper-bound env))
                        (u (frame->a (rep iteration env fr)))
                    ))))
    )
    (defrule rep-lemma-65
        (implies
            (and
                (natp iteration)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
            )
            (element-list-equiv
                (sublist 
                    (+ (frame->j (rep iteration env fr)) 1)
                    (- (envir->upper-bound env) 1)
                    (frame->a fr)
                )
                (sublist
                    (+ (frame->j (rep iteration env fr)) 2)
                    (envir->upper-bound env)
                    (frame->a (rep iteration env fr))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53 rep-lemma-54 rep-lemma-55 rep-lemma-56 sublist-6
                  rep-lemma-57 rep-lemma-58 rep-lemma-59 rep-lemma-60 rep-lemma-61
                  rep-lemma-62 rep-lemma-63 rep-lemma-64)
        :use (rep-lemma-55 rep-lemma-63 rep-lemma-64) 
    )
    (defrule rep-lemma-66
        (implies
            (and
                (natp (frame->j (rep iteration env fr)))
                (natp iteration)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
            )
            (equal
                (sublist
                    0
                    (+ (frame->j (rep iteration env fr)) 1)
                    (frame->a fr)
                )
                (append
                    (sublist
                        0
                        (frame->j (rep iteration env fr))
                        (frame->a fr)
                    )
                    (sublist
                        (+ (frame->j (rep iteration env fr)) 1)
                        (+ (frame->j (rep iteration env fr)) 1)
                        (frame->a fr)
                    )
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53 rep-lemma-54 rep-lemma-55 rep-lemma-56 sublist-6
                  rep-lemma-57 rep-lemma-58 rep-lemma-59 rep-lemma-60 rep-lemma-61
                  rep-lemma-62 rep-lemma-63 rep-lemma-64 rep-lemma-65)
        :hints (("Goal" :use
                  (rep-lemma-41 rep-lemma-46
                    (:instance sublist-14
                      (i 0)
                      (j (frame->j (rep iteration env fr)))
                      (k (+ (frame->j (rep iteration env fr)) 1))
                      (u (frame->a fr))
                    )
                  )
                )) 
    )
    (defrule rep-lemma-67
        (implies
            (and
                (natp iteration)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
            )
            (equal
                (sublist
                    0
                    (+ (frame->j (rep iteration env fr)) 1)
                    (frame->a fr)
                )
                (append
                    (sublist
                        0
                        (frame->j (rep iteration env fr))
                        (frame->a fr)
                    )
                    (sublist
                        (+ (frame->j (rep iteration env fr)) 1)
                        (+ (frame->j (rep iteration env fr)) 1)
                        (frame->a fr)
                    )
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist-2 sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53 rep-lemma-54 rep-lemma-55 rep-lemma-56 sublist-6
                  rep-lemma-57 rep-lemma-58 rep-lemma-59 rep-lemma-60 rep-lemma-61
                  rep-lemma-62 rep-lemma-63 rep-lemma-64 rep-lemma-65 rep-lemma-66)
        :enable (sublist)
        :hints (("Goal" :cases ((natp (frame->j (rep iteration env fr)))))
                ("Subgoal 2" :expand
                  (
                    (sublist
                        0
                        (frame->j (rep iteration env fr))
                        (frame->a fr)
                    )
                  )
                )
                ("Subgoal 1" :use (rep-lemma-66)
                )) 
    )
    (defrule rep-lemma-68
        (implies
            (and
                (natp iteration)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
            )
            (integer-listp
                (sublist 
                    0
                    (frame->j (rep iteration env fr))
                    (frame->a fr)
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist-2 sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53 rep-lemma-54 rep-lemma-55 rep-lemma-56 sublist-6
                  rep-lemma-57 rep-lemma-58 rep-lemma-59 rep-lemma-60 rep-lemma-61
                  rep-lemma-62 rep-lemma-63 rep-lemma-64 rep-lemma-65 rep-lemma-66
                  rep-lemma-67)
        :enable (sublist)
        :hints (("Goal" :cases ((natp (frame->j (rep iteration env fr)))))
                ("Subgoal 2" :use (rep-lemma-5)
                    :expand
                    (
                       (integer-listp
                           (sublist 
                               0
                               (frame->j (rep iteration env fr))
                               (frame->a fr)
                           )
                       )))
                ("Subgoal 1" :use (rep-lemma-5
                    (:instance sublist-6
                        (i 0)
                        (j (frame->j (rep iteration env fr)))
                        (u (frame->a fr))
                    ))))
    )
    (defrule rep-lemma-69
        (implies
            (and
                (natp iteration)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
            )
            (element-list-equiv
                (sublist
                    0
                    (+ (frame->j (rep iteration env fr)) 1)
                    (frame->a fr)
                )
                (append
                    (sublist
                        0
                        (frame->j (rep iteration env fr))
                        (frame->a fr)
                    )
                    (sublist
                        (+ (frame->j (rep iteration env fr)) 1)
                        (+ (frame->j (rep iteration env fr)) 1)
                        (frame->a fr)
                    )
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53 rep-lemma-54 rep-lemma-55 rep-lemma-56 sublist-6
                  rep-lemma-57 rep-lemma-58 rep-lemma-59 rep-lemma-60 rep-lemma-61
                  rep-lemma-62 rep-lemma-63 rep-lemma-64 rep-lemma-65 rep-lemma-66
                  rep-lemma-67 rep-lemma-68)
        :use (rep-lemma-57 rep-lemma-60 rep-lemma-67 rep-lemma-68)
    )
    (defrule rep-lemma-70
        (implies
            (and
                (natp (frame->j (rep iteration env fr)))
                (natp iteration)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
            )
            (equal
                (sublist
                    0
                    (+ (frame->j (rep iteration env fr)) 1)
                    (frame->a (rep iteration env fr))
                )
                (append
                    (sublist
                        0
                        (frame->j (rep iteration env fr))
                        (frame->a (rep iteration env fr))
                    )
                    (sublist
                        (+ (frame->j (rep iteration env fr)) 1)
                        (+ (frame->j (rep iteration env fr)) 1)
                        (frame->a (rep iteration env fr))
                    )
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53 rep-lemma-54 rep-lemma-55 rep-lemma-56 sublist-6
                  rep-lemma-57 rep-lemma-58 rep-lemma-59 rep-lemma-60 rep-lemma-61
                  rep-lemma-62 rep-lemma-63 rep-lemma-64 rep-lemma-65 rep-lemma-66
                  rep-lemma-67 rep-lemma-68 rep-lemma-69)
        :hints (("Goal" :use
                  (rep-lemma-7 rep-lemma-41 rep-lemma-46
                    (:instance sublist-14
                      (i 0)
                      (j (frame->j (rep iteration env fr)))
                      (k (+ (frame->j (rep iteration env fr)) 1))
                      (u (frame->a (rep iteration env fr)))
                    )
                  )
                )) 
    )
    (defrule rep-lemma-71
        (implies
            (and
                (natp iteration)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
            )
            (equal
                (sublist
                    0
                    (+ (frame->j (rep iteration env fr)) 1)
                    (frame->a (rep iteration env fr))
                )
                (append
                    (sublist
                        0
                        (frame->j (rep iteration env fr))
                        (frame->a (rep iteration env fr))
                    )
                    (sublist
                        (+ (frame->j (rep iteration env fr)) 1)
                        (+ (frame->j (rep iteration env fr)) 1)
                        (frame->a (rep iteration env fr))
                    )
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist-2 sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53 rep-lemma-54 rep-lemma-55 rep-lemma-56 sublist-6
                  rep-lemma-57 rep-lemma-58 rep-lemma-59 rep-lemma-60 rep-lemma-61
                  rep-lemma-62 rep-lemma-63 rep-lemma-64 rep-lemma-65 rep-lemma-66
                  rep-lemma-67 rep-lemma-68 rep-lemma-69 rep-lemma-70)
        :enable (sublist)
        :hints (("Goal" :cases ((natp (frame->j (rep iteration env fr)))))
                ("Subgoal 2" :expand
                  (
                    (sublist
                        0
                        (frame->j (rep iteration env fr))
                        (frame->a (rep iteration env fr))
                    )
                  )
                )
                ("Subgoal 1" :use (rep-lemma-70)
                )) 
    )
    (defrule rep-lemma-72
        (implies
            (and
                (natp iteration)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
            )
            (integer-listp
                (sublist 
                    0
                    (frame->j (rep iteration env fr))
                    (frame->a (rep iteration env fr))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist-2 sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53 rep-lemma-54 rep-lemma-55 rep-lemma-56 sublist-6
                  rep-lemma-57 rep-lemma-58 rep-lemma-59 rep-lemma-60 rep-lemma-61
                  rep-lemma-62 rep-lemma-63 rep-lemma-64 rep-lemma-65 rep-lemma-66
                  rep-lemma-67 rep-lemma-68 rep-lemma-69 rep-lemma-70 rep-lemma-71)
        :enable (sublist)
        :hints (("Goal" :cases ((natp (frame->j (rep iteration env fr)))))
                ("Subgoal 2" :use (rep-lemma-5)
                    :expand
                    (
                       (integer-listp
                           (sublist 
                               0
                               (frame->j (rep iteration env fr))
                               (frame->a (rep iteration env fr))
                           )
                       )))
                ("Subgoal 1" :use (rep-lemma-5
                    (:instance sublist-6
                        (i 0)
                        (j (frame->j (rep iteration env fr)))
                        (u (frame->a (rep iteration env fr)))
                    ))))
    )
    (defrule rep-lemma-73
        (implies
            (and
                (natp iteration)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
            )
            (element-list-equiv
                (sublist
                    0
                    (+ (frame->j (rep iteration env fr)) 1)
                    (frame->a (rep iteration env fr))
                )
                (append
                    (sublist
                        0
                        (frame->j (rep iteration env fr))
                        (frame->a (rep iteration env fr))
                    )
                    (sublist
                        (+ (frame->j (rep iteration env fr)) 1)
                        (+ (frame->j (rep iteration env fr)) 1)
                        (frame->a (rep iteration env fr))
                    )
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53 rep-lemma-54 rep-lemma-55 rep-lemma-56 sublist-6
                  rep-lemma-57 rep-lemma-58 rep-lemma-59 rep-lemma-60 rep-lemma-61
                  rep-lemma-62 rep-lemma-63 rep-lemma-64 rep-lemma-65 rep-lemma-66
                  rep-lemma-67 rep-lemma-68 rep-lemma-69 rep-lemma-70 rep-lemma-71
                  rep-lemma-72)
        :use (rep-lemma-58 rep-lemma-61 rep-lemma-71 rep-lemma-72)
    )
    (defrule rep-lemma-74
        (implies
            (and
                (natp iteration)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
            )
            (equal
                (append
                    (sublist
                        0
                        (frame->j (rep iteration env fr))
                        (frame->a fr)
                    )
                    (sublist
                        (+ (frame->j (rep iteration env fr)) 1)
                        (+ (frame->j (rep iteration env fr)) 1)
                        (frame->a fr)
                    )
                )
                (append
                    (sublist
                        0
                        (frame->j (rep iteration env fr))
                        (frame->a (rep iteration env fr))
                    )
                    (sublist
                        (+ (frame->j (rep iteration env fr)) 1)
                        (+ (frame->j (rep iteration env fr)) 1)
                        (frame->a (rep iteration env fr))
                    )
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53 rep-lemma-54 rep-lemma-55 rep-lemma-56 sublist-6
                  rep-lemma-57 rep-lemma-58 rep-lemma-59 rep-lemma-60 rep-lemma-61
                  rep-lemma-62 rep-lemma-63 rep-lemma-64 rep-lemma-65 rep-lemma-66
                  rep-lemma-67 rep-lemma-68 rep-lemma-69 rep-lemma-70 rep-lemma-71
                  rep-lemma-72 rep-lemma-73)
        :use (rep-lemma-54 rep-lemma-67 rep-lemma-71)
    )
    (defrule rep-lemma-75
        (implies
            (and
                (natp iteration)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
            )
            (equal
                (append
                    (sublist
                        0
                        (frame->j (rep iteration env fr))
                        (frame->a fr)
                    )
                    (sublist
                        (+ (frame->j (rep iteration env fr)) 1)
                        (+ (frame->j (rep iteration env fr)) 1)
                        (frame->a fr)
                    )
                )
                (append
                    (sublist
                        0
                        (frame->j (rep iteration env fr))
                        (frame->a (rep iteration env fr))
                    )
                    (sublist
                        (+ (frame->j (rep iteration env fr)) 1)
                        (+ (frame->j (rep iteration env fr)) 1)
                        (frame->a fr)
                    )
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  append element-list-equiv-implies-element-list-equiv-append-2
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53 rep-lemma-54 rep-lemma-55 rep-lemma-56 sublist-6
                  rep-lemma-57 rep-lemma-58 rep-lemma-59 rep-lemma-60 rep-lemma-61
                  rep-lemma-62 rep-lemma-63 rep-lemma-64 rep-lemma-65 rep-lemma-66
                  rep-lemma-67 rep-lemma-68 rep-lemma-69 rep-lemma-70 rep-lemma-71
                  rep-lemma-72 rep-lemma-73 rep-lemma-74)
        :use (rep-lemma-56 rep-lemma-60 rep-lemma-61 rep-lemma-74
              (:instance element-list-equiv-implies-element-list-equiv-append-2
               (a 
                    (sublist
                        0
                        (frame->j (rep iteration env fr))
                        (frame->a (rep iteration env fr))
                    )
               )
               (b
                    (sublist
                        (+ (frame->j (rep iteration env fr)) 1)
                        (+ (frame->j (rep iteration env fr)) 1)
                        (frame->a (rep iteration env fr))
                    )
               )
               (b-equiv
                    (sublist
                        (+ (frame->j (rep iteration env fr)) 1)
                        (+ (frame->j (rep iteration env fr)) 1)
                        (frame->a fr)
                    )
               )
               ))
    )
    (defrule rep-lemma-76
        (implies
            (and
                (natp iteration)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
            )
            (equal
                (sublist
                    0
                    (frame->j (rep iteration env fr))
                    (frame->a fr)
                )
                (sublist
                     0
                     (frame->j (rep iteration env fr))
                     (frame->a (rep iteration env fr))
                 )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53 rep-lemma-54 rep-lemma-55 rep-lemma-56 sublist-6
                  rep-lemma-57 rep-lemma-58 rep-lemma-59 rep-lemma-60 rep-lemma-61
                  rep-lemma-62 rep-lemma-63 rep-lemma-64 rep-lemma-65 rep-lemma-66
                  rep-lemma-67 rep-lemma-68 rep-lemma-69 rep-lemma-70 rep-lemma-71
                  rep-lemma-72 rep-lemma-73 rep-lemma-74 rep-lemma-75)
        :use (rep-lemma-68 rep-lemma-72 rep-lemma-75
              (:instance equal-of-appends-when-true-listps
               (x1 
                    (sublist
                        0
                        (frame->j (rep iteration env fr))
                        (frame->a fr)
                    )
               )
               (x2 
                    (sublist
                        0
                        (frame->j (rep iteration env fr))
                        (frame->a (rep iteration env fr))
                    )
               )
               (y 
                    (sublist
                        (+ (frame->j (rep iteration env fr)) 1)
                        (+ (frame->j (rep iteration env fr)) 1)
                        (frame->a fr)
                    )
               )
              ))
    )
    (defrule rep-lemma-77
        (implies
            (and
                (natp iteration)
                (natp index)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (< (envir->upper-bound env) index)
                (< index (len (frame->a fr)))
            )
            (equal
                (nth
                    index
                    (frame->a (rep (- iteration 1) env fr))
                )
                (nth
                    index
                    (frame->a (rep iteration env fr))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53 rep-lemma-54 rep-lemma-55 rep-lemma-56 sublist-6
                  rep-lemma-57 rep-lemma-58 rep-lemma-59 rep-lemma-60 rep-lemma-61
                  rep-lemma-62 rep-lemma-63 rep-lemma-64 rep-lemma-65 rep-lemma-66
                  rep-lemma-67 rep-lemma-68 rep-lemma-69 rep-lemma-70 rep-lemma-71
                  rep-lemma-72 rep-lemma-73 rep-lemma-74 rep-lemma-75 rep-lemma-76)
        :hints (("Goal"
                 :cases ((frame->loop-break (rep iteration env fr))))
                 ("Subgoal 2"
                  :use (rep-lemma-22))
                 ("Subgoal 1"
                  :expand ((rep iteration env fr)))
               )
    )
    (defrule rep-lemma-78
        (implies
            (and
                (natp iteration)
                (natp index)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (< (envir->upper-bound env) index)
                (< index (len (frame->a fr)))
            )
            (equal
                (nth
                    index
                    (frame->a fr)
                )
                (nth
                    index
                    (frame->a (rep iteration env fr))
                )
            )
        )
        :induct (dec-induct iteration)
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53 rep-lemma-54 rep-lemma-55 rep-lemma-56 sublist-6
                  rep-lemma-57 rep-lemma-58 rep-lemma-59 rep-lemma-60 rep-lemma-61
                  rep-lemma-62 rep-lemma-63 rep-lemma-64 rep-lemma-65 rep-lemma-66
                  rep-lemma-67 rep-lemma-68 rep-lemma-69 rep-lemma-70 rep-lemma-71
                  rep-lemma-72 rep-lemma-73 rep-lemma-74 rep-lemma-75 rep-lemma-76
                  rep-lemma-77)
        :hints (("Subgoal *1/2"
                 :use (rep-lemma-77)
                 :do-not-induct t)
                ("Subgoal *1/1"
                 :expand ((rep 0 env fr))
                 :do-not-induct t))
    )
    (defrule rep-lemma-79
        (implies
            (and
                (natp iteration)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (frame->loop-break (rep iteration env fr))
                (<
                    (envir->k env)
                    (nth
                        (+
                            (frame->j (rep (- iteration 1) env fr))
                            2
                        )
                        (frame->a (rep (- iteration 1) env fr))
                    )
                )
            )
            (<
                (envir->k env)
                (nth
                    (+
                        (frame->j (rep iteration env fr))
                        2
                    )
                    (frame->a (rep iteration env fr))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53 rep-lemma-54 rep-lemma-55 rep-lemma-56 sublist-6
                  rep-lemma-57 rep-lemma-58 rep-lemma-59 rep-lemma-60 rep-lemma-61
                  rep-lemma-62 rep-lemma-63 rep-lemma-64 rep-lemma-65 rep-lemma-66
                  rep-lemma-67 rep-lemma-68 rep-lemma-69 rep-lemma-70 rep-lemma-71
                  rep-lemma-72 rep-lemma-73 rep-lemma-74 rep-lemma-75 rep-lemma-76
                  rep-lemma-77 rep-lemma-78)
        :hints (("Goal"
                 :expand (rep iteration env fr)
                 :use (rep-lemma-8)))
    )
    (defrule rep-lemma-80
        (implies
            (and
                (natp iteration)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (not (frame->loop-break (rep iteration env fr)))
                (<= 0 (- iteration 1))
            )
            (<
                (envir->k env)
                (nth
                    (+ (- (envir->upper-bound env) iteration) 1)
                    (frame->a (rep iteration env fr))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53 rep-lemma-54 rep-lemma-55 rep-lemma-56 sublist-6
                  rep-lemma-57 rep-lemma-58 rep-lemma-59 rep-lemma-60 rep-lemma-61
                  rep-lemma-62 rep-lemma-63 rep-lemma-64 rep-lemma-65 rep-lemma-66
                  rep-lemma-67 rep-lemma-68 rep-lemma-69 rep-lemma-70 rep-lemma-71
                  rep-lemma-72 rep-lemma-73 rep-lemma-74 rep-lemma-75 rep-lemma-76
                  rep-lemma-77 rep-lemma-78 rep-lemma-79)
        :hints (("Goal"
                 :use
                 (
                     rep-lemma-19
                 )
                 :expand
                 (
                     (rep iteration env fr)
                     (rep 1 env fr)
                     (rep 0 env fr)
                 )
                 ))
    )
    (defrule rep-lemma-81
        (implies
            (and
                (natp iteration)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (not (frame->loop-break (rep iteration env fr)))
                (<=
                    (+
                        (frame->j (rep iteration env fr))
                        2
                    )
                    (envir->upper-bound env)
                )
            )
            (<
                (envir->k env)
                (nth
                    (+ (- (envir->upper-bound env) iteration) 1)
                    (frame->a (rep iteration env fr))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53 rep-lemma-54 rep-lemma-55 rep-lemma-56 sublist-6
                  rep-lemma-57 rep-lemma-58 rep-lemma-59 rep-lemma-60 rep-lemma-61
                  rep-lemma-62 rep-lemma-63 rep-lemma-64 rep-lemma-65 rep-lemma-66
                  rep-lemma-67 rep-lemma-68 rep-lemma-69 rep-lemma-70 rep-lemma-71
                  rep-lemma-72 rep-lemma-73 rep-lemma-74 rep-lemma-75 rep-lemma-76
                  rep-lemma-77 rep-lemma-78 rep-lemma-79 rep-lemma-80)
        :use (rep-lemma-9 rep-lemma-80)
    )
    (defrule rep-lemma-82
        (implies
            (and
                (natp iteration)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (not (frame->loop-break (rep iteration env fr)))
                (<=
                    (+
                        (frame->j (rep iteration env fr))
                        2
                    )
                    (envir->upper-bound env)
                )
            )
            (<
                (envir->k env)
                (nth
                    (+
                        (frame->j (rep iteration env fr))
                        2
                    )
                    (frame->a (rep iteration env fr))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53 rep-lemma-54 rep-lemma-55 rep-lemma-56 sublist-6
                  rep-lemma-57 rep-lemma-58 rep-lemma-59 rep-lemma-60 rep-lemma-61
                  rep-lemma-62 rep-lemma-63 rep-lemma-64 rep-lemma-65 rep-lemma-66
                  rep-lemma-67 rep-lemma-68 rep-lemma-69 rep-lemma-70 rep-lemma-71
                  rep-lemma-72 rep-lemma-73 rep-lemma-74 rep-lemma-75 rep-lemma-76
                  rep-lemma-77 rep-lemma-78 rep-lemma-79 rep-lemma-80 rep-lemma-81)
        :use (rep-lemma-9 rep-lemma-81)
    )
    (defrule rep-lemma-83
        (implies
            (and
                (natp iteration)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (<=
                    (+
                        (frame->j (rep iteration env fr))
                        2
                    )
                    (envir->upper-bound env)
                )
            )
            (<
                (envir->k env)
                (nth
                    (+
                        (frame->j (rep iteration env fr))
                        2
                    )
                    (frame->a (rep iteration env fr))
                )
            )
        )
        :induct (dec-induct iteration)
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53 rep-lemma-54 rep-lemma-55 rep-lemma-56 sublist-6
                  rep-lemma-57 rep-lemma-58 rep-lemma-59 rep-lemma-60 rep-lemma-61
                  rep-lemma-62 rep-lemma-63 rep-lemma-64 rep-lemma-65 rep-lemma-66
                  rep-lemma-67 rep-lemma-68 rep-lemma-69 rep-lemma-70 rep-lemma-71
                  rep-lemma-72 rep-lemma-73 rep-lemma-74 rep-lemma-75 rep-lemma-76
                  rep-lemma-77 rep-lemma-78 rep-lemma-79 rep-lemma-80 rep-lemma-81
                  rep-lemma-82)
        :hints (("Subgoal *1/2"
                 :cases ((not (frame->loop-break (rep iteration env fr)))))
                 ("Subgoal *1/2.2"
                 :use (rep-lemma-8)
                 :expand (rep iteration env fr)
                 :do-not-induct t)
                 ("Subgoal *1/2.1"
                 :use rep-lemma-82
                 :do-not-induct t)
                 ("Subgoal *1/1"
                 :use (rep-lemma-9)
                 :expand (rep 0 env fr)
                 :do-not-induct t))
    )
    (defrule rep-lemma-84
        (implies
            (and
                (natp iteration)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (not (frame->loop-break (rep iteration env fr)))
                (<= 0 (- iteration 1))
            )
            (<
                (envir->k env)
                (nth
                    (- (envir->upper-bound env) iteration)
                    (frame->a (rep iteration env fr))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53 rep-lemma-54 rep-lemma-55 rep-lemma-56 sublist-6
                  rep-lemma-57 rep-lemma-58 rep-lemma-59 rep-lemma-60 rep-lemma-61
                  rep-lemma-62 rep-lemma-63 rep-lemma-64 rep-lemma-65 rep-lemma-66
                  rep-lemma-67 rep-lemma-68 rep-lemma-69 rep-lemma-70 rep-lemma-71
                  rep-lemma-72 rep-lemma-73 rep-lemma-74 rep-lemma-75 rep-lemma-76
                  rep-lemma-77 rep-lemma-78 rep-lemma-79 rep-lemma-80 rep-lemma-81
                  rep-lemma-82 rep-lemma-83)
        :hints (("Goal"
                 :use
                 (
                     rep-lemma-19
                 )
                 :expand
                 (
                     (rep iteration env fr)
                     (rep 1 env fr)
                     (rep 0 env fr)
                 )
                 ))
    )
    (defrule rep-lemma-85
        (implies
            (and
                (natp iteration)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (not (frame->loop-break (rep iteration env fr)))
                (<=
                    (+
                        (frame->j (rep iteration env fr))
                        2
                    )
                    (envir->upper-bound env)
                )
            )
            (<
                (envir->k env)
                (nth
                    (- (envir->upper-bound env) iteration)
                    (frame->a (rep iteration env fr))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53 rep-lemma-54 rep-lemma-55 rep-lemma-56 sublist-6
                  rep-lemma-57 rep-lemma-58 rep-lemma-59 rep-lemma-60 rep-lemma-61
                  rep-lemma-62 rep-lemma-63 rep-lemma-64 rep-lemma-65 rep-lemma-66
                  rep-lemma-67 rep-lemma-68 rep-lemma-69 rep-lemma-70 rep-lemma-71
                  rep-lemma-72 rep-lemma-73 rep-lemma-74 rep-lemma-75 rep-lemma-76
                  rep-lemma-77 rep-lemma-78 rep-lemma-79 rep-lemma-80 rep-lemma-81
                  rep-lemma-82 rep-lemma-83 rep-lemma-84)
        :use (rep-lemma-9 rep-lemma-84)
    )
    (defrule rep-lemma-86
        (implies
            (and
                (natp iteration)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (not (frame->loop-break (rep iteration env fr)))
                (<=
                    (+
                        (frame->j (rep iteration env fr))
                        2
                    )
                    (envir->upper-bound env)
                )
            )
            (<
                (envir->k env)
                (nth
                    (+
                        (frame->j (rep iteration env fr))
                        1
                    )
                    (frame->a (rep iteration env fr))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53 rep-lemma-54 rep-lemma-55 rep-lemma-56 sublist-6
                  rep-lemma-57 rep-lemma-58 rep-lemma-59 rep-lemma-60 rep-lemma-61
                  rep-lemma-62 rep-lemma-63 rep-lemma-64 rep-lemma-65 rep-lemma-66
                  rep-lemma-67 rep-lemma-68 rep-lemma-69 rep-lemma-70 rep-lemma-71
                  rep-lemma-72 rep-lemma-73 rep-lemma-74 rep-lemma-75 rep-lemma-76
                  rep-lemma-77 rep-lemma-78 rep-lemma-79 rep-lemma-80 rep-lemma-81
                  rep-lemma-82 rep-lemma-83 rep-lemma-84 rep-lemma-85)
        :use (rep-lemma-9 rep-lemma-85)
    )
    (defrule rep-lemma-87
        (implies
            (and
                (natp iteration)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (<=
                    (+
                        (frame->j (rep iteration env fr))
                        2
                    )
                    (envir->upper-bound env)
                )
            )
            (<
                (envir->k env)
                (nth
                    (+
                        (frame->j (rep iteration env fr))
                        1
                    )
                    (frame->a (rep iteration env fr))
                )
            )
        )
        :induct (dec-induct iteration)
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53 rep-lemma-54 rep-lemma-55 rep-lemma-56 sublist-6
                  rep-lemma-57 rep-lemma-58 rep-lemma-59 rep-lemma-60 rep-lemma-61
                  rep-lemma-62 rep-lemma-63 rep-lemma-64 rep-lemma-65 rep-lemma-66
                  rep-lemma-67 rep-lemma-68 rep-lemma-69 rep-lemma-70 rep-lemma-71
                  rep-lemma-72 rep-lemma-73 rep-lemma-74 rep-lemma-75 rep-lemma-76
                  rep-lemma-77 rep-lemma-78 rep-lemma-79 rep-lemma-80 rep-lemma-81
                  rep-lemma-82 rep-lemma-83 rep-lemma-84 rep-lemma-85 rep-lemma-86)
        :hints (("Subgoal *1/2"
                 :cases ((not (frame->loop-break (rep iteration env fr)))))
                 ("Subgoal *1/2.2"
                 :use (rep-lemma-8)
                 :expand (rep iteration env fr)
                 :do-not-induct t)
                 ("Subgoal *1/2.1"
                 :use rep-lemma-86
                 :do-not-induct t)
                 ("Subgoal *1/1"
                 :use (rep-lemma-9)
                 :expand (rep 0 env fr)
                 :do-not-induct t))
    )
    (defrule rep-lemma-88
        (implies
            (and
                (natp iteration)
                (natp iter)
                (<= iter iteration)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (frame->loop-break (rep iter env fr))
            )
            (frame->loop-break (rep iteration env fr))
        )
        :induct (dec-induct iteration)
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53 rep-lemma-54 rep-lemma-55 rep-lemma-56 sublist-6
                  rep-lemma-57 rep-lemma-58 rep-lemma-59 rep-lemma-60 rep-lemma-61
                  rep-lemma-62 rep-lemma-63 rep-lemma-64 rep-lemma-65 rep-lemma-66
                  rep-lemma-67 rep-lemma-68 rep-lemma-69 rep-lemma-70 rep-lemma-71
                  rep-lemma-72 rep-lemma-73 rep-lemma-74 rep-lemma-75 rep-lemma-76
                  rep-lemma-77 rep-lemma-78 rep-lemma-79 rep-lemma-80 rep-lemma-81
                  rep-lemma-82 rep-lemma-83 rep-lemma-84 rep-lemma-85 rep-lemma-86
                  rep-lemma-87)
        :hints (("Subgoal *1/2"
                 :cases ((<= iter (- iteration 1))))
                 ("Subgoal *1/2.2"
                 :do-not-induct t)
                 ("Subgoal *1/2.1"
                 :expand (rep iteration env fr)
                 :do-not-induct t)
                 ("Subgoal *1/1"
                 :do-not-induct t))
    )
    (defrule rep-lemma-89
        (implies
            (and
                (natp iteration)
                (natp iter)
                (<= iter iteration)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (frame->loop-break (rep iter env fr))
            )
            (equal
                (frame->j (rep iter env fr))
                (frame->j (rep iteration env fr))
            )
        )
        :induct (dec-induct iteration)
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53 rep-lemma-54 rep-lemma-55 rep-lemma-56 sublist-6
                  rep-lemma-57 rep-lemma-58 rep-lemma-59 rep-lemma-60 rep-lemma-61
                  rep-lemma-62 rep-lemma-63 rep-lemma-64 rep-lemma-65 rep-lemma-66
                  rep-lemma-67 rep-lemma-68 rep-lemma-69 rep-lemma-70 rep-lemma-71
                  rep-lemma-72 rep-lemma-73 rep-lemma-74 rep-lemma-75 rep-lemma-76
                  rep-lemma-77 rep-lemma-78 rep-lemma-79 rep-lemma-80 rep-lemma-81
                  rep-lemma-82 rep-lemma-83 rep-lemma-84 rep-lemma-85 rep-lemma-86
                  rep-lemma-87 rep-lemma-88)
        :hints (("Subgoal *1/2"
                 :cases ((<= iter (- iteration 1))))
                 ("Subgoal *1/2.2"
                 :do-not-induct t)
                 ("Subgoal *1/2.1"
                 :use (rep-lemma-8 rep-lemma-88)
                 :do-not-induct t)
                 ("Subgoal *1/1"
                 :do-not-induct t))
    )
    (defrule rep-lemma-90
        (implies
            (and
                (natp iteration)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (<=
                    (+
                        (frame->j (rep iteration env fr))
                        2
                    )
                    (envir->upper-bound env)
                )
                (<
                    iteration
                    (-
                        (envir->upper-bound env)
                        (frame->j (rep iteration env fr))
                    )
                )
            )
            (not (frame->loop-break (rep iteration env fr)))
        )
        :induct (dec-induct iteration)
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53 rep-lemma-54 rep-lemma-55 rep-lemma-56 sublist-6
                  rep-lemma-57 rep-lemma-58 rep-lemma-59 rep-lemma-60 rep-lemma-61
                  rep-lemma-62 rep-lemma-63 rep-lemma-64 rep-lemma-65 rep-lemma-66
                  rep-lemma-67 rep-lemma-68 rep-lemma-69 rep-lemma-70 rep-lemma-71
                  rep-lemma-72 rep-lemma-73 rep-lemma-74 rep-lemma-75 rep-lemma-76
                  rep-lemma-77 rep-lemma-78 rep-lemma-79 rep-lemma-80 rep-lemma-81
                  rep-lemma-82 rep-lemma-83 rep-lemma-84 rep-lemma-85 rep-lemma-86
                  rep-lemma-87 rep-lemma-88 rep-lemma-89)
        :hints (("Subgoal *1/2"
                 :use
                 (rep-lemma-8 rep-lemma-9
                  (:instance rep-lemma-89
                    (iter iteration)
                    (iteration
                        (-
                            (envir->upper-bound env)
                            (frame->j (rep iteration env fr))
                        )
                    )
                  )
                  (:instance rep-lemma-9
                    (iteration (- iteration 1))
                  )
                 )
                 :cases ((not (frame->loop-break (rep iteration env fr))))
                 :do-not-induct t)
                 ("Subgoal *1/1"
                 :use (rep-lemma-9)
                 :expand (rep 0 env fr)
                 :do-not-induct t))
    )
    (defrule rep-lemma-91
        (implies
            (and
                (natp iteration)
                (< 0 iteration)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (frame->loop-break (rep iteration env fr))
            )
            (<=
                (frame->j (rep iteration env fr))
                (frame->j (rep (- iteration 1) env fr))
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53 rep-lemma-54 rep-lemma-55 rep-lemma-56 sublist-6
                  rep-lemma-57 rep-lemma-58 rep-lemma-59 rep-lemma-60 rep-lemma-61
                  rep-lemma-62 rep-lemma-63 rep-lemma-64 rep-lemma-65 rep-lemma-66
                  rep-lemma-67 rep-lemma-68 rep-lemma-69 rep-lemma-70 rep-lemma-71
                  rep-lemma-72 rep-lemma-73 rep-lemma-74 rep-lemma-75 rep-lemma-76
                  rep-lemma-77 rep-lemma-78 rep-lemma-79 rep-lemma-80 rep-lemma-81
                  rep-lemma-82 rep-lemma-83 rep-lemma-84 rep-lemma-85 rep-lemma-86
                  rep-lemma-87 rep-lemma-88 rep-lemma-89 rep-lemma-90)
        :use (rep-lemma-8)
    )
    (defrule rep-lemma-92
        (implies
            (and
                (natp iteration)
                (< 0 iteration)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (not (frame->loop-break (rep iteration env fr)))
            )
            (<=
                (frame->j (rep iteration env fr))
                (frame->j (rep (- iteration 1) env fr))
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53 rep-lemma-54 rep-lemma-55 rep-lemma-56 sublist-6
                  rep-lemma-57 rep-lemma-58 rep-lemma-59 rep-lemma-60 rep-lemma-61
                  rep-lemma-62 rep-lemma-63 rep-lemma-64 rep-lemma-65 rep-lemma-66
                  rep-lemma-67 rep-lemma-68 rep-lemma-69 rep-lemma-70 rep-lemma-71
                  rep-lemma-72 rep-lemma-73 rep-lemma-74 rep-lemma-75 rep-lemma-76
                  rep-lemma-77 rep-lemma-78 rep-lemma-79 rep-lemma-80 rep-lemma-81
                  rep-lemma-82 rep-lemma-83 rep-lemma-84 rep-lemma-85 rep-lemma-86
                  rep-lemma-87 rep-lemma-88 rep-lemma-89 rep-lemma-90 rep-lemma-91)
        :use (rep-lemma-9 rep-lemma-29 rep-lemma-30)
    )
    (defrule rep-lemma-93
        (implies
            (and
                (natp iteration)
                (< 0 iteration)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
            )
            (<=
                (frame->j (rep iteration env fr))
                (frame->j (rep (- iteration 1) env fr))
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53 rep-lemma-54 rep-lemma-55 rep-lemma-56 sublist-6
                  rep-lemma-57 rep-lemma-58 rep-lemma-59 rep-lemma-60 rep-lemma-61
                  rep-lemma-62 rep-lemma-63 rep-lemma-64 rep-lemma-65 rep-lemma-66
                  rep-lemma-67 rep-lemma-68 rep-lemma-69 rep-lemma-70 rep-lemma-71
                  rep-lemma-72 rep-lemma-73 rep-lemma-74 rep-lemma-75 rep-lemma-76
                  rep-lemma-77 rep-lemma-78 rep-lemma-79 rep-lemma-80 rep-lemma-81
                  rep-lemma-82 rep-lemma-83 rep-lemma-84 rep-lemma-85 rep-lemma-86
                  rep-lemma-87 rep-lemma-88 rep-lemma-89 rep-lemma-90 rep-lemma-91
                  rep-lemma-92)
        :hints (("Goal"
                 :cases ((not (frame->loop-break (rep iteration env fr)))))
                 ("Subgoal 2"
                 :use (rep-lemma-91)
                 :do-not-induct t)
                 ("Subgoal 1"
                 :use (rep-lemma-92)
                 :do-not-induct t))
    )
    (defrule rep-lemma-94
        (implies
            (and
                (natp iteration)
                (natp iter)
                (<= iter iteration)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
            )
            (<=
                (frame->j (rep iteration env fr))
                (frame->j (rep iter env fr))
            )
        )
        :induct (dec-induct iteration)
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53 rep-lemma-54 rep-lemma-55 rep-lemma-56 sublist-6
                  rep-lemma-57 rep-lemma-58 rep-lemma-59 rep-lemma-60 rep-lemma-61
                  rep-lemma-62 rep-lemma-63 rep-lemma-64 rep-lemma-65 rep-lemma-66
                  rep-lemma-67 rep-lemma-68 rep-lemma-69 rep-lemma-70 rep-lemma-71
                  rep-lemma-72 rep-lemma-73 rep-lemma-74 rep-lemma-75 rep-lemma-76
                  rep-lemma-77 rep-lemma-78 rep-lemma-79 rep-lemma-80 rep-lemma-81
                  rep-lemma-82 rep-lemma-83 rep-lemma-84 rep-lemma-85 rep-lemma-86
                  rep-lemma-87 rep-lemma-88 rep-lemma-89 rep-lemma-90 rep-lemma-91
                  rep-lemma-92 rep-lemma-93)
        :hints (("Subgoal *1/2"
                 :cases ((<= iter (- iteration 1))))
                 ("Subgoal *1/2.2"
                 :use (rep-lemma-93)
                 :do-not-induct t)
                 ("Subgoal *1/2.1"
                 :use (rep-lemma-93)
                 :do-not-induct t)
                 ("Subgoal *1/1"
                 :do-not-induct t))
    )
    (defrule rep-lemma-95
        (implies
            (and
                (natp iteration)
                (natp iter)
                (<= iter iteration)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (not (frame->loop-break (rep iteration env fr)))
            )
            (not (frame->loop-break (rep iter env fr)))
        )
        :induct (dec-induct iteration)
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53 rep-lemma-54 rep-lemma-55 rep-lemma-56 sublist-6
                  rep-lemma-57 rep-lemma-58 rep-lemma-59 rep-lemma-60 rep-lemma-61
                  rep-lemma-62 rep-lemma-63 rep-lemma-64 rep-lemma-65 rep-lemma-66
                  rep-lemma-67 rep-lemma-68 rep-lemma-69 rep-lemma-70 rep-lemma-71
                  rep-lemma-72 rep-lemma-73 rep-lemma-74 rep-lemma-75 rep-lemma-76
                  rep-lemma-77 rep-lemma-78 rep-lemma-79 rep-lemma-80 rep-lemma-81
                  rep-lemma-82 rep-lemma-83 rep-lemma-84 rep-lemma-85 rep-lemma-86
                  rep-lemma-87 rep-lemma-88 rep-lemma-89 rep-lemma-90 rep-lemma-91
                  rep-lemma-92 rep-lemma-93 rep-lemma-94)
        :hints (("Subgoal *1/2"
                 :cases ((<= iter (- iteration 1))))
                 ("Subgoal *1/2.2"
                 :do-not-induct t)
                 ("Subgoal *1/2.1"
                 :use (rep-lemma-29)
                 :do-not-induct t)
                 ("Subgoal *1/1"
                 :do-not-induct t))
    )
    (defrule rep-lemma-96
        (implies
            (and
                (natp (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (not (frame->loop-break (rep (envir->upper-bound env) env fr)))
            )
            (equal
                (frame->j (rep (envir->upper-bound env) env fr))
                -1
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53 rep-lemma-54 rep-lemma-55 rep-lemma-56 sublist-6
                  rep-lemma-57 rep-lemma-58 rep-lemma-59 rep-lemma-60 rep-lemma-61
                  rep-lemma-62 rep-lemma-63 rep-lemma-64 rep-lemma-65 rep-lemma-66
                  rep-lemma-67 rep-lemma-68 rep-lemma-69 rep-lemma-70 rep-lemma-71
                  rep-lemma-72 rep-lemma-73 rep-lemma-74 rep-lemma-75 rep-lemma-76
                  rep-lemma-77 rep-lemma-78 rep-lemma-79 rep-lemma-80 rep-lemma-81
                  rep-lemma-82 rep-lemma-83 rep-lemma-84 rep-lemma-85 rep-lemma-86
                  rep-lemma-87 rep-lemma-88 rep-lemma-89 rep-lemma-90 rep-lemma-91
                  rep-lemma-92 rep-lemma-93 rep-lemma-94 rep-lemma-95)
        :use ((:instance rep-lemma-9
               (iteration (envir->upper-bound env))
               ))
    )
   (defrule rep-lemma-97
        (implies
            (and
                (natp (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (natp (frame->j (rep (envir->upper-bound env) env fr)))
            )
            (frame->loop-break (rep (envir->upper-bound env) env fr))
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53 rep-lemma-54 rep-lemma-55 rep-lemma-56 sublist-6
                  rep-lemma-57 rep-lemma-58 rep-lemma-59 rep-lemma-60 rep-lemma-61
                  rep-lemma-62 rep-lemma-63 rep-lemma-64 rep-lemma-65 rep-lemma-66
                  rep-lemma-67 rep-lemma-68 rep-lemma-69 rep-lemma-70 rep-lemma-71
                  rep-lemma-72 rep-lemma-73 rep-lemma-74 rep-lemma-75 rep-lemma-76
                  rep-lemma-77 rep-lemma-78 rep-lemma-79 rep-lemma-80 rep-lemma-81
                  rep-lemma-82 rep-lemma-83 rep-lemma-84 rep-lemma-85 rep-lemma-86
                  rep-lemma-87 rep-lemma-88 rep-lemma-89 rep-lemma-90 rep-lemma-91
                  rep-lemma-92 rep-lemma-93 rep-lemma-94 rep-lemma-95 rep-lemma-96)
        :hints (("Goal"
                 :use (rep-lemma-96)
                 :cases ((not (frame->loop-break (rep (envir->upper-bound env) env fr))))))
    )
    (defrule rep-lemma-98
        (implies
            (and
                (natp iteration)
                (natp (envir->upper-bound env))
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (not (frame->loop-break fr))
                (<
                    iteration
                    (-
                        (envir->upper-bound env)
                        (frame->j (rep (envir->upper-bound env) env fr))
                    )
                )
            )
            (not (frame->loop-break (rep iteration env fr)))
        )
        :induct (dec-induct iteration)
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53 rep-lemma-54 rep-lemma-55 rep-lemma-56 sublist-6
                  rep-lemma-57 rep-lemma-58 rep-lemma-59 rep-lemma-60 rep-lemma-61
                  rep-lemma-62 rep-lemma-63 rep-lemma-64 rep-lemma-65 rep-lemma-66
                  rep-lemma-67 rep-lemma-68 rep-lemma-69 rep-lemma-70 rep-lemma-71
                  rep-lemma-72 rep-lemma-73 rep-lemma-74 rep-lemma-75 rep-lemma-76
                  rep-lemma-77 rep-lemma-78 rep-lemma-79 rep-lemma-80 rep-lemma-81
                  rep-lemma-82 rep-lemma-83 rep-lemma-84 rep-lemma-85 rep-lemma-86
                  rep-lemma-87 rep-lemma-88 rep-lemma-89 rep-lemma-90 rep-lemma-91
                  rep-lemma-92 rep-lemma-93 rep-lemma-94 rep-lemma-95 rep-lemma-96
                  rep-lemma-97)
        :hints (("Subgoal *1/2"
                 :use
                 (rep-lemma-8
                  (:instance rep-lemma-6
                    (iteration
                        (envir->upper-bound env)
                    )
                  )
                  (:instance rep-lemma-89
                    (iter iteration)
                    (iteration
                        (envir->upper-bound env)
                    )
                  )
                  (:instance rep-lemma-9
                    (iteration (- iteration 1))
                  )
                 )
                 :cases ((not (frame->loop-break (rep iteration env fr))))
                 :do-not-induct t)
                 ("Subgoal *1/1"
                 :expand (rep 0 env fr)
                 :do-not-induct t))
    )
    (defrule rep-lemma-99
        (implies
            (and
                (natp (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (not (frame->loop-break fr))
            )
            (not
                (frame->loop-break
                    (rep
                        (-
                            (-
                                (envir->upper-bound env)
                                (frame->j (rep (envir->upper-bound env) env fr))
                            )
                            1
                        )
                        env
                        fr
                    )
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53 rep-lemma-54 rep-lemma-55 rep-lemma-56 sublist-6
                  rep-lemma-57 rep-lemma-58 rep-lemma-59 rep-lemma-60 rep-lemma-61
                  rep-lemma-62 rep-lemma-63 rep-lemma-64 rep-lemma-65 rep-lemma-66
                  rep-lemma-67 rep-lemma-68 rep-lemma-69 rep-lemma-70 rep-lemma-71
                  rep-lemma-72 rep-lemma-73 rep-lemma-74 rep-lemma-75 rep-lemma-76
                  rep-lemma-77 rep-lemma-78 rep-lemma-79 rep-lemma-80 rep-lemma-81
                  rep-lemma-82 rep-lemma-83 rep-lemma-84 rep-lemma-85 rep-lemma-86
                  rep-lemma-87 rep-lemma-88 rep-lemma-89 rep-lemma-90 rep-lemma-91
                  rep-lemma-92 rep-lemma-93 rep-lemma-94 rep-lemma-95 rep-lemma-96
                  rep-lemma-97 rep-lemma-98)
        :use ((:instance rep-lemma-6
                    (iteration
                        (envir->upper-bound env)
                    )
                  )
                  (:instance rep-lemma-46
                    (iteration
                        (envir->upper-bound env)
                    )
                  )
                  (:instance rep-lemma-98
                    (iteration
                        (-
                            (-
                                (envir->upper-bound env)
                                (frame->j (rep (envir->upper-bound env) env fr))
                            )
                            1
                        )
                    )
                  ))
    )
    (defrule rep-lemma-100
        (implies
            (and
                (natp (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (not (frame->loop-break fr))
                (frame->loop-break
                    (rep
                        (-
                            (envir->upper-bound env)
                            (frame->j (rep (envir->upper-bound env) env fr))
                        )
                        env
                        fr
                    )
                )
            )
            (<=
                (nth
                    (frame->j
                        (rep
                            (-
                                (envir->upper-bound env)
                                (frame->j (rep (envir->upper-bound env) env fr))
                            )
                            env
                            fr
                        )
                    )
                    (frame->a
                        (rep
                            (-
                                (-
                                    (envir->upper-bound env)
                                    (frame->j (rep (envir->upper-bound env) env fr))
                                )
                                1
                            )
                            env
                            fr
                        )
                    )
                )
                (envir->k env)
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53 rep-lemma-54 rep-lemma-55 rep-lemma-56 sublist-6
                  rep-lemma-57 rep-lemma-58 rep-lemma-59 rep-lemma-60 rep-lemma-61
                  rep-lemma-62 rep-lemma-63 rep-lemma-64 rep-lemma-65 rep-lemma-66
                  rep-lemma-67 rep-lemma-68 rep-lemma-69 rep-lemma-70 rep-lemma-71
                  rep-lemma-72 rep-lemma-73 rep-lemma-74 rep-lemma-75 rep-lemma-76
                  rep-lemma-77 rep-lemma-78 rep-lemma-79 rep-lemma-80 rep-lemma-81
                  rep-lemma-82 rep-lemma-83 rep-lemma-84 rep-lemma-85 rep-lemma-86
                  rep-lemma-87 rep-lemma-88 rep-lemma-89 rep-lemma-90 rep-lemma-91
                  rep-lemma-92 rep-lemma-93 rep-lemma-94 rep-lemma-95 rep-lemma-96
                  rep-lemma-97 rep-lemma-98 rep-lemma-99)
        :hints (("Goal" :expand
                 (
                    (rep
                        (-
                            (envir->upper-bound env)
                            (frame->j (rep (envir->upper-bound env) env fr))
                        )
                        env
                        fr
                    )
                 )
                :use
                 (rep-lemma-99
                  (:instance rep-lemma-6
                    (iteration
                        (envir->upper-bound env)
                    )
                  )
                  (:instance rep-lemma-46
                    (iteration
                        (envir->upper-bound env)
                    )
                  )
                  (:instance rep-lemma-9
                        (iteration
                            (-
                                (-
                                    (envir->upper-bound env)
                                    (frame->j (rep (envir->upper-bound env) env fr))
                                )
                                1
                            )
                        )
                    )
                )
                ))
    )
    (defrule rep-lemma-101
        (implies
            (and
                (natp (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (not (frame->loop-break fr))
                (natp (frame->j (rep (envir->upper-bound env) env fr)))
                (frame->loop-break
                    (rep
                        (-
                            (envir->upper-bound env)
                            (frame->j (rep (envir->upper-bound env) env fr))
                        )
                        env
                        fr
                    )
                )
            )
            (<=
                (nth
                    (frame->j
                        (rep
                            (-
                                (envir->upper-bound env)
                                (frame->j (rep (envir->upper-bound env) env fr))
                            )
                            env
                            fr
                        )
                    )
                    (frame->a
                        (rep
                            (-
                                (envir->upper-bound env)
                                (frame->j (rep (envir->upper-bound env) env fr))
                            )
                            env
                            fr
                        )
                    )
                )
                (envir->k env)
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53 rep-lemma-54 rep-lemma-55 rep-lemma-56 sublist-6
                  rep-lemma-57 rep-lemma-58 rep-lemma-59 rep-lemma-60 rep-lemma-61
                  rep-lemma-62 rep-lemma-63 rep-lemma-64 rep-lemma-65 rep-lemma-66
                  rep-lemma-67 rep-lemma-68 rep-lemma-69 rep-lemma-70 rep-lemma-71
                  rep-lemma-72 rep-lemma-73 rep-lemma-74 rep-lemma-75 rep-lemma-76
                  rep-lemma-77 rep-lemma-78 rep-lemma-79 rep-lemma-80 rep-lemma-81
                  rep-lemma-82 rep-lemma-83 rep-lemma-84 rep-lemma-85 rep-lemma-86
                  rep-lemma-87 rep-lemma-88 rep-lemma-89 rep-lemma-90 rep-lemma-91
                  rep-lemma-92 rep-lemma-93 rep-lemma-94 rep-lemma-95 rep-lemma-96
                  rep-lemma-97 rep-lemma-98 rep-lemma-99 rep-lemma-100)
        :use (rep-lemma-99 rep-lemma-100
              (:instance rep-lemma-22
               (iteration
                            (-
                                (envir->upper-bound env)
                                (frame->j (rep (envir->upper-bound env) env fr))
                            )
               )
               (index
                    (frame->j
                        (rep
                            (-
                                (envir->upper-bound env)
                                (frame->j (rep (envir->upper-bound env) env fr))
                            )
                            env
                            fr
                        )
                    )
               ))
                  (:instance rep-lemma-6
                    (iteration
                        (envir->upper-bound env)
                    )
                  )
                  (:instance rep-lemma-8
                    (iteration
                            (-
                                (envir->upper-bound env)
                                (frame->j (rep (envir->upper-bound env) env fr))
                            )
                    )
                  )
                  (:instance rep-lemma-46
                    (iteration
                        (envir->upper-bound env)
                    )
                  )
                  (:instance rep-lemma-9
                        (iteration
                            (-
                                (-
                                    (envir->upper-bound env)
                                    (frame->j (rep (envir->upper-bound env) env fr))
                                )
                                1
                            )
                        )
                    )
                )
               )
    )
    (defrule rep-lemma-102
        (implies
            (and
                (natp (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (not (frame->loop-break fr))
                (natp (frame->j (rep (envir->upper-bound env) env fr)))
            )
            (frame->loop-break
                (rep
                    (-
                        (envir->upper-bound env)
                        (frame->j (rep (envir->upper-bound env) env fr))
                    )
                    env
                    fr
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53 rep-lemma-54 rep-lemma-55 rep-lemma-56 sublist-6
                  rep-lemma-57 rep-lemma-58 rep-lemma-59 rep-lemma-60 rep-lemma-61
                  rep-lemma-62 rep-lemma-63 rep-lemma-64 rep-lemma-65 rep-lemma-66
                  rep-lemma-67 rep-lemma-68 rep-lemma-69 rep-lemma-70 rep-lemma-71
                  rep-lemma-72 rep-lemma-73 rep-lemma-74 rep-lemma-75 rep-lemma-76
                  rep-lemma-77 rep-lemma-78 rep-lemma-79 rep-lemma-80 rep-lemma-81
                  rep-lemma-82 rep-lemma-83 rep-lemma-84 rep-lemma-85 rep-lemma-86
                  rep-lemma-87 rep-lemma-88 rep-lemma-89 rep-lemma-90 rep-lemma-91
                  rep-lemma-92 rep-lemma-93 rep-lemma-94 rep-lemma-95 rep-lemma-96
                  rep-lemma-97 rep-lemma-98 rep-lemma-99 rep-lemma-100 rep-lemma-101)
        :hints (("Goal"
                 :use
                 ((:instance rep-lemma-6
                    (iteration
                        (envir->upper-bound env)
                    )
                  )
                  (:instance rep-lemma-46
                    (iteration
                        (envir->upper-bound env)
                    )
                  )
                  (:instance rep-lemma-9
                    (iteration
                        (-
                            (envir->upper-bound env)
                            (frame->j (rep (envir->upper-bound env) env fr))
                        )
                    )
                  )
                  (:instance rep-lemma-94
                    (iter
                        (-
                            (envir->upper-bound env)
                            (frame->j (rep (envir->upper-bound env) env fr))
                        )
                    )
                    (iteration
                        (envir->upper-bound env)
                    )
                  )
                 )
                 :cases
                 (
                    (frame->loop-break
                        (rep
                            (-
                                (envir->upper-bound env)
                                (frame->j (rep (envir->upper-bound env) env fr))
                            )
                            env
                            fr
                        )
                    )
                 )))
    )
    (defrule rep-lemma-103
        (implies
            (and
                (natp (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (not (frame->loop-break fr))
                (natp (frame->j (rep (envir->upper-bound env) env fr)))
            )
            (<=
                (nth
                    (frame->j
                        (rep
                            (-
                                (envir->upper-bound env)
                                (frame->j (rep (envir->upper-bound env) env fr))
                            )
                            env
                            fr
                        )
                    )
                    (frame->a
                        (rep
                            (-
                                (envir->upper-bound env)
                                (frame->j (rep (envir->upper-bound env) env fr))
                            )
                            env
                            fr
                        )
                    )
                )
                (envir->k env)
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53 rep-lemma-54 rep-lemma-55 rep-lemma-56 sublist-6
                  rep-lemma-57 rep-lemma-58 rep-lemma-59 rep-lemma-60 rep-lemma-61
                  rep-lemma-62 rep-lemma-63 rep-lemma-64 rep-lemma-65 rep-lemma-66
                  rep-lemma-67 rep-lemma-68 rep-lemma-69 rep-lemma-70 rep-lemma-71
                  rep-lemma-72 rep-lemma-73 rep-lemma-74 rep-lemma-75 rep-lemma-76
                  rep-lemma-77 rep-lemma-78 rep-lemma-79 rep-lemma-80 rep-lemma-81
                  rep-lemma-82 rep-lemma-83 rep-lemma-84 rep-lemma-85 rep-lemma-86
                  rep-lemma-87 rep-lemma-88 rep-lemma-89 rep-lemma-90 rep-lemma-91
                  rep-lemma-92 rep-lemma-93 rep-lemma-94 rep-lemma-95 rep-lemma-96
                  rep-lemma-97 rep-lemma-98 rep-lemma-99 rep-lemma-100 rep-lemma-101
                  rep-lemma-102)
        :use (rep-lemma-101 rep-lemma-102)
    )
   (defrule rep-lemma-104
        (implies
            (and
                (natp (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (not (frame->loop-break fr))
                (natp (frame->j (rep (envir->upper-bound env) env fr)))
            )
            (equal
                (frame->j
                    (rep
                        (-
                            (envir->upper-bound env)
                            (frame->j (rep (envir->upper-bound env) env fr))
                        )
                        env
                        fr
                    )
                )
                (frame->j
                    (rep
                        (envir->upper-bound env)
                        env
                        fr
                    )
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53 rep-lemma-54 rep-lemma-55 rep-lemma-56 sublist-6
                  rep-lemma-57 rep-lemma-58 rep-lemma-59 rep-lemma-60 rep-lemma-61
                  rep-lemma-62 rep-lemma-63 rep-lemma-64 rep-lemma-65 rep-lemma-66
                  rep-lemma-67 rep-lemma-68 rep-lemma-69 rep-lemma-70 rep-lemma-71
                  rep-lemma-72 rep-lemma-73 rep-lemma-74 rep-lemma-75 rep-lemma-76
                  rep-lemma-77 rep-lemma-78 rep-lemma-79 rep-lemma-80 rep-lemma-81
                  rep-lemma-82 rep-lemma-83 rep-lemma-84 rep-lemma-85 rep-lemma-86
                  rep-lemma-87 rep-lemma-88 rep-lemma-89 rep-lemma-90 rep-lemma-91
                  rep-lemma-92 rep-lemma-93 rep-lemma-94 rep-lemma-95 rep-lemma-96
                  rep-lemma-97 rep-lemma-98 rep-lemma-99 rep-lemma-100 rep-lemma-101
                  rep-lemma-102 rep-lemma-103)
        :use (rep-lemma-102
               (:instance rep-lemma-89
                    (iter
                        (-
                            (envir->upper-bound env)
                            (frame->j (rep (envir->upper-bound env) env fr))
                        )
                    )
                    (iteration
                        (envir->upper-bound env)
                    )
               )
               (:instance rep-lemma-6
                    (iteration
                        (envir->upper-bound env)
                    )
               )
               (:instance rep-lemma-46
                    (iteration
                        (envir->upper-bound env)
                    )
               ))
    )
   (defrule rep-lemma-105
        (implies
            (and
                (natp (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (not (frame->loop-break fr))
                (natp (frame->j (rep (envir->upper-bound env) env fr)))
            )
            (<=
                (nth
                    (frame->j
                        (rep
                            (envir->upper-bound env)
                            env
                            fr
                        )
                    )
                    (frame->a
                        (rep
                            (-
                                (envir->upper-bound env)
                                (frame->j (rep (envir->upper-bound env) env fr))
                            )
                            env
                            fr
                        )
                    )
                )
                (envir->k env)
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53 rep-lemma-54 rep-lemma-55 rep-lemma-56 sublist-6
                  rep-lemma-57 rep-lemma-58 rep-lemma-59 rep-lemma-60 rep-lemma-61
                  rep-lemma-62 rep-lemma-63 rep-lemma-64 rep-lemma-65 rep-lemma-66
                  rep-lemma-67 rep-lemma-68 rep-lemma-69 rep-lemma-70 rep-lemma-71
                  rep-lemma-72 rep-lemma-73 rep-lemma-74 rep-lemma-75 rep-lemma-76
                  rep-lemma-77 rep-lemma-78 rep-lemma-79 rep-lemma-80 rep-lemma-81
                  rep-lemma-82 rep-lemma-83 rep-lemma-84 rep-lemma-85 rep-lemma-86
                  rep-lemma-87 rep-lemma-88 rep-lemma-89 rep-lemma-90 rep-lemma-91
                  rep-lemma-92 rep-lemma-93 rep-lemma-94 rep-lemma-95 rep-lemma-96
                  rep-lemma-97 rep-lemma-98 rep-lemma-99 rep-lemma-100 rep-lemma-101
                  rep-lemma-102 rep-lemma-103 rep-lemma-104)
        :use (rep-lemma-103 rep-lemma-104)
    )
   (defrule rep-lemma-106
        (implies
            (and
                (natp iteration)
                (natp iter)
                (<= iter iteration)
                (<= iteration (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (frame->loop-break (rep iter env fr))
            )
            (equal
                (frame->a (rep iter env fr))
                (frame->a (rep iteration env fr))
            )
        )
        :induct (dec-induct iteration)
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53 rep-lemma-54 rep-lemma-55 rep-lemma-56 sublist-6
                  rep-lemma-57 rep-lemma-58 rep-lemma-59 rep-lemma-60 rep-lemma-61
                  rep-lemma-62 rep-lemma-63 rep-lemma-64 rep-lemma-65 rep-lemma-66
                  rep-lemma-67 rep-lemma-68 rep-lemma-69 rep-lemma-70 rep-lemma-71
                  rep-lemma-72 rep-lemma-73 rep-lemma-74 rep-lemma-75 rep-lemma-76
                  rep-lemma-77 rep-lemma-78 rep-lemma-79 rep-lemma-80 rep-lemma-81
                  rep-lemma-82 rep-lemma-83 rep-lemma-84 rep-lemma-85 rep-lemma-86
                  rep-lemma-87 rep-lemma-88 rep-lemma-89 rep-lemma-90 rep-lemma-91
                  rep-lemma-92 rep-lemma-93 rep-lemma-94 rep-lemma-95 rep-lemma-96
                  rep-lemma-97 rep-lemma-98 rep-lemma-99 rep-lemma-100 rep-lemma-101
                  rep-lemma-102 rep-lemma-103 rep-lemma-104 rep-lemma-105)
        :hints (("Subgoal *1/2"
                 :cases ((<= iter (- iteration 1))))
                 ("Subgoal *1/2.2"
                 :do-not-induct t)
                 ("Subgoal *1/2.1"
                 :expand (rep iteration env fr)
                 :use (rep-lemma-88)
                 :do-not-induct t)
                 ("Subgoal *1/1"
                 :do-not-induct t))
    )
    (defrule rep-lemma-107
        (implies
            (and
                (natp (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (not (frame->loop-break fr))
                (natp (frame->j (rep (envir->upper-bound env) env fr)))
            )
            (equal
                (frame->a
                    (rep
                        (-
                            (envir->upper-bound env)
                            (frame->j (rep (envir->upper-bound env) env fr))
                        )
                        env
                        fr
                    )
                )
                (frame->a
                    (rep
                        (envir->upper-bound env)
                        env
                        fr
                    )
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53 rep-lemma-54 rep-lemma-55 rep-lemma-56 sublist-6
                  rep-lemma-57 rep-lemma-58 rep-lemma-59 rep-lemma-60 rep-lemma-61
                  rep-lemma-62 rep-lemma-63 rep-lemma-64 rep-lemma-65 rep-lemma-66
                  rep-lemma-67 rep-lemma-68 rep-lemma-69 rep-lemma-70 rep-lemma-71
                  rep-lemma-72 rep-lemma-73 rep-lemma-74 rep-lemma-75 rep-lemma-76
                  rep-lemma-77 rep-lemma-78 rep-lemma-79 rep-lemma-80 rep-lemma-81
                  rep-lemma-82 rep-lemma-83 rep-lemma-84 rep-lemma-85 rep-lemma-86
                  rep-lemma-87 rep-lemma-88 rep-lemma-89 rep-lemma-90 rep-lemma-91
                  rep-lemma-92 rep-lemma-93 rep-lemma-94 rep-lemma-95 rep-lemma-96
                  rep-lemma-97 rep-lemma-98 rep-lemma-99 rep-lemma-100 rep-lemma-101
                  rep-lemma-102 rep-lemma-103 rep-lemma-104 rep-lemma-105 rep-lemma-106)
        :use (rep-lemma-102
               (:instance rep-lemma-106
                    (iter
                        (-
                            (envir->upper-bound env)
                            (frame->j (rep (envir->upper-bound env) env fr))
                        )
                    )
                    (iteration
                        (envir->upper-bound env)
                    )
               )
               (:instance rep-lemma-6
                    (iteration
                        (envir->upper-bound env)
                    )
               )
               (:instance rep-lemma-46
                    (iteration
                        (envir->upper-bound env)
                    )
               ))
    )
    (defrule rep-lemma-108
        (implies
            (and
                (natp (envir->upper-bound env))
                (< (envir->upper-bound env) (len (frame->a fr)))
                (equal (frame->j fr) (- (envir->upper-bound env) 1))
                (not (frame->loop-break fr))
                (natp (frame->j (rep (envir->upper-bound env) env fr)))
            )
            (<=
                (nth
                    (frame->j
                        (rep
                            (envir->upper-bound env)
                            env
                            fr
                        )
                    )
                    (frame->a
                        (rep
                            (envir->upper-bound env)
                            env
                            fr
                        )
                    )
                )
                (envir->k env)
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep-lemma-8 rep-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep-lemma-13 rep-lemma-9 rep-lemma-5 rep-lemma-22 rep-lemma-24
                  rep-lemma-26 rep-lemma-27 rep-lemma-27 sublist-11 sublist-12
                  rep-lemma-7 rep-lemma-25 rep-lemma-30 rep-lemma-32 rep-lemma-33
                  rep-lemma-35 rep-lemma-19 rep-lemma-36 rep-lemma-20 rep-lemma-21
                  rep-lemma-18 rep-lemma-29 rep-lemma-16 rep-lemma-28 rep-lemma-37
                  rep-lemma-38 rep-lemma-39 rep-lemma-40 rep-lemma-41 rep-lemma-42
                  rep-lemma-43 rep-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep-lemma-45 rep-lemma-46 rep-lemma-47
                  rep-lemma-48 rep-lemma-49 rep-lemma-50 rep-lemma-51 rep-lemma-52
                  rep-lemma-53 rep-lemma-54 rep-lemma-55 rep-lemma-56 sublist-6
                  rep-lemma-57 rep-lemma-58 rep-lemma-59 rep-lemma-60 rep-lemma-61
                  rep-lemma-62 rep-lemma-63 rep-lemma-64 rep-lemma-65 rep-lemma-66
                  rep-lemma-67 rep-lemma-68 rep-lemma-69 rep-lemma-70 rep-lemma-71
                  rep-lemma-72 rep-lemma-73 rep-lemma-74 rep-lemma-75 rep-lemma-76
                  rep-lemma-77 rep-lemma-78 rep-lemma-79 rep-lemma-80 rep-lemma-81
                  rep-lemma-82 rep-lemma-83 rep-lemma-84 rep-lemma-85 rep-lemma-86
                  rep-lemma-87 rep-lemma-88 rep-lemma-89 rep-lemma-90 rep-lemma-91
                  rep-lemma-92 rep-lemma-93 rep-lemma-94 rep-lemma-95 rep-lemma-96
                  rep-lemma-97 rep-lemma-98 rep-lemma-99 rep-lemma-100 rep-lemma-101
                  rep-lemma-102 rep-lemma-103 rep-lemma-104 rep-lemma-105 rep-lemma-106
                  rep-lemma-107)
        :use (rep-lemma-105 rep-lemma-107)
    )
)

