The directory insertion corresponds to the experiment of deductive verification of insertion sorting program.
It includes the files containing underlying theory and the theory for verification conditions.

Each file represents a theory, which consists of a set of definitions and theorems. Thus, each file is the library,
it forms a module for ACL2 proving system, which is called a book.

To prove all theorems of a book a process, which is called a certification, is launched. The book is certified
if all theorems were proved. Thus, to prove a certain theorem it is necessary to add it to the book and to certify it.

In order to apply in given book another theorem from another book, it is necessary to use "include-book" instruction.
In order to certify a book all included books have to be certified.